/**
 * @file      cubes_1_msp_endian.h
 * @author    John Wikman 
 * @copyright MIT License
 * @brief     Functions for endian conversion.
 *
 * @details
 * Declares function prototypes for converting a byte sequence from Big-Endian
 * into system defined integers.
 */

#ifndef CUBES_1_MSP_ENDIAN_H
#define CUBES_1_MSP_ENDIAN_H

void cubes_1_msp_to_bigendian32(unsigned char *dest, unsigned long number);
unsigned long cubes_1_msp_from_bigendian32(const unsigned char *src);

#endif /* CUBES_1_MSP_ENDIAN_H */
