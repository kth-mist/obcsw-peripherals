/**
 * @file      cubes_1_msp_opcodes.h
 * @author    John Wikman
 * @copyright MIT License
 * @brief     Defines all the standard opcodes in MSP.
 *
 * @details
 * Defines all the standard opcodes in MSP as well as macros for categorizing
 * opcodes.
 */

#ifndef CUBES_1_MSP_OPCODES_H
#define CUBES_1_MSP_OPCODES_H

/* MSP Control Flow */
#define CUBES_1_MSP_OP_NULL        0x00
#define CUBES_1_MSP_OP_DATA_FRAME  0x01
#define CUBES_1_MSP_OP_F_ACK       0x02
#define CUBES_1_MSP_OP_T_ACK       0x03
#define CUBES_1_MSP_OP_EXP_SEND    0x04
#define CUBES_1_MSP_OP_EXP_BUSY    0x05

/* System Commands */
#define CUBES_1_MSP_OP_ACTIVE      0x10
#define CUBES_1_MSP_OP_SLEEP       0x11
#define CUBES_1_MSP_OP_POWER_OFF   0x12

/* Standard OBC Request */
#define CUBES_1_MSP_OP_REQ_PAYLOAD 0x20
#define CUBES_1_MSP_OP_REQ_HK      0x21
#define CUBES_1_MSP_OP_REQ_PUS     0x22

/* Standard OBC Read */
#define CUBES_1_MSP_OP_SEND_TIME   0x30
#define CUBES_1_MSP_OP_SEND_PUS    0x31


/* Values for determining opcode type */
#define CUBES_1_MSP_OP_TYPE_CTRL 0x00
#define CUBES_1_MSP_OP_TYPE_SYS  0x10
#define CUBES_1_MSP_OP_TYPE_REQ  0x20
#define CUBES_1_MSP_OP_TYPE_SEND 0x30

/**
 * @brief Determines the opcode type.
 * @param opcode The opcode value.
 * @return The type of the opcode. It will return either CUBES_1_MSP_OP_TYPE_CTRL,
 *         CUBES_1_MSP_OP_TYPE_SYS, CUBES_1_MSP_OP_TYPE_REQ, or CUBES_1_MSP_OP_TYPE_SEND.
 */
#define CUBES_1_MSP_OP_TYPE(opcode) ((opcode) & 0x30)

/**
 * @brief Determines whether the opcode is custom or not.
 * @param opcode The opcode value.
 * @return A non-zero value if the value represents a custom opcode. Otherwise
 *         0 is returned.
 */
#define CUBES_1_MSP_OP_IS_CUSTOM(opcode) (((opcode) & 0x70) >= 0x50)

#endif /* CUBES_1_MSP_OPCODES_H */
