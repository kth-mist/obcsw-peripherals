/**
 * @file      cubes_1_handler.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     CUBES 1 state utility functions
 */
#ifndef CUBES_1_EXP_HANDLER_H
#define CUBES_1_EXP_HANDLER_H

#include <stdint.h>

#define ARDUINO_UNIT_ID_CUBES_1 (144)

// IFLASH1 starts at 0xC0000 and ends at 0xFFFFF
// This address is relative to the start of IFLASH1
#define CUBES_1_FLASH_START_ADDR   (0x3F000)

void cubes_1_setup();
uint8_t cubes_1_hvps_active();
volatile uint8_t cubes_1_powered_off();
uint32_t cubes_1_timestamp();
int16_t cubes_1_hvps_dtp1();
int16_t cubes_1_hvps_dtp2();
uint16_t cubes_1_hvps_dt1();
uint16_t cubes_1_hvps_dt2();
uint16_t cubes_1_hvps_vb();
uint16_t cubes_1_hvps_tb();
uint8_t cubes_1_citi_conf_selected();
void cubes_1_citi_conf(uint8_t id, uint8_t *dst);
void cubes_1_prob_conf(uint8_t *dst);
uint8_t cubes_1_gateware_conf();
uint32_t cubes_1_calib_pulse_conf();
void cubes_1_restore_seqflags();
void cubes_1_save_seqflags();

void cubes_1_handler_send(uint8_t opcode, uint8_t *data, size_t *len);
void cubes_1_handler_recv(uint8_t opcode, const uint8_t *data, size_t len);
void cubes_1_handler_sys(uint8_t opcode);

#endif /* CUBES_1_EXP_HANDLER_H */
