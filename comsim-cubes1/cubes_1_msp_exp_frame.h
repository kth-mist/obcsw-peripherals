/**
 * @file      cubes_1_msp_exp_frame.h
 * @author    John Wikman
 * @copyright MIT License
 * @brief     Declares functions for handling MSP frames on the experiment side.
 */

#ifndef CUBES_1_MSP_FRAME_H
#define CUBES_1_MSP_FRAME_H

unsigned long cubes_1_msp_exp_frame_generate_fcs(const unsigned char *data, int from_obc, unsigned long len);
int cubes_1_msp_exp_frame_fcs_valid(const unsigned char *data, int from_obc, unsigned long len);
void cubes_1_msp_exp_frame_format_header(unsigned char *dest, unsigned char opcode, unsigned char frame_id, unsigned long dl);
void cubes_1_msp_exp_frame_format_empty_header(unsigned char *dest, unsigned char opcode);

#endif /* CUBES_1_MSP_FRAME_H */
