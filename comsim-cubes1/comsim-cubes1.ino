/**
 * @file comsim-cubes.ino
 */

extern "C" {
	#include "cubes_1_msp_i2c_slave.h"
	#include "cubes_1_handler.h"
}

#define I2C_CLOCKSPEED (400000)

/** Arduino Setup */
void setup(void)
{
	Serial.begin(9600);
	cubes_1_msp_i2c_setup(I2C_CLOCKSPEED);

	cubes_1_setup();

	Serial.print("Setup Complete\n");
}

void loop(void)
{
	if (cubes_1_powered_off()) {
		cubes_1_save_seqflags();
	}

	delay(10);
}
