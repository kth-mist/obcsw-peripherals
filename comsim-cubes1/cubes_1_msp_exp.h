/**
 * @file      cubes_1_msp_exp.h
 * @author    John Wikman
 * @copyright MIT License
 * @brief     A header that includes all the commonly used headers in the
 *            experiment side of MSP.
 */

#ifndef CUBES_1_MSP_EXP_H
#define CUBES_1_MSP_EXP_H

#ifdef __cplusplus
extern "C" {
#endif
#include "cubes_1_msp_configuration.h"
#include "cubes_1_msp_constants.h"
#include "cubes_1_msp_crc.h"
#include "cubes_1_msp_endian.h"
#include "cubes_1_msp_opcodes.h"
#include "cubes_1_msp_seqflags.h"
#include "cubes_1_msp_exp_callback.h"
#include "cubes_1_msp_exp_definitions.h"
#include "cubes_1_msp_exp_error.h"
#include "cubes_1_msp_exp_frame.h"
#include "cubes_1_msp_exp_handler.h"
#include "cubes_1_msp_exp_state.h"
#ifdef __cplusplus
}
#endif

#endif /* CUBES_1_MSP_EXP_H */
