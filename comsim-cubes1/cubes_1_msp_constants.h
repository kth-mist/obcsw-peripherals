/**
 * @file      cubes_1_msp_constants.h
 * @author    John Wikman
 * @copyright MIT License
 * @brief     Defines constants relevant to MSP.
 *
 * @details
 * Defines constants relevant to both the OBC side and experiment side of the
 * MSP library.
 */

#ifndef CUBES_1_MSP_CONSTANTS_H
#define CUBES_1_MSP_CONSTANTS_H

/**
 * @brief The maximum data length in MSP.
 *
 * The maximum amount of data that can be sent in an MSP transaction.
 */
#define CUBES_1_MSP_MAX_DATA_LENGTH 4294967295UL

#endif /* CUBES_1_MSP_CONSTANTS_H */
