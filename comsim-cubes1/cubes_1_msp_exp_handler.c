/**
 * @file      cubes_1_msp_exp_handler.c
 * @author    William Stackenäs
 * @brief     Non-arduino version of CUBES simulator with MSP callbacks
 *
 * @details
 * Custom functions and structures used within cubes
 */
 
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include "cubes_1_msp_exp_handler.h"
#include "cubes_1_defines.h"
#include "cubes_1_handler.h"

static size_t recvlen;
static size_t recvidx;
static uint8_t recvbuf[CUBES_1_MSP_RECVBUF_SIZE];

static size_t sendlen;
static size_t sendidx;
static uint8_t sendbuf[CUBES_1_MSP_SENDBUF_SIZE];

void cubes_1_msp_expsend_start(unsigned char opcode, unsigned long *len)
{
	if (len == NULL)
		return;

	cubes_1_handler_send(opcode, sendbuf, &sendlen);

	// Truncate data if too large
	if (sendlen > CUBES_1_MSP_SENDBUF_SIZE)
		sendlen = CUBES_1_MSP_SENDBUF_SIZE;

	sendidx = 0;

	*len = sendlen;
}

void cubes_1_msp_expsend_data(unsigned char opcode,
                            unsigned char *buf,
                            unsigned long len,
                            unsigned long offset)
{
	(void) opcode;

	if (buf == NULL)
		return;

	memcpy(buf, &sendbuf[offset], len);

	// Update index
	sendidx = offset + len;
}

void cubes_1_msp_expsend_complete(unsigned char opcode)
{
	if (sendidx != sendlen) {
		// Did not send all of the data, should probably warn the user!
		// TODO...
	}
	return;
}

void cubes_1_msp_expsend_error(unsigned char opcode, int error)
{
	fprintf(stderr, "[CUBES] Send error: %d\n", error);
	return;
}

void cubes_1_msp_exprecv_start(unsigned char opcode, unsigned long len)
{
	(void) opcode;

	recvlen = len;
	recvidx = 0;
}

void cubes_1_msp_exprecv_data(unsigned char opcode,
                            const unsigned char *buf,
                            unsigned long len,
                            unsigned long offset)
{
	// Truncate received data
	if (offset >= CUBES_1_MSP_RECVBUF_SIZE)
		return;

	if ((offset + len) > CUBES_1_MSP_RECVBUF_SIZE)
		len = CUBES_1_MSP_RECVBUF_SIZE - offset;

	memcpy(&recvbuf[offset], buf, len);
	recvidx = offset + len;
}

void cubes_1_msp_exprecv_complete(unsigned char opcode)
{
	if (recvidx != recvlen) {
		// Did not receive all of the data, should probably warn the user!
		// TODO...
	}
	cubes_1_handler_recv(opcode, recvbuf, recvlen);
}

void cubes_1_msp_exprecv_error(unsigned char opcode, int error)
{
	fprintf(stderr, "[CUBES] Receive error: %d\n", error);
	return;
}

void cubes_1_msp_exprecv_syscommand(unsigned char opcode)
{
	cubes_1_handler_sys(opcode);
}
