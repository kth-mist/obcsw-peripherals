/**
 * @file hdrm_io_handler.c
 */

#include <stdio.h>
#include <stdlib.h>

#include <Arduino.h>

#include "hdrm_io_handler.h"

#define ARDUINO_UNIT_ID ARDUINO_UNIT_ID_HDRM

#define ID_1A (0)
#define ID_3A (1)
#define ID_1B (2)
#define ID_3B (3)

static unsigned long _deploy_delay = 0;
static unsigned char _reset_status_on_deploy_start = 0;

static volatile unsigned char _status_1 = 0;
static volatile unsigned char _status_3 = 0;

static struct {
	unsigned char trigger_count;
	unsigned char reject_count;
	volatile unsigned long deploy_start_time;
	volatile unsigned char deploy_active_flag : 1;
	volatile unsigned char *status;
} _hdrm[4] = {{0}};

static void hdrm_1a_deploy_set();
static void hdrm_3a_deploy_set();
static void hdrm_1b_deploy_set();
static void hdrm_3b_deploy_set();
static void hdrm_set(unsigned char id, unsigned char line);
static unsigned char hdrm_deploy_has_completed(unsigned char id);

void hdrm_reset(const struct hdrm_config *conf)
{
	// Attach input-pullups to HDRM Sim. LEDs to check if the corresponding LEDs are lightened:
	pinMode(hdrmPin1A, INPUT_PULLUP);
	pinMode(hdrmPin3A, INPUT_PULLUP);
	pinMode(hdrmPin1B, INPUT_PULLUP);
	pinMode(hdrmPin3B, INPUT_PULLUP);
	// Status pin as output for OBC to catch:
	pinMode(hdrmStatusPin1, OUTPUT);
	pinMode(hdrmStatusPin3, OUTPUT);

	attachInterrupt(digitalPinToInterrupt(hdrmPin1A), hdrm_1a_deploy_set, CHANGE);
	attachInterrupt(digitalPinToInterrupt(hdrmPin3A), hdrm_3a_deploy_set, CHANGE);
	attachInterrupt(digitalPinToInterrupt(hdrmPin1B), hdrm_1b_deploy_set, CHANGE);
	attachInterrupt(digitalPinToInterrupt(hdrmPin3B), hdrm_3b_deploy_set, CHANGE);

	if (conf != NULL) {
		_hdrm[ID_1A].reject_count = conf->reject_count_1a;
		_hdrm[ID_3A].reject_count = conf->reject_count_3a;
		_hdrm[ID_1B].reject_count = conf->reject_count_1b;
		_hdrm[ID_3B].reject_count = conf->reject_count_3b;

		_deploy_delay = conf->deploy_delay;
		_reset_status_on_deploy_start = conf->reset_status_on_deploy_start;
	}

	_hdrm[ID_1A].status = &_status_1;
	_hdrm[ID_1B].status = &_status_1;
	_hdrm[ID_3A].status = &_status_3;
	_hdrm[ID_3B].status = &_status_3;

	for (size_t i = 0; i < 4; i++) {
		*(_hdrm[i].status) = 0;
		_hdrm[i].trigger_count = 0;
	}
}

unsigned char hdrm_1_status_get(void)
{
	_status_1 |= hdrm_deploy_has_completed(ID_1A);
	_status_1 |= hdrm_deploy_has_completed(ID_1B);

	return _status_1;
}

unsigned char hdrm_3_status_get(void)
{
	_status_3 |= hdrm_deploy_has_completed(ID_3A);
	_status_3 |= hdrm_deploy_has_completed(ID_3B);

	return _status_3;
}

/*
 * Interrupt handler function for HDRM1A -> respond when a CHANGE in state on HDRM1A detected.
 */
static void hdrm_1a_deploy_set()
{
	unsigned char line = digitalRead(hdrmPin1A);
	hdrm_set(ID_1A, line);
}

/*
 * Interrupt handler function for HDRM3A -> respond when a CHANGE in state on HDRM3A detected.
 */
static void hdrm_3a_deploy_set()
{
	unsigned char line = digitalRead(hdrmPin3A);
	hdrm_set(ID_3A, line);
}

/*
 * Interrupt handler function for HDRM1B -> respond when a CHANGE in state on HDRM1B detected.
 */
static void hdrm_1b_deploy_set()
{
	unsigned char line = digitalRead(hdrmPin1B);
	hdrm_set(ID_1B, line);
}

/*
 * Interrupt handler function for HDRM3B -> respond when a CHANGE in state on HDRM3B detected.
 */
static void hdrm_3b_deploy_set()
{
	unsigned char line = digitalRead(hdrmPin3B);
	hdrm_set(ID_3B, line);
}

static void hdrm_set(unsigned char id, unsigned char line)
{
	if (line) {
		if (!_hdrm[id].deploy_active_flag) {
			if (_hdrm[id].trigger_count >= _hdrm[id].reject_count) {
				_hdrm[id].deploy_start_time = millis();
				_hdrm[id].deploy_active_flag = 1;
				if (_reset_status_on_deploy_start) {
					*_hdrm[id].status = 0;
					_hdrm[id].trigger_count = 0;
				}
			} else {
				_hdrm[id].trigger_count++;
			}
		}
	} else {
		*_hdrm[id].status |= hdrm_deploy_has_completed(id);
		_hdrm[id].deploy_active_flag = 0;
	}
}

static unsigned char hdrm_deploy_has_completed(unsigned char id)
{
	unsigned long elapsed;

	if (_hdrm[id].deploy_active_flag) {
		elapsed = millis() - _hdrm[id].deploy_start_time;

		if (elapsed >= _deploy_delay) {
			_hdrm[id].deploy_active_flag = 0;
			return 1;
		}
	}

	return 0;
}
