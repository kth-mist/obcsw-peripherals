/**
 * @file hdrm_io_handler.h
 */

#ifndef HDRM_HANDLER_H
#define HDRM_HANDLER_H

/* @brief Used in the QEMU/HAL-clone setup to differentiate
          between Arduino simulators */
#define ARDUINO_UNIT_ID_HDRM      (46)

/*
 *hdrmPinXY are pins that receive deploy command from DB
 */
#define hdrmPin1A       54
#define hdrmPin3A       61
#define hdrmPin1B       57
#define hdrmPin3B       65

/*
 * hdrmStatusPinX indicate deployment status of hdrm units
 */
#define hdrmStatusPin1  6
#define hdrmStatusPin3  2

struct hdrm_config {
	/*
	 * @brief How many times to reject the deployment command for each HDRM pin
	 */
	unsigned char reject_count_1a;
	unsigned char reject_count_3a;
	unsigned char reject_count_1b;
	unsigned char reject_count_3b;

	/*
	 * @brief How long the deployment procedure should take before indicating a deployed status
	 */
	unsigned long deploy_delay;

	/*
	 * @brief Whether the corresponding status pin should be automatically reset upon
	 *        receiving a deployment command after the deployment has already been
	 *        completed. A value of 1 could allow restarting the deployment procedure
	 *        without needing to call hdrm_reset, but would not be representative of
	 *        the real HDRM device.
	 */
	unsigned char reset_status_on_deploy_start : 1;
};

void hdrm_reset(const struct hdrm_config *conf);

unsigned char hdrm_1_status_get(void);
unsigned char hdrm_3_status_get(void);

#endif /* HDRM_HANDLER_H */
