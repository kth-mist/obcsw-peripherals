/**
 * @file   hdrm-simulator.ino
 * @author Ngai Nam Chan (Thomas)
 * @author Pak Chuen Hau (Jason)
 */

/*
 *hdrmPinXY are pins that receive deploy command from DB
 */
const int hdrmPin1A = 54;
const int hdrmPin3A = 61;
const int hdrmPin1B = 57;
const int hdrmPin3B = 65;
/*
 * hdrmStatusPinX indicate deployment status of hdrm units
 */
const int hdrmStatusPin1 = 6;
const int hdrmStatusPin3 = 2;

unsigned long hdrm_status_delay = 5000; // Customizable HDRM sim status pin throwing delay. Should not exceed the max. timeout below.
const unsigned long hdrm_timeout = 240000; // (For reference only) Max. timeout duration for HDRM deployment. Should not exceed this!
const unsigned int hdrm_deploy_attempts = 3; // Max. HDRM deployment attempts = 3 according to definitions on the Initialization Phase.

/*
 * Rejecting the first n times HDRM deployment (hardcoded for now):
 */
static int rejectTimes1A = 3;
static int rejectTimes3A = 0;
static int rejectTimes1B = 0;
static int rejectTimes3B = 1;
/* 
 * Trigger incremental counters for HDRM deployment rejection:
 */
static int triggerCounter1A = 0;
static int triggerCounter3A = 0;
static int triggerCounter1B = 0;
static int triggerCounter3B = 0;

volatile unsigned long elapsedTime1A = 0;
volatile unsigned long time1A = 0;
volatile unsigned long elapsedTime3A = 0;
volatile unsigned long time3A = 0;
volatile unsigned long elapsedTime1B = 0;
volatile unsigned long time1B = 0;
volatile unsigned long elapsedTime3B = 0;
volatile unsigned long time3B = 0;
/*
 * Flags to be set in ISRs
 */
volatile boolean timerOn1A = LOW;
volatile boolean timeCapture1A = LOW;
volatile boolean timerOn3A = LOW;
volatile boolean timeCapture3A = LOW;
volatile boolean timerOn1B = LOW;
volatile boolean timeCapture1B = LOW;
volatile boolean timerOn3B = LOW;
volatile boolean timeCapture3B = LOW;

void setup()
{
  // Attach input-pullups to HDRM Sim. LEDs to check if the corresponding LEDs are lightened:
  pinMode(hdrmPin1A, INPUT_PULLUP);
  pinMode(hdrmPin3A, INPUT_PULLUP);
  pinMode(hdrmPin1B, INPUT_PULLUP);
  pinMode(hdrmPin3B, INPUT_PULLUP);
  // Status pin as output for OBC to catch:
  pinMode(hdrmStatusPin1, OUTPUT);
  pinMode(hdrmStatusPin3, OUTPUT);

  attachInterrupt(digitalPinToInterrupt(hdrmPin1A), hdrmHandler1A, CHANGE);
  attachInterrupt(digitalPinToInterrupt(hdrmPin3A), hdrmHandler3A, CHANGE);
  attachInterrupt(digitalPinToInterrupt(hdrmPin1B), hdrmHandler1B, CHANGE);
  attachInterrupt(digitalPinToInterrupt(hdrmPin3B), hdrmHandler3B, CHANGE);

  Serial.begin(115200);
  Serial.println("HDRM Simulator started up.");
}


void loop()
{
  /* Debug logs:
  Serial.print(digitalRead(hdrmPin1A));
  Serial.print(digitalRead(hdrmPin3A));
  Serial.print(digitalRead(hdrmPin1B));
  Serial.println(digitalRead(hdrmPin3B));
  Serial.println(digitalRead(hdrmStatusPin1));
  Serial.println(digitalRead(hdrmStatusPin3));
  */

/*
 * If Pulse for HDRM1A is enabled for over 240 seconds 
 * Indicate to the iOBC by setting hdrmStatusPin1
 */
  if(timerOn1A)
  {
    if(timeCapture1A)
    {
      digitalWrite(hdrmStatusPin3, LOW); // Throw HDRM Status Pin 3 back to LOW as completed 3A deployment.
      time1A = millis();
      timeCapture1A = LOW;
    }
    elapsedTime1A = millis()-time1A;
    if(elapsedTime1A >= hdrm_timeout || elapsedTime1A >= hdrm_status_delay)
    {
      if(!digitalRead(hdrmStatusPin1))
      {
        digitalWrite(hdrmStatusPin1, HIGH);
        Serial.println("HDRM Status Pin 1 triggered from Pin 1A.");
        triggerCounter1A = 0; // Reset 1A counter trigger deployment for next initialization test purpose.
      }
    }
  }

 /*
 * If Pulse for HDRM3A is enabled for over 240 seconds 
 * Indicate to the iOBC by setting hdrmStatusPin3
 */
  if(timerOn3A)
  {
    if(timeCapture3A)
    {
      digitalWrite(hdrmStatusPin1, LOW); // Throw HDRM Status Pin 1 back to LOW as completed 1A deployment.
      time3A = millis();
      timeCapture3A = LOW;
    }
    elapsedTime3A = millis()-time3A;
    if(elapsedTime3A >= hdrm_timeout || elapsedTime3A >= hdrm_status_delay)
    {
      if(!digitalRead(hdrmStatusPin3))
      {
        digitalWrite(hdrmStatusPin3, HIGH);
        Serial.println("HDRM Status Pin 3 triggered from Pin 3A.");
        triggerCounter3A = 0; // Reset 3A counter trigger deployment for next initialization test purpose.
      }
    }
  }

/*
 * If Pulse for HDRM1B is enabled for over 240 seconds 
 * Indicate to the iOBC by setting hdrmStatusPin1
 */
  if(timerOn1B)
  {
    if(timeCapture1B)
    {
      digitalWrite(hdrmStatusPin3, LOW); // Throw HDRM Status Pin 3 back to LOW as completed 3A deployment.
      time1B = millis();
      timeCapture1B = LOW;
    }
    elapsedTime1B = millis()-time1B;
    if(elapsedTime1B >= hdrm_timeout || elapsedTime1B >= hdrm_status_delay)
    {
      if(!digitalRead(hdrmStatusPin1))
      {
        digitalWrite(hdrmStatusPin1, HIGH);
        Serial.println("HDRM Status Pin 1 triggered from Pin 1B.");
        triggerCounter1B = 0; // Reset 1B counter trigger deployment for next initialization test purpose.
      }
    }
  }

/*
 * If Pulse for HDRM3B is enabled for over 240 seconds 
 * Indicate to the iOBC by setting hdrmStatusPin3
 */
  if(timerOn3B)
  {
    if(timeCapture3B)
    {
      digitalWrite(hdrmStatusPin1, LOW); // Throw HDRM Status Pin 1 back to LOW as completed all deployment -> loop again to 1A.
      time3B = millis();
      timeCapture3B = LOW;
    }
    elapsedTime3B = millis()-time3B;
    if(elapsedTime3B >= hdrm_timeout || elapsedTime3B >= hdrm_status_delay)
    {
      if(!digitalRead(hdrmStatusPin3))
      {
        digitalWrite(hdrmStatusPin3, HIGH);
        Serial.println("HDRM Status Pin 3 triggered from Pin 3B.");
        triggerCounter3B = 0; // Reset 3B counter trigger deployment for next initialization test purpose.
      }
    }
  }
  
}

/*
 * Interrupt handler function for HDRM1A -> respond when a CHANGE in state on HDRM1A detected.
 */
void hdrmHandler1A()
{
  if(digitalRead(hdrmPin1A)==HIGH && rejectTimes1A < hdrm_deploy_attempts) // Will not enter loop if # of reject times exceeds HDRM deployment attempts.
  {
    if(triggerCounter1A - rejectTimes1A >= 0) 
    {
      timerOn1A = HIGH;
      timeCapture1A = HIGH;
    }
    else { triggerCounter1A++; }
  }
  else
  { 
    timerOn1A = LOW;
    timeCapture1A = LOW;
  }
}

/*
 * Interrupt handler function for HDRM3A -> respond when a CHANGE in state on HDRM3A detected.
 */
void hdrmHandler3A()
{
  if(digitalRead(hdrmPin3A)==HIGH && rejectTimes3A < hdrm_deploy_attempts) // Will not enter loop if # of reject times exceeds HDRM deployment attempts.
  {
    if(triggerCounter3A - rejectTimes3A >= 0) 
    {
      timerOn3A = HIGH;
      timeCapture3A = HIGH;
    }
    else { triggerCounter3A++; }
  }
  else
  { 
    timerOn3A = LOW;
    timeCapture3A = LOW;
  }
}

/*
 * Interrupt handler function for HDRM1B -> respond when a CHANGE in state on HDRM1B detected.
 */
void hdrmHandler1B()
{
  if(digitalRead(hdrmPin1B)==HIGH && rejectTimes1B < hdrm_deploy_attempts) // Will not enter loop if # of reject times exceeds HDRM deployment attempts.
  { 
    if(triggerCounter1B - rejectTimes1B >= 0) 
    {
      timerOn1B = HIGH;
      timeCapture1B = HIGH;
    }
    else { triggerCounter1B++; }
  }
  else
  { 
    timerOn1B = LOW;
    timeCapture1B = LOW;
  }
}

/*
 * Interrupt handler function for HDRM3B -> respond when a CHANGE in state on HDRM3B detected.
 */
void hdrmHandler3B()
{
  if(digitalRead(hdrmPin3B)==HIGH && rejectTimes3B < hdrm_deploy_attempts) // Will not enter loop if # of reject times exceeds HDRM deployment attempts.
  { 
    if(triggerCounter3B - rejectTimes3B >= 0)
    {
      timerOn3B = HIGH;
      timeCapture3B = HIGH;
    }
    else { triggerCounter3B++; }
  }
  else
  { 
    timerOn3B = LOW;
    timeCapture3B = LOW;
  }
}
