/**
 * @file   ax25.c
 * @author John Wikman and William Stackenäs
 *
 * Encoding of AX.25 frames
 */

#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include "ax25.h"

static uint16_t crc16_table[256];
static int crc16_initialized = 0;

static void crc16_initialize_table(void)
{
    int i;
    uint16_t tmp;

    for (i = 0; i < 256; i++) {
        tmp = 0;

        if ((i & 0x01) != 0)
            tmp = tmp ^ 0x1021;
        if ((i & 0x02) != 0)
            tmp = tmp ^ 0x2042;
        if ((i & 0x04) != 0)
            tmp = tmp ^ 0x4084;
        if ((i & 0x08) != 0)
            tmp = tmp ^ 0x8108;
        if ((i & 0x10) != 0)
            tmp = tmp ^ 0x1231;
        if ((i & 0x20) != 0)
            tmp = tmp ^ 0x2462;
        if ((i & 0x40) != 0)
            tmp = tmp ^ 0x48C4;
        if ((i & 0x80) != 0)
            tmp = tmp ^ 0x9188;

        crc16_table[i] = tmp;
    }

    crc16_initialized = 1;
}

/**
 * CRC-CITT checksum
 */
uint16_t crc16(const uint8_t *data, size_t length)
{
    size_t i;
    uint16_t tmpchk;

    if (!crc16_initialized)
        crc16_initialize_table();

    tmpchk = 0xffff;

    for (i = 0; i < length; i++)
        tmpchk = ((tmpchk << 8) & 0xff00) ^ crc16_table[((tmpchk >> 8) ^ data[i]) & 0xff];

    return tmpchk;
}

/**
 * Encodes an AX.25 frame, given the info field data pointed to by src.
 */
void ax25_encode(uint8_t *dst,
                 size_t *dstlen,
                 const uint8_t *src,
                 size_t srclen,
                 const char *to_callsign,
                 const char *from_callsign)
{
    int i;
	uint16_t fcs;

	dst[0] = 0x7e; // Head flag

    // to_callsign
    for (i = 0; i < 6; i++) {
        if (to_callsign[i] == 0)
            break;

        dst[1 + i] = to_callsign[i] << 1;
    }
    for (; i < 6; i++)
        dst[1 + i] = 0x40; // pad with spaces
    dst[7] = 0x60 | 0x00; // SSID = 0

    // from_callsign
    for (i = 0; i < 6; i++) {
        if (from_callsign[i] == 0)
            break;

        dst[8 + i] = from_callsign[i] << 1;
    }
    for (; i < 6; i++)
        dst[8 + i] = 0x40; // pad with spaces
    dst[14] = 0x61 | 0x00; // SSID = 0

	dst[15] = 0x03; // Control bits
	dst[16] = 0xf0; // Protocol identifier
	memcpy(&dst[17], src, srclen);

	fcs = crc16(dst, 17 + srclen);
	dst[17 + srclen] = (uint8_t) ((fcs >> 8) & 0xff);
	dst[17 + srclen + 1] = (uint8_t) (fcs & 0xff);
	dst[17 + srclen + 2] = 0x7e; // Tail flag

	*dstlen = srclen + 20;
}
