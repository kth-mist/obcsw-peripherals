/**
 * @file   ax25.h
 * @author John Wikman and William Stackenäs
 */

#ifndef AX25_H
#define AX25_H

#include <inttypes.h>
#include <stdlib.h>

void ax25_encode(uint8_t *dst,
                 size_t *dstlen,
                 const uint8_t *src,
                 size_t srclen,
                 const char *to_callsign,
                 const char *from_callsign);

#endif /* AX25_H */
