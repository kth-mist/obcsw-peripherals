/**
 * @file trxvu_cmds.h
 */

#ifndef TRXVU_CMDS_H
#define TRXVU_CMDS_H

#include <stdint.h>

/* @brief Used in the QEMU/HAL-clone setup to differentiate
          between Arduino simulators */
#define ARDUINO_UNIT_ID_TRXVU     (85)

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Resets the TRXVU simulator.
 */
void trxvu_setup(void);

/**
 * @brief Receive a frame at the TRXVU.
 *
 * @param[in] data The frame data that should be recevied at the TRXVU.
 * @param[in] size The size of the frame data.
 *
 * @return 0 on success,
 *         1 on invalid argument,
 *         2 if RX buffer is full (cannot receive data)
 */
int trxvu_recv_frame(uint8_t *data, size_t size);

/**
 * @brief Have the TRXVU send a frame to the given buffer.
 *
 * @param[out] data The buffer to store the frame sent from the TRXVU.
 * @param[in] max_size The max size of the data. If the TRXVU sends more data, it will be truncated.
 * @param[in] size Where to store the size of the sent data.
 *
 * @return 0 on success,
 *         1 on invalid argument,
 *         2 if TX buffer is empty (no data to send)
 */
int trxvu_send_frame(uint8_t *data, size_t max_size, size_t* size);

/**
 * @brief Prints any available telemetry in the TRXVU downlink buffer as a FTCMD reply
 */
void trxvu_tm_reply_cb(void);

#ifdef __cplusplus
}
#endif

#endif /* TRXVU_CMDS_H */
