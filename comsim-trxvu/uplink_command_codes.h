/**
 * @file   uplink_command_codes.h
 * @author John Wikman and William Stackenäs
 */

#ifndef UPLINK_COMMAND_CODES_H
#define UPLINK_COMMAND_CODES_H

#define ULCC_WATCHDOG_RESET                 (0xCC)
#define ULCC_SOFTWARE_RESET                 (0xAA)
#define ULCC_HARDWARE_SYSTEM_RESET          (0xAB)
#define ULCC_GET_FRAME_COUNT                (0x21)
#define ULCC_GET_FRAME                      (0x22)
#define ULCC_REMOVE_FRAME                   (0x24)
#define ULCC_MEASURE_ALL_TELEMETRY_CHANNELS (0x1a)
#define ULCC_REPORT_RECEIVER_UPTIME         (0x40)

#endif /* UPLINK_COMMAND_CODES_H */
