/**
 * @file   uplink_command_handler.h
 * @author John Wikman and William Stackenäs
 */

#ifndef UPLINK_COMMAND_HANDLER_H
#define UPLINK_COMMAND_HANDLER_H

#define UL_DEFAULT_BITRATE (1200)

#define UL_MAX_FRAMES       (40)
#define UL_MAX_PAYLOAD_SIZE (200)
#define UL_MAX_FRAME_SIZE   (UL_MAX_PAYLOAD_SIZE + 20)

unsigned long ul_watchdog_last_kick();
uint8_t ul_soft_reset_was_triggered();
uint8_t ul_hard_reset_was_triggered();
unsigned int ul_bitrate();

void ul_command_handler_init(void);
int ul_command_handler_provide_frame(const uint8_t *data, size_t datalen);
void ul_command_handler_get_i2c_response(const uint8_t **buf, size_t *len);
int ul_command_handler_receive_i2c(const uint8_t *data, size_t datalen);

#endif /* UPLINK_COMMAND_HANDLER_H */
