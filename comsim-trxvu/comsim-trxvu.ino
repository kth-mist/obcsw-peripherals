/**
 * @file trxvu_due.ino
 */

#include <inttypes.h>
#include <stdlib.h>

#include <Wire.h>
#include "debug.hpp"

#include "ftcmd.h"

extern "C" {
	#include "kiss.h"
	#include "downlink_command_handler.h"
	#include "uplink_command_handler.h"
	#include "trxvu_cmds.h"
}

#if (BUFFER_LENGTH < 245)
#error You must change the BUFFER_LENGTH constant in Wire.h to at least 245 before you can use the simulator.
#endif

#define I2C_BITRATE (400000)

// Change to 1 to print telemetry to Serial (as FTCMD replies) in addition to SerialUSB
#define TM_OVER_SERIAL  0

//#define USE_REAL_TRXVU_I2C_ADDRESSES

#ifdef USE_REAL_TRXVU_I2C_ADDRESSES
#define I2C_DOWNLINK_ADDRESS (0x61)
#define I2C_UPLINK_ADDRESS   (0x60)
#else
#define I2C_DOWNLINK_ADDRESS (0x63)
#define I2C_UPLINK_ADDRESS   (0x62)
#endif

void downlink_i2c_receive_cb(int len);
void downlink_i2c_request_cb(void);
void uplink_i2c_receive_cb(int len);
void uplink_i2c_request_cb(void);

uint8_t kiss_encode_buffer[515];
size_t kiss_encode_buffer_length;
uint8_t downlink_receive_buffer[256];
uint8_t uplink_receive_buffer[256];

unsigned long beacon_poll_time = 0;

void setup(void)
{
	trxvu_setup();

	kiss_encode_buffer_length = 0;

	// SerialUSB = TNC uplink/downlink
	SerialUSB.begin(19200);

	Wire.begin(I2C_DOWNLINK_ADDRESS);
	Wire.setClock(I2C_BITRATE);
	Wire.onReceive(downlink_i2c_receive_cb);
	Wire.onRequest(downlink_i2c_request_cb);

	Wire1.begin(I2C_UPLINK_ADDRESS);
	Wire1.setClock(I2C_BITRATE);
	Wire1.onReceive(uplink_i2c_receive_cb);
	Wire1.onRequest(uplink_i2c_request_cb);

	DEBUG_INIT();
	DEBUG_PRINTLN("TRXVU SIM Started.");
	DEBUG_FLUSH();
}

void loop(void)
{
	const uint8_t *data;
	size_t datalen;
	static int dbg_count = 0;

	// Check if we received KISS frame to uplink
	if (SerialUSB.available()) {
		int c = SerialUSB.read();
		if ((c >= 0) && (c <= 255)) {
			uint8_t has_full_frame = kiss_decode_provide((uint8_t) c);

			if (has_full_frame) {
				kiss_decode_fetch(&data, &datalen);
				if (data != NULL) {
					(void)ul_command_handler_provide_frame(data, datalen);
				}
			}
		}
	}

	// Check if we received a frame from downlink
	// (only if we have nothing that is buffered!)
	if (kiss_encode_buffer_length == 0) {
		if (dl_beacon_interval() != 0) {
			if ((millis() - beacon_poll_time) > dl_beacon_interval() * 1000) {
				dl_command_handler_poll_beacon_frame(&data, &datalen);
				beacon_poll_time = millis();
			} else {
				data = NULL;
			}
		} else {
			dl_command_handler_poll_frame(&data, &datalen);
		}
		if (data != NULL) {
			size_t kisslen;

			// Strip first flag byte and last 3 CRC + flag bytes
			kiss_encode(kiss_encode_buffer,
			            &kiss_encode_buffer_length,
			            &data[1],
			            datalen - 4);
		}
	}

	if (kiss_encode_buffer_length > 0) {
		if (SerialUSB.availableForWrite()) {
			SerialUSB.write(kiss_encode_buffer, kiss_encode_buffer_length);

			DEBUG_PRINT("Wrote KISS frame (");
			DEBUG_PRINTINT(dbg_count++);
			DEBUG_PRINTLN("):");
			DEBUG_PRINTBYTES(kiss_encode_buffer, kiss_encode_buffer_length);
			DEBUG_PRINTLN("");
			DEBUG_FLUSH();

			kiss_encode_buffer_length = 0;
		}
	}

	/* TODO: Check state variables from uplink/downlink command handlers */

	ftcmd_fire();

	if (TM_OVER_SERIAL) {
		trxvu_tm_reply_cb();
	} else {
		delay(30);
	}
}

/**
 * Downlink I2C receive Callback
 */
void downlink_i2c_receive_cb(int len)
{
	int ret;
	size_t datalen = 0;

	(void) len;

	while (Wire.available()) {
		if (datalen < sizeof(downlink_receive_buffer))
			downlink_receive_buffer[datalen++] = Wire.read();
		else
			(void) Wire.read();
	}

	ret = dl_command_handler_receive_i2c(downlink_receive_buffer, datalen);

	/* TODO: Handle return code here. */
}

/**
 * Downlink I2C request callback.
 */
void downlink_i2c_request_cb(void)
{
	const uint8_t *data;
	size_t datalen;

	dl_command_handler_get_i2c_response(&data, &datalen);
	if (data == NULL) {
		const uint8_t tmpdata[] = {0xff};
		Wire.write(tmpdata, 1);
	} else {
		Wire.write(data, datalen);
	}
}

/**
 * Uplink I2C receive Callback
 */
void uplink_i2c_receive_cb(int len)
{
	int ret;
	size_t datalen = 0;

	(void) len;

	while (Wire1.available()) {
		if (datalen < sizeof(uplink_receive_buffer))
			uplink_receive_buffer[datalen++] = Wire1.read();
		else
			(void) Wire1.read();
	}

	ret = ul_command_handler_receive_i2c(uplink_receive_buffer, datalen);

	/* TODO: Handle return code here. */
}

/**
 * Uplink I2C request callback.
 */
void uplink_i2c_request_cb(void)
{
	const uint8_t *data;
	size_t datalen;

	ul_command_handler_get_i2c_response(&data, &datalen);
	if (data == NULL) {
		const uint8_t tmpdata[] = {0xff};
		Wire1.write(tmpdata, 1);
	} else {
		Wire1.write(data, datalen);
	}
}
