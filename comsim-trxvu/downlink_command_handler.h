/**
 * @file   downlink_command_handler.h
 * @author John Wikman and William Stackenäs
 */

#ifndef DOWNLINK_COMMAND_HANDLER_H
#define DOWNLINK_COMMAND_HANDLER_H

#include <inttypes.h>
#include <stdlib.h>

#define DL_DEFAULT_BITRATE       (1200)
#define DL_DEFAULT_FROM_CALLSIGN ("MIST  ")
#define DL_DEFAULT_TO_CALLSIGN   ("SA0SAT")

#define DL_MAX_FRAMES       (40)
#define DL_MAX_PAYLOAD_SIZE (235)
#define DL_MAX_FRAME_SIZE   (DL_MAX_PAYLOAD_SIZE + 20)

unsigned long dl_watchdog_last_kick();
uint8_t dl_soft_reset_was_triggered();
uint8_t dl_hard_reset_was_triggered();
uint8_t dl_idle_state_on();
uint16_t dl_beacon_interval();
char* dl_to_callsign();
char* dl_from_callsign();
unsigned int dl_bitrate();

void dl_command_handler_init(void);
void dl_command_handler_poll_frame(const uint8_t **buf, size_t *len);
void dl_command_handler_poll_beacon_frame(const uint8_t **buf, size_t *len);
void dl_command_handler_get_i2c_response(const uint8_t **buf, size_t *len);
int dl_command_handler_receive_i2c(const uint8_t *data, size_t datalen);

#endif /* DOWNLINK_COMMAND_HANDLER_H */
