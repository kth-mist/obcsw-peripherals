/**
 * @file   downlink_command_codes.h
 * @author John Wikman and William Stackenäs
 */

#ifndef DOWNLINK_COMMAND_CODES_H
#define DOWNLINK_COMMAND_CODES_H

#define DLCC_WATCHDOG_RESET                  (0xCC)
#define DLCC_SOFTWARE_RESET                  (0xAA)
#define DLCC_HARDWARE_SYSTEM_RESET           (0xAB)
#define DLCC_SEND_FRAME                      (0x10)
#define DLCC_SEND_FRAME_OVERRIDE_CALLSIGN    (0x11)
#define DLCC_SET_BEACON                      (0x14)
#define DLCC_SET_BEACON_OVERRIDE_CALLSIGN    (0x15)
#define DLCC_CLEAR_BEACON                    (0x1F)
#define DLCC_SET_DEFAULT_TO_CALLSIGN         (0x22)
#define DLCC_SET_DEFAULT_FROM_CALLSIGN       (0x23)
#define DLCC_SET_TRANSMITTER_IDLE_STATE      (0x24)
#define DLCC_MEASURE_ALL_TELEMETRY_CHANNELS  (0x25)
#define DLCC_MEASURE_LAST_TELEMETRY_CHANNELS (0x26)
#define DLCC_SET_BITRATE                     (0x28)
#define DLCC_REPORT_TRANSMITTER_UPTIME       (0x40)
#define DLCC_REPORT_TRANSMITTER_STATE        (0x41)

#endif /* DOWNLINK_COMMAND_CODES_H */
