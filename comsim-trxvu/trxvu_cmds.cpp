/**
 * @file trxvu_cmds.cpp
 */

#include <Arduino.h>
#include <string.h>
#include <stdint.h>

#include "ftcmd.h"

#include "trxvu_cmds.h"

extern "C" {
	#include "uplink_command_handler.h"
	#include "downlink_command_handler.h"
}

#define ARDUINO_UNIT_ID ARDUINO_UNIT_ID_TRXVU

static uint8_t ft_ax25_buf[DL_MAX_FRAME_SIZE];
static char ft_reply_buf[(UL_MAX_FRAME_SIZE << 1) + 10]; // to make room for ft syntax overheads

static void cmd_tc_cb(const char **argv, unsigned int argn);
static void unknown_cmd_cb(const char **argv, unsigned int argn);

static const ftcmd_cmd_t cmd_table[] = {
	{ "tc", cmd_tc_cb },
	{ NULL, NULL },
};

static uint8_t char_to_hex(char c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	else if (c >= 'a' && c <= 'f')
		return c + 10 - 'a';
	else if (c >= 'A' && c <= 'F')
		return c + 10 - 'A';

	return 0xff;
}

static char hex_to_char(uint8_t h)
{
	h &= 0xF;

	if (h <= 9)
		return h + '0';

	return h - 10 + 'a';
}

void trxvu_setup(void)
{
	dl_command_handler_init();
	ul_command_handler_init();

	ftcmd_init(cmd_table, unknown_cmd_cb);
}

int trxvu_recv_frame(uint8_t *data, size_t size)
{
	int err;

	if (data == NULL || size < 1 || size > UL_MAX_FRAME_SIZE)
		return 1;

	err = ul_command_handler_provide_frame(data, size);
	if (err != 0)
		return 2;

	return 0;
}

int trxvu_send_frame(uint8_t *data, size_t max_size, size_t* size)
{
	if (data == NULL || max_size < 1 || size == NULL)
		return 1;

	const uint8_t *buf;
	size_t buflen;

	if (dl_beacon_interval() != 0)
		dl_command_handler_poll_beacon_frame(&buf, &buflen);
	else
		dl_command_handler_poll_frame(&buf, &buflen);
	if (buf == NULL)
		return 2;

	if (buflen < max_size)
		*size = buflen;
	else
		*size = max_size;

	memcpy(data, buf, *size);
	return 0;
}

void trxvu_tm_reply_cb(void)
{
	int err;
	size_t len;
	unsigned int j;
	char c1, c2;

	// Print any available telemetry in the TRXVU downlink buffer
	err = trxvu_send_frame(ft_ax25_buf, DL_MAX_FRAME_SIZE, &len);
	if (err == 0) {
		strcpy(ft_reply_buf, "+tm;");
		for (j = 0; j < len; j++) {
			c1 = hex_to_char((ft_ax25_buf[j] >> 4) & 0xf);
			c2 = hex_to_char(ft_ax25_buf[j] & 0xf);

			ft_reply_buf[4 + (j << 1)] = c1;
			ft_reply_buf[5 + (j << 1)] = c2;
		}
		ft_reply_buf[4 + (j << 1)] = '\0';

		Serial.println(ft_reply_buf);
	} else if (err != 2) {
		Serial.print("trxvu_send_frame returned ");
		Serial.println(err);
	}
	// If err was 2, then there was no telemetry to send
}

static void cmd_tc_cb_respond(int err)
{
	Serial.print("+tc;");
	Serial.println(err);
}

static void cmd_tc_cb(const char **argv, unsigned int argn)
{
	int err;
	size_t j;
	size_t len;
	const char *hex;
	uint8_t low, high;

	if (argn != 2) {
		Serial.println("tc: requires one argument");
		cmd_tc_cb_respond(2);
		return;
	}

	hex = argv[1];
	len = strlen(hex);
	if (len == 0) {
		Serial.println("tc: empty hex string parameter");
		cmd_tc_cb_respond(2);
		return;
	}
	if (len & 1) {
		Serial.print("tc: hex string parameter of odd length (");
		Serial.print(len);
		Serial.println(" bytes)");
		Serial.println(hex);
		cmd_tc_cb_respond(3);
		return;
	}
	if (len < (16 << 1) || len >= (UL_MAX_FRAME_SIZE << 1)) {
		Serial.print("tc: too long or too short raw hex parameter (");
		Serial.print(len);
		Serial.println(" bytes)");
		cmd_tc_cb_respond(4);
		return;
	}

	for (j = 0; (j << 1) + 1 < len; j++) {
		high = char_to_hex(hex[j << 1]);
		low = char_to_hex(hex[(j << 1) + 1]);

		if (high > 0xf || low > 0xf) {
			Serial.print("tc: bad hex value ");
			Serial.print(hex[j]);
			Serial.println(hex[j + 1]);
			cmd_tc_cb_respond(5);
			return;
		}
		ft_ax25_buf[j] = (high << 4) | low;
	}

	err = trxvu_recv_frame(ft_ax25_buf, j);
	if (err != 0) {
		Serial.print("trxvu_recv_frame returned ");
		Serial.println(err);
		cmd_tc_cb_respond(6);
		return;
	}

	cmd_tc_cb_respond(0);
}

static void unknown_cmd_cb(const char **argv, unsigned int argn)
{
	(void) argn;

	Serial.print("Invalid cmd: ");
	Serial.println(argv[0]);
}
