/**
 * @file   downlink_command_handler.c
 * @author John Wikman and William Stackenäs
 */

#include <Arduino.h>

#include <inttypes.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "ax25.h"
#include "downlink_command_codes.h"
#include "downlink_command_handler.h"
#include "utils.h"

static uint8_t tx_buffer[DL_MAX_FRAMES][DL_MAX_FRAME_SIZE];
static size_t tx_buffer_length[DL_MAX_FRAMES];
static size_t tx_head;
static size_t tx_bufcount;

static uint8_t beacon_buffer[DL_MAX_FRAME_SIZE];
static size_t beacon_length;
static uint16_t beacon_interval;

static char to_callsign[7];
static char from_callsign[7];

static uint8_t response_data[DL_MAX_FRAME_SIZE];
static size_t response_length;

/* Time in milliseconds since the downlink was initialized */
static unsigned long starttime;

/* Time (in milliseconds since startup) of last watchdog reset */
static unsigned long watchdog_reset_time;

/* Boolean variable specifying a soft reset */
static uint8_t indicator_soft_reset;

/* Boolean variable specifying a hard reset */
static uint8_t indicator_hard_reset;

/* Boolean variable specifying if transmitter should be turned on during idle */
static uint8_t indicator_idle_state_on;

/* Radio bitrate in bits per second. Should be used to determine the delay period in main loop. */
static unsigned int bitrate;

unsigned long dl_watchdog_last_kick()
{
	return watchdog_reset_time;
}

/**
 * Returns whether the TRXVU DL was soft reset and clears the
 * soft reset flag
 */
uint8_t dl_soft_reset_was_triggered()
{
	uint8_t triggered = indicator_soft_reset;
	indicator_soft_reset = 0;
	return triggered;
}

/**
 * Returns whether the TRXVU DL was hard reset and clears the
 * hard reset flag
 */
uint8_t dl_hard_reset_was_triggered()
{
	uint8_t triggered = indicator_hard_reset;
	indicator_hard_reset = 0;
	return triggered;
}

uint8_t dl_idle_state_on()
{
	return indicator_idle_state_on;
}

uint16_t dl_beacon_interval()
{
	return beacon_interval;
}

unsigned int dl_bitrate()
{
	return bitrate;
}

char* dl_to_callsign()
{
	return to_callsign;
}

char* dl_from_callsign()
{
	return from_callsign;
}

/**
 * Initializes/resets the downlink command handler.
 */
void dl_command_handler_init(void)
{
	tx_head = 0;
	tx_bufcount = 0;
	beacon_length = 0;
	beacon_interval = 0;
	strcpy(to_callsign, DL_DEFAULT_TO_CALLSIGN);
	strcpy(from_callsign, DL_DEFAULT_FROM_CALLSIGN);
	response_length = 0;
	starttime = millis();
	watchdog_reset_time = millis();
	indicator_soft_reset = 0;
	indicator_hard_reset = 0;
	indicator_idle_state_on = 0;
	bitrate = DL_DEFAULT_BITRATE;
}

void dl_command_handler_poll_frame(const uint8_t **buf, size_t *len)
{
	if (tx_bufcount == 0) {
		*buf = NULL;
		*len = 0;
	} else {
		*buf = tx_buffer[tx_head];
		*len = tx_buffer_length[tx_head];
		tx_head++;
		if (tx_head >= DL_MAX_FRAMES)
			tx_head = 0;

		tx_bufcount--;
	}
}

void dl_command_handler_poll_beacon_frame(const uint8_t **buf, size_t *len)
{
	if (beacon_length == 0) {
		*buf = NULL;
	} else {
		*buf = beacon_buffer;
	}
	*len = beacon_length;
}

void dl_command_handler_get_i2c_response(const uint8_t **buf, size_t *len)
{
	if (response_length == 0) {
		*buf = NULL;
		*len = 0;
	} else {
		*buf = response_data;
		*len = response_length;
	}
}

/**
 * Handle incoming I2C transmissions to TRXVU downlink. First byte of received
 * data indicates an opcode.
 */
int dl_command_handler_receive_i2c(const uint8_t *data, size_t datalen)
{
	int ret = 0;
	uint8_t command_code = data[0];
	size_t slot = (tx_head + tx_bufcount) % DL_MAX_FRAMES;
	unsigned long uptime;

	switch (command_code) {
	case DLCC_WATCHDOG_RESET:
		watchdog_reset_time = millis();
		response_length = 0;
		break;
	case DLCC_SOFTWARE_RESET:
		indicator_soft_reset = 1;
		response_length = 0;
		break;
	case DLCC_HARDWARE_SYSTEM_RESET:
		indicator_hard_reset = 1;
		response_length = 0;
		break;
	case DLCC_SEND_FRAME:
		/* TODO: Describe format here and check length */
		if (tx_bufcount >= DL_MAX_FRAMES) {
			response_data[0] = 0xFF;
			response_length = 1;
		} else {
			ax25_encode(tx_buffer[slot],
			            &tx_buffer_length[slot],
			            &data[1],
			            datalen - 1,
			            to_callsign,
			            from_callsign);
			tx_bufcount++;

			response_data[0] = (uint8_t) ((DL_MAX_FRAMES - tx_bufcount) & 0xff);
			response_length = 1;
		}
		break;
	case DLCC_SEND_FRAME_OVERRIDE_CALLSIGN:
		/* TODO: Describe format here and check length */
		if (tx_bufcount >= DL_MAX_FRAMES) {
			response_data[0] = 0xFF;
			response_length = 1;
		} else {
			ax25_encode(tx_buffer[slot],
			            &tx_buffer_length[slot],
			            &data[15],
			            datalen - 15,
			            (const char *) &data[1],
			            (const char *) &data[8]);
			tx_bufcount++;

			response_data[0] = (uint8_t) ((DL_MAX_FRAMES - tx_bufcount) & 0xff);
			response_length = 1;
		}
		break;
	case DLCC_SET_BEACON:
		/* TODO: Describe format here and check length */
		beacon_interval = data[1];
		beacon_interval |= ((data[2] & 0x0f) << 8);
		if (beacon_interval > 3000)
			beacon_interval = 3000;

		ax25_encode(beacon_buffer,
		            &beacon_length,
		            &data[3],
		            datalen - 3,
		            to_callsign,
		            from_callsign);

		response_length = 0;
		break;
	case DLCC_SET_BEACON_OVERRIDE_CALLSIGN:
		/* TODO: Describe format here and check length */
		beacon_interval = data[1];
		beacon_interval |= ((data[2] & 0x0f) << 8);
		if (beacon_interval > 3000)
			beacon_interval = 3000;

		ax25_encode(beacon_buffer,
		            &beacon_length,
		            &data[17],
		            datalen - 17,
		            (const char *) &data[3],
		            (const char *) &data[10]);

		response_length = 0;
		break;
	case DLCC_CLEAR_BEACON:
		beacon_length = 0;
		beacon_interval = 0;
		response_length = 0;
		break;
	case DLCC_SET_DEFAULT_TO_CALLSIGN:
		/* TODO: Describe format here and check length */
		memcpy(to_callsign, &data[1], 7);
		response_length = 0;
		break;
	case DLCC_SET_DEFAULT_FROM_CALLSIGN:
		/* TODO: Describe format here and check length */
		memcpy(from_callsign, &data[1], 7);
		response_length = 0;
		break;
	case DLCC_SET_TRANSMITTER_IDLE_STATE:
		/* TODO: Describe format here and check length */
		if ((data[1] & 0x01) == 0x01)
			indicator_idle_state_on = 1;
		else
			indicator_idle_state_on = 0;
		response_length = 0;
		break;
	case DLCC_MEASURE_ALL_TELEMETRY_CHANNELS:
		/* TODO: Describe format here and check length */
		u16tole(&response_data[0], 826); // reflected power
		u16tole(&response_data[2], 9120); // forward power
		u16tole(&response_data[4], 51); // power bus voltage
		u16tole(&response_data[6], 1662); // total supply current
		u16tole(&response_data[8], 627); // power amplifier temp
		u16tole(&response_data[10], 916); // local oscillator temperature
		response_length = 12;
		break;
	case DLCC_MEASURE_LAST_TELEMETRY_CHANNELS:
		/* TODO: Describe format here and check length */
		u16tole(&response_data[0], 826); // reflected power
		u16tole(&response_data[2], 9120); // forward power
		u16tole(&response_data[4], 51); // power bus voltage
		u16tole(&response_data[6], 1662); // total supply current
		u16tole(&response_data[8], 627); // power amplifier temp
		u16tole(&response_data[10], 916); // local oscillator temperature
		response_length = 12;
		break;
	case DLCC_SET_BITRATE:
		/* TODO: Describe format here and check length */
		switch (data[1]) {
		case 0x01:
			bitrate = 1200;
			break;
		case 0x02:
			bitrate = 2400;
			break;
		case 0x04:
			bitrate = 4800;
			break;
		case 0x08:
			bitrate = 9600;
			break;
		default:
			break;
		}
		response_length = 0;
		break;
	case DLCC_REPORT_TRANSMITTER_UPTIME:
		/* TODO: Describe format here and check length */
		uptime = (millis() - starttime) / 1000;
		u32tole(&response_data[0], (uint32_t) uptime);
		response_length = 4;
		break;
	case DLCC_REPORT_TRANSMITTER_STATE:
		/* TODO: Describe format here and check length */
		response_data[0] = 0;
		if (indicator_idle_state_on)
			response_data[0] |= 0x01;
		if (beacon_length > 0)
			response_data[0] |= 0x02;

		switch (bitrate) {
		case 2400:
			response_data[0] |= 0x04;
			break;
		case 4800:
			response_data[0] |= 0x08;
			break;
		case 9600:
			response_data[0] |= 0x0c;
			break;
		default:
			break;
		}

		response_length = 1;
		break;
	default:
		ret = 1;
		break;
	}

	return ret;
}
