/**
 * @file   utils.h
 * @author John Wikman and William Stackenäs
 *
 * Common functionality that is useful throughout the simulator.
 */

#ifndef UTILS_H
#define UTILS_H

#include <inttypes.h>

void u16tole(uint8_t *dst, uint16_t value);
void u16tobe(uint8_t *dst, uint16_t value);

void u32tole(uint8_t *dst, uint32_t value);
void u32tobe(uint8_t *dst, uint32_t value);

uint32_t letou32(const uint8_t *src);
uint32_t betou32(const uint8_t *src);

#endif /* UTILS_H */
