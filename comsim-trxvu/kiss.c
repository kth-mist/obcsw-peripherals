/**
 * @file kiss.c
 */

#include <Arduino.h>

#include <inttypes.h>
#include <stdlib.h>

#include "kiss.h"

#define FEND  (0xc0)
#define FESC  (0xdb)
#define TFEND (0xdc)
#define TFESC (0xdd)

static uint8_t recvbuf[515];
static size_t recvlen = 0;

static uint8_t decodebuf[256];
static size_t decodelen = 0;

/**
 * Encodes a KISS packet. To be on the safe side, the region pointed to by
 * dst should have a capacity of at least (srclen * 2) + 3 bytes.
 */
void kiss_encode(uint8_t *dst,
                 size_t *dstlen,
                 const uint8_t *src,
                 size_t srclen)
{
	size_t i;
	size_t len;

	len = 0;

	dst[len++] = FEND;
	dst[len++] = 0x00; // data frame

	for (i = 0; i < srclen; i++) {
		/* Escape KISS delimiters (if any) */
		switch (src[i]) {
		case FEND:
			dst[len++] = FESC;
			dst[len++] = TFEND;
			break;
		case FESC:
			dst[len++] = FESC;
			dst[len++] = TFESC;
			break;
		default:
			dst[len++] = src[i];
			break;
		}
	}

	dst[len++] = FEND;

	*dstlen = len;
}

/**
 * Returns true if we have received a full KISS frame.
 */
uint8_t kiss_decode_provide(uint8_t data)
{
	uint8_t ret = 0;

	// If we received way too much data, clear the buffer.
	if (recvlen >= sizeof(recvbuf))
		recvlen = 0;

	switch (recvlen) {
	case 0:
		if (data != FEND)
			recvlen = 0;
		else
			recvbuf[recvlen++] = data;
		break;
	case 1:
		if (data != 0x00) // data frame
			recvlen = 0;
		else
			recvbuf[recvlen++] = data;
		break;
	default:
		recvbuf[recvlen++] = data;
		if (data == FEND)
			ret = 1; // indicate that we have a full frame
		break;
	}

	return ret;
}

/**
 * Returns a decoded KISS frame.
 */
void kiss_decode_fetch(const uint8_t **data, size_t *datalen)
{
	size_t i;

	if (recvlen == 0) {
		*data = NULL;
		*datalen = 0;
		return;
	}

	decodelen = 0;

	for (i = 2; i < (recvlen - 1); i++) {
		switch (recvbuf[i]) {
		case FESC:
			i++;
			switch (recvbuf[i]) {
			case TFEND:
				decodebuf[decodelen++] = FEND;
				break;
			case TFESC:
				decodebuf[decodelen++] = FESC;
				break;
			default:
				decodebuf[decodelen++] = recvbuf[i];
				break;
			}
			break;
		default:
			decodebuf[decodelen++] = recvbuf[i];
			break;
		}
	}

	*data = decodebuf;
	*datalen = decodelen;

	recvlen = 0;
}
