/**
 * @file kiss.h
 */

#ifndef KISS_H
#define KISS_H

#include <inttypes.h>
#include <stdlib.h>

void kiss_encode(uint8_t *dst,
                 size_t *dstlen,
                 const uint8_t *src,
                 size_t srclen);

uint8_t kiss_decode_provide(uint8_t data);
void kiss_decode_fetch(const uint8_t **data, size_t *datalen);

#endif /* KISS_H */
