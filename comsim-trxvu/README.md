# Radio Simulator

This folder contains a full TRXVU + RF Checkout Box simulator that runs on
a single Arduino Due. The simulator shall be connected to the computer running
Elveti using the Native USB port and to the iOBC using both of the I2C
connections on the Arduino Due.

## Setup
This setup process assumes the following:
 - You are able to compile and flash Arduino Due software.
 - You have access to an Arduino Due.
 - You have access to a laptop with Elveti and VPSE installed.
 - You have the necessary cables and wires to connect the devices.

### 1. Compiling the simulator code
Before you can compile the simulator, you must first increase the default
buffer size for I2C communications to at least 245 bytes. This is done
by opening up the **Wire.h** file and changing the `BUFFER_LENGTH` constant
**from 32 to 245**.

On Arch Linux you can find the Wire.h file under
`$HOME/.arduino15/packages/arduino/hardware/sam/<version>/libraries/Wire/src`.
It should probably look quite similar on other systems. For example, on
Windows it can be found under `C:\Users\<user>\AppData\Local` followed by the
relative path from `$HOME` on Arch Linux.

Now compile and flash the simulator onto the Arduino Due.

### 2. Connecting the simulator hardware
Connect the **SDA (20)** and **SDA1** pins on the Arduino to the SDA line
coming from the iOBC (ISIS on-board computer). Connect the **SCL (21)** and
**SCL1** pins on the Arduino to the SCL line coming from the iOBC. Also make
sure that you connect the Arduino to ground. **The Arduino can experience
frequent I2C bit-errors if not connected to ground!**

By default, the simulator uses the I2C address 0x63 for the downlink and
0x62 for the uplink. This can be changed to the same addresses used in the
real TRXVU, 0x61 for the downlink and 0x60 for the uplink, by defining
`USE_REAL_TRXVU_I2C_ADDRESSES`.

Connect the **Native USB** port on the Arduino to any of the USB ports on the
Elveti laptop. As you may notice, there are two USB ports on the Arduino. The
Native USB port is the one closest to the reset button (the "left" port).

### 3. Configuring the Elveti laptop
First, make sure that all instances of Elveti are closed.

Check the **Device Manager** in Windows and see which COM port you connected
the Arduino to. Under the COM submenu you should find something along the lines
of **Arduino Due (COM#)** where **#** is a placeholder for its number. Let
**COM#** be the COM port that was connected to your Arduino Due.

Open up **VSPE**. Add a **Connector** and give it a COM port, lets say
**COM11**. Then add a **Serial Redirector** between **COM#** and **COM11**. Make
sure that the _Redirect modem registers_ option in the Serial Redirector is not
checked. Now start the VSPE emulation if not already started.

Open up `GroundStationManager.exe.config` and set the following values:
 - **UplinkTncComPort**: `COM11` (or which ever COM port you set as the Connector in VSPE)
 - **DownlinkTncComPort**: `UPLINK`
 - **UplinkTncType**: `Generic`
 - **GenericTncBaudrate**: `19200`

Open up `FrontEnd.exe.config` and set the following values:
 - **AX25SrcCallSign**: `SA0SAT`
 - **AX25SrcSSID**: `0`
 - **AX25DstCallSign**: `MIST  ` (note the 2 spaces after the T)
 - **AX25DstSSID**: `0`
 - **UseTCSegmentation**: `true`

The .config files are located somewhere under the Elveti folder (usually right
next to the .exe files).

Now you can start Elveti and the iOBC and the communication should work between
the two.
