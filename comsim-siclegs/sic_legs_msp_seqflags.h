/**
 * @file      sic_legs_msp_seqflags.h
 * @author    John Wikman
 * @copyright MIT License
 * @brief     Functionalities for keeping track of transaction-ID's in MSP.
 */

#ifndef SIC_LEGS_MSP_SEQFLAGS_H
#define SIC_LEGS_MSP_SEQFLAGS_H

/**
 * @brief A type for handling sequence flags in MSP.
 * 
 * The fields in the struct should never be accessed or modified directly. Use
 * the handler functions declared in sic_legs_msp_seqflags.h to interract with this
 * type.
 */
typedef struct {
	/**
	 * @brief Bit vector for keeping track of sequence flag values.
	 */
	unsigned short values[4];

	/**
	 * @brief Bit vector for checking if a sequence flag has been initialized or not.
	 */
	unsigned short inits[4];
} sic_legs_msp_seqflags_t;


sic_legs_msp_seqflags_t sic_legs_msp_seqflags_init(void);
int sic_legs_msp_seqflags_increment(volatile sic_legs_msp_seqflags_t *flags, unsigned char opcode);
int sic_legs_msp_seqflags_get(volatile const sic_legs_msp_seqflags_t *flags, unsigned char opcode);
int sic_legs_msp_seqflags_get_next(volatile const sic_legs_msp_seqflags_t *flags, unsigned char opcode);
int sic_legs_msp_seqflags_is_set(volatile const sic_legs_msp_seqflags_t *flags, unsigned char opcode, unsigned char flag);
int sic_legs_msp_seqflags_set(volatile sic_legs_msp_seqflags_t *flags, unsigned char opcode, unsigned char flag);

#endif /* SIC_LEGS_MSP_SEQFLAGS_H */
