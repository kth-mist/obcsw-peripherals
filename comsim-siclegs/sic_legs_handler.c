/**
 * @file      sic_legs_handler.c
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     SiC LEGS experiment handler functions
 */

#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include <Arduino.h>

#include "sic_legs_defines.h"
#include "sic_legs_handler.h"
#include "sic_legs_msp_exp.h"

static volatile uint8_t legs_started;
static volatile uint8_t sic_started;
static volatile uint8_t powered_off;

static uint16_t rand_uint16(uint16_t min, uint16_t max);

void sic_legs_setup()
{
	legs_started = 0;
	sic_started = 0;
	powered_off = 0;

	// Initializing the MSP sequence flags is skipped since it would desync
	// with the OBC if this function is called more than once
}

/**
 * @brief Send callback for MSP communication.
 *
 * @param[in] opcode The MSP opcode that should be handled.
 * @param[out] data A buffer that should be filled with data to send. It
 *                  must be at least SIC_LEGS_MSP_SENDBUF_SIZE bytes large.
 * @param[out] len Where to store the amount of bytes to send.
 */
void sic_legs_handler_send(uint8_t opcode, uint8_t *data, size_t *len)
{
	uint8_t b;

	switch (opcode) {
	case SIC_LEGS_MSP_OP_REQ_PIEZO:
		if (legs_started) {
			// Return static length to make it easier to test
			*len = PIEZO_PAYLOAD_SIZE;
			// Return pseudo-random data with the opcode as the seed
			b = SIC_LEGS_MSP_OP_REQ_PIEZO;
			for (size_t i = 0; i < *len; i++) {
				data[i] = b;
				b *= 73;
			}
			legs_started = 0;
		} else {
			*len = 0;
		}
		break;
	case SIC_LEGS_MSP_OP_REQ_SIC:
		if (sic_started) {
			*len = SIC_PAYLOAD_SIZE;
			// Return pseudo-random data with the opcode as the seed
			b = SIC_LEGS_MSP_OP_REQ_SIC;
			for (size_t i = 0; i < *len; i++) {
				data[i] = b;
				b *= 67;
			}
			sic_started = 0;
		} else {
			*len = 0;
		}
		break;
	default:
		*len = 0;
		break;
	}
}

/**
 * @brief Receive callback for MSP communication.
 *
 * @param[in] opcode The MSP opcode that should be handled.
 * @param[out] data A buffer containing the bytes that were received.
 * @param[out] len The amount of bytes that was received.
 */
void sic_legs_handler_recv(uint8_t opcode, const uint8_t *data, size_t len)
{
	(void) opcode;
	(void) data;
	(void) len;
}

/**
 * @brief Sys callback for MSP communication.
 *
 * @param[in] opcode The MSP opcode that should be handled.
 */
void sic_legs_handler_sys(uint8_t opcode)
{
	switch (opcode) {
	case SIC_LEGS_MSP_OP_POWER_OFF:
		legs_started = 0;
		sic_started = 0;
		powered_off = 1;
		break;
	case SIC_LEGS_MSP_OP_START_EXP_PIEZO:
		legs_started = 1;
		break;
	case SIC_LEGS_MSP_OP_STOP_EXP_PIEZO:
		legs_started = 0;
		break;
	case SIC_LEGS_MSP_OP_START_EXP_SIC:
		sic_started = 1;
		break;
	case SIC_LEGS_MSP_OP_STOP_EXP_SIC:
		sic_started = 0;
		break;
	default:
		break;
	}
}

uint8_t sic_legs_powered_off()
{
	return powered_off;
}

static uint16_t rand_uint16(uint16_t min, uint16_t max)
{
	if (min >= max)
		return min;
	return (uint16_t) ((rand() % (max - min)) + min);
}
