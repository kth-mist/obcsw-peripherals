/**
 * @file      sic_legs_msp_endian.h
 * @author    John Wikman 
 * @copyright MIT License
 * @brief     Functions for endian conversion.
 *
 * @details
 * Declares function prototypes for converting a byte sequence from Big-Endian
 * into system defined integers.
 */

#ifndef SIC_LEGS_MSP_ENDIAN_H
#define SIC_LEGS_MSP_ENDIAN_H

void sic_legs_msp_to_bigendian32(unsigned char *dest, unsigned long number);
unsigned long sic_legs_msp_from_bigendian32(const unsigned char *src);

#endif /* SIC_LEGS_MSP_ENDIAN_H */
