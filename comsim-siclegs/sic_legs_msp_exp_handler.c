/**
 * @file      sic_legs_msp_exp_handler.c
 * @author    William Stackenäs
 * @brief     Non-arduino version of SiC LEGS simulator with MSP callbacks
 *
 * @details
 * Custom functions and structures used within cubes
 */
 
#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include "sic_legs_msp_exp_handler.h"
#include "sic_legs_defines.h"
#include "sic_legs_handler.h"

static size_t recvlen;
static size_t recvidx;
static uint8_t recvbuf[SIC_LEGS_MSP_RECVBUF_SIZE];

static size_t sendlen;
static size_t sendidx;
static uint8_t sendbuf[SIC_LEGS_MSP_SENDBUF_SIZE];

void sic_legs_msp_expsend_start(unsigned char opcode, unsigned long *len)
{
	if (len == NULL)
		return;

	sic_legs_handler_send(opcode, sendbuf, &sendlen);

	// Truncate data if too large
	if (sendlen > SIC_LEGS_MSP_SENDBUF_SIZE)
		sendlen = SIC_LEGS_MSP_SENDBUF_SIZE;

	sendidx = 0;

	*len = sendlen;
}

void sic_legs_msp_expsend_data(unsigned char opcode,
                            unsigned char *buf,
                            unsigned long len,
                            unsigned long offset)
{
	(void) opcode;

	if (buf == NULL)
		return;

	memcpy(buf, &sendbuf[offset], len);

	// Update index
	sendidx = offset + len;
}

void sic_legs_msp_expsend_complete(unsigned char opcode)
{
	if (sendidx != sendlen) {
		// Did not send all of the data, should probably warn the user!
		// TODO...
	}
	return;
}

void sic_legs_msp_expsend_error(unsigned char opcode, int error)
{
	fprintf(stderr, "[SiC LEGS] Send error: %d\n", error);
	return;
}

void sic_legs_msp_exprecv_start(unsigned char opcode, unsigned long len)
{
	(void) opcode;

	recvlen = len;
	recvidx = 0;
}

void sic_legs_msp_exprecv_data(unsigned char opcode,
                            const unsigned char *buf,
                            unsigned long len,
                            unsigned long offset)
{
	// Truncate received data
	if (offset >= SIC_LEGS_MSP_RECVBUF_SIZE)
		return;

	if ((offset + len) > SIC_LEGS_MSP_RECVBUF_SIZE)
		len = SIC_LEGS_MSP_RECVBUF_SIZE - offset;

	memcpy(&recvbuf[offset], buf, len);
	recvidx = offset + len;
}

void sic_legs_msp_exprecv_complete(unsigned char opcode)
{
	if (recvidx != recvlen) {
		// Did not receive all of the data, should probably warn the user!
		// TODO...
	}
	sic_legs_handler_recv(opcode, recvbuf, recvlen);
}

void sic_legs_msp_exprecv_error(unsigned char opcode, int error)
{
	fprintf(stderr, "[SiC LEGS] Receive error: %d\n", error);
	return;
}

void sic_legs_msp_exprecv_syscommand(unsigned char opcode)
{
	sic_legs_handler_sys(opcode);
}
