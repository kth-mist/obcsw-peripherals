/**
 * @file      sic_legs_defines.h
 * @author    William Stackenäs
 * @brief     Custom defines used for SiC LEGS
 */

#ifndef SIC_LEGS_DEFINES_H
#define SIC_LEGS_DEFINES_H

/* MSP Custom Commands */
#define SIC_LEGS_MSP_OP_START_EXP_PIEZO        0x50
#define SIC_LEGS_MSP_OP_STOP_EXP_PIEZO         0x51
#define SIC_LEGS_MSP_OP_START_EXP_SIC          0X52
#define SIC_LEGS_MSP_OP_STOP_EXP_SIC           0x53

#define SIC_LEGS_MSP_OP_REQ_PIEZO              0x60
#define SIC_LEGS_MSP_OP_REQ_SIC                0x61

#define PIEZO_PAYLOAD_SIZE          400
#define SIC_PAYLOAD_SIZE            448

#define SIC_LEGS_MSP_RECVBUF_SIZE  (1)
#define SIC_LEGS_MSP_SENDBUF_SIZE  (SIC_PAYLOAD_SIZE)

#endif /* SIC_LEGS_DEFINES_H */
