/**
 * @file      sic_legs_handler.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     SiC LEGS state utility functions
 */
#ifndef SIC_LEGS_EXP_HANDLER_H
#define SIC_LEGS_EXP_HANDLER_H

#include <stdint.h>

void sic_legs_setup();
uint8_t sic_legs_powered_off();

void sic_legs_handler_send(uint8_t opcode, uint8_t *data, size_t *len);
void sic_legs_handler_recv(uint8_t opcode, const uint8_t *data, size_t len);
void sic_legs_handler_sys(uint8_t opcode);

#endif /* SIC_LEGS_EXP_HANDLER_H */
