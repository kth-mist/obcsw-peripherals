/**
 * @file      sic_legs_msp_exp_definitions.h
 * @author    John Wikman
 * @copyright MIT License
 * @brief     Contains defines for the experiment side of MSP.
 *
 * @details
 * Contains definitions that the experiment side of MSP uses during
 * communication. Some definitions can come from other files, in which case
 * this header only checks that they are actually defined.
 */

#ifndef SIC_LEGS_MSP_EXP_DEFINITIONS_H
#define SIC_LEGS_MSP_EXP_DEFINITIONS_H

/* Import SIC_LEGS_MSP_EXP_ADDR and SIC_LEGS_MSP_EXP_MTU from the configuration file */
#include "sic_legs_msp_configuration.h"

#ifndef SIC_LEGS_MSP_EXP_ADDR
#error SIC_LEGS_MSP_EXP_ADDR not set
#endif

#ifndef SIC_LEGS_MSP_EXP_MTU
#error SIC_LEGS_MSP_EXP_MTU not set
#else
/**
 * @brief The maximum size an MSP frame can have.
 *
 * This definition should be used to determine minimum size of the buffers used
 * to send or receive MSP frames.
 */
#define SIC_LEGS_MSP_EXP_MAX_FRAME_SIZE (((SIC_LEGS_MSP_EXP_MTU) + 5) > 9 ? ((SIC_LEGS_MSP_EXP_MTU) + 5) : 9)
#endif


#endif /* SIC_LEGS_MSP_EXP_DEFINITIONS_H */
