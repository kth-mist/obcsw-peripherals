/**
 * @file comsim-siclegs.ino
 */

extern "C" {
	#include "sic_legs_msp_i2c_slave.h"
	#include "sic_legs_msp_exp.h"
}

#define I2C_CLOCKSPEED (400000)

/** Arduino Setup */
void setup(void)
{
	Serial.begin(9600);
	sic_legs_msp_i2c_setup(I2C_CLOCKSPEED);

	// Initialize the MSP state with a blank set of flags.
	sic_legs_msp_exp_state_initialize(sic_legs_msp_seqflags_init());

	Serial.print("Setup Complete\n");
}

void loop(void)
{
	// TODO
	delay(1000);
}
