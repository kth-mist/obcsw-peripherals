/**
 * @file      sic_legs_msp_crc.h
 * @author    John Wikman
 * @copyright MIT License
 * @brief     Functions for calculating CRC values.
 */

#ifndef SIC_LEGS_MSP_CRC_H
#define SIC_LEGS_MSP_CRC_H

unsigned long sic_legs_msp_crc32(const unsigned char *data, unsigned long len, unsigned long start_remainder);

#endif /* SIC_LEGS_MSP_CRC_H */
