/**
 * @file      sic_legs_msp_exp.h
 * @author    John Wikman
 * @copyright MIT License
 * @brief     A header that includes all the commonly used headers in the
 *            experiment side of MSP.
 */

#ifndef SIC_LEGS_MSP_EXP_H
#define SIC_LEGS_MSP_EXP_H

#ifdef __cplusplus
extern "C" {
#endif
#include "sic_legs_msp_configuration.h"
#include "sic_legs_msp_constants.h"
#include "sic_legs_msp_crc.h"
#include "sic_legs_msp_endian.h"
#include "sic_legs_msp_opcodes.h"
#include "sic_legs_msp_seqflags.h"
#include "sic_legs_msp_exp_callback.h"
#include "sic_legs_msp_exp_definitions.h"
#include "sic_legs_msp_exp_error.h"
#include "sic_legs_msp_exp_frame.h"
#include "sic_legs_msp_exp_handler.h"
#include "sic_legs_msp_exp_state.h"
#ifdef __cplusplus
}
#endif

#endif /* SIC_LEGS_MSP_EXP_H */
