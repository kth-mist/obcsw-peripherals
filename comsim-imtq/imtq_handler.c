// Copyright (c) 2021 Andrew Berezovskyi and MIST contributors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <Arduino.h>

#include "ADCSDataStructs.h"
#include "imtq_cmds.h"
#include "imtq_parameter.h"

#define INCREMENT_DATA 1

static uint8_t response_data[ADCS_MAX_REPLY_SIZE];
static uint16_t response_length = 0;

static int iSend = 1;
static int iReceive = 1;

static ADCSSensorData mtm_data;
static uint8_t mtm_data_ready;
static uint8_t mtm_new;
static ADCSSystemState sys_state;
static ADCSCoilData coil_data;
static ADCSSelfTestData self_test_data;
static ADCSDetumbleData detumble_data;
static ADCSCommandedActuatorData commanded_act;
static ADCSParameterReply param_reply;

static ADCSSimpleReply reply;

static uint8_t was_reset = 0;
static uint8_t self_test_axis;
static uint16_t bdot_dur;

uint8_t imtq_was_reset()
{
	uint8_t r = was_reset;
	was_reset = 0;
	return r;
}


/* Function definitions *******************************************************/
static uint8_t gen_stat(uint8_t new_data, uint8_t iva, uint8_t error)
{
	uint8_t stat = 0;
	if (new_data) {
		stat |= (1 << 7);
	}

	stat |= ((iva & 0x7) << 4);
	stat |= (error & 0xF);

	return stat;
}

// Prepares response opCode and Stat and sanitizes length of received data. If received data is of
// variable length, then the received data length must be greater than expected length
static int imtq_prepare_data(const uint8_t *data, size_t recv_len, size_t exp_len, uint8_t var_len)
{
	int ret = 0;

	if (data == NULL || recv_len < 1)
		reply.operationCode = TC_OP_FF;
	else
		reply.operationCode = data[0];

	response_length = sizeof(reply);
	if (recv_len < exp_len || (var_len && recv_len == exp_len)) {
		reply.stat = gen_stat(1, 0, 0x3);
	} else if (!var_len && recv_len > exp_len) {
		reply.stat = gen_stat(1, 0, 0x4);
	} else {
		reply.stat = gen_stat(1, 0, 0);
		ret = 1;
	}
	memcpy(response_data, (uint8_t *)&reply, sizeof(reply));
	return ret;
}

void imtq_handler_init(void)
{
	imtq_cmds_init(&mtm_data);
	memset(&sys_state, 0, sizeof(sys_state));
	was_reset = 1;
	mtm_data_ready = 0;
	mtm_new = 0;
	imtq_param_resetall();
}

void imtq_request(const uint8_t **buf, size_t *len)
{
	if (response_length == 0) {
		*buf = NULL;
		*len = 0;
	} else {
		*buf = response_data;
		*len = response_length;
	}
	iSend++;
}

void imtq_receive(const uint8_t *data, size_t datalen)
{
	int err;
	ADCSActuatorData *commandToActuators;
	ADCSHKData shk_data;
	uint8_t recv_cmd, i, j;

	if (datalen > 0) {
		recv_cmd = data[0];
	} else {
		recv_cmd = TC_OP_FF;
	}

	switch (recv_cmd) {
	case TC_OP_01: //Soft reset
		imtq_handler_init();
		response_data[0] = TC_OP_FF;
		response_data[1] = TC_OP_FF;
		response_length = 2;
		break;

	case TC_OP_02: //No-OP
		(void) imtq_prepare_data(data, datalen, 1, 0);
		break;

	case TC_OP_03: //Cancel Op
		if (imtq_prepare_data(data, datalen, 1, 0)) {
			sys_state.mode = IMTQ_MODE_IDLE;
		}
		break;

	case TC_OP_04: //Start MTM measurements
		if (imtq_prepare_data(data, datalen, 1, 0)) {
			mtm_new = 1;
			mtm_data_ready = 1;
		}
		break;

	case TC_OP_05: //Start actuators current
		if (imtq_prepare_data(data, datalen, 9, 0)) {
			commandToActuators = (ADCSActuatorData *)data;
		}
		break;

	case TC_OP_06: //Start actuation dipole
		if (imtq_prepare_data(data, datalen, 9, 0)) {
			commandToActuators = (ADCSActuatorData *)data;

			commanded_act.commandedX = commandToActuators->currentX;
			commanded_act.commandedY = commandToActuators->currentY;
			commanded_act.commandedZ = commandToActuators->currentZ;

			imtq_cmds_set_act(commandToActuators);
		}
		break;

	case TC_OP_07: // Start actuators PWM (currently does the same as start actuators current)
		if (imtq_prepare_data(data, datalen, 9, 0)) {
			commandToActuators = (ADCSActuatorData *)data;
		}
		break;

	case TC_OP_08: // Start self test
		if (imtq_prepare_data(data, datalen, 2, 0)) {
			sys_state.mode = IMTQ_MODE_SELFTEST;
			self_test_axis = data[1];
		}
		break;

	case TC_OP_09: // Start detumbling (BDOT)
		if (imtq_prepare_data(data, datalen, 3, 0)) {
			sys_state.mode = IMTQ_MODE_DETUMBLE;
			bdot_dur = ((ADCSDurationData*)data)->duration;
		}
		break;

	case TC_DR_01: // Get System State
		if (imtq_prepare_data(data, datalen, 1, 0)) {
			sys_state.operationCode = data[0];
			sys_state.stat = gen_stat(1, 0, 0);
			sys_state.err = 0;
			sys_state.uptime = 0x12345678;
			memcpy(response_data, (uint8_t *)&sys_state, sizeof(sys_state));
			response_length = sizeof(sys_state);
		}
		break;

	case TC_DR_02: // Get MTM raw data (currently does the same as calibrated)
		if (imtq_prepare_data(data, datalen, 1, 0)) {
			mtm_data.operationCode = data[0];

			if (mtm_data_ready) {
				mtm_data.stat = gen_stat(mtm_new, 0, 0);
				mtm_new = 0;
			} else {
				mtm_data.stat = gen_stat(0, 0x7, 0x5);
			}

			memcpy(response_data, (uint8_t *)&mtm_data, sizeof(mtm_data));
			response_length = sizeof(mtm_data);
		}
		break;

	case TC_DR_03: //Get calibrated MTM data
		if (imtq_prepare_data(data, datalen, 1, 0)) {
			mtm_data.operationCode = data[0];

			if (mtm_data_ready) {
				mtm_data.stat = gen_stat(mtm_new, 0, 0);
				mtm_new = 0;
			} else {
				mtm_data.stat = gen_stat(0, 0x7, 0x5);
			}

			memcpy(response_data, (uint8_t *)&mtm_data, sizeof(mtm_data));
			response_length = sizeof(mtm_data);
		}
		break;

	case TC_DR_04: //Get coil current
		if (imtq_prepare_data(data, datalen, 1, 0)) {
			coil_data.operationCode = data[0];
			coil_data.stat = gen_stat(1, 0, 0);
			coil_data.coilX = -100;
			coil_data.coilY = -200;
			coil_data.coilZ = -300;

			memcpy(response_data, (uint8_t *)&coil_data, sizeof(coil_data));
			response_length = sizeof(coil_data);
		}
		break;

	case TC_DR_05: //Get coil temperature
		if (imtq_prepare_data(data, datalen, 1, 0)) {
			coil_data.operationCode = data[0];
			coil_data.stat = gen_stat(1, 0, 0);
			coil_data.coilX = -150;
			coil_data.coilY = -250;
			coil_data.coilZ = -350;

			memcpy(response_data, (uint8_t *)&coil_data, sizeof(coil_data));
			response_length = sizeof(coil_data);
		}
		break;

	case TC_DR_06: // Get Commanded Actuation Dipole
		if (imtq_prepare_data(data, datalen, 1, 0)) {
			commanded_act.operationCode = data[0];
			commanded_act.stat = gen_stat(1, 0, 0);

			memcpy(response_data, (uint8_t*)&commanded_act, sizeof(commanded_act));
			response_length = sizeof(commanded_act);
		}
		break;

	case TC_DR_07: // Get Self-test
		if (imtq_prepare_data(data, datalen, 1, 0)) {
			if (sys_state.mode == IMTQ_MODE_SELFTEST) {
				if (self_test_axis == 0)
					response_length = sizeof(self_test_data) * 8;
				else
					response_length = sizeof(self_test_data) * 3;

				for (i = 0; i < response_length / sizeof(self_test_data); i++) {
					self_test_data.operationCode = data[0];
					self_test_data.stat = gen_stat(1, 0, 0);
					self_test_data.err = 0;
					if (self_test_axis != 0) {
						switch (i) {
						case 0:
							self_test_data.step = 0; // INIT
							break;
						case 1:
							self_test_data.step = self_test_axis;
							break;
						default:
							self_test_data.step = 7; // FINA
							break;
						}
					} else {
						self_test_data.step = i;
					}
					for (j = 0; j < 3; j++) {
						self_test_data.rawMag[j] = i * 12 + j * 4 + 0;
						self_test_data.calMag[j] = -(i * 12 + j * 4 + 1);
						self_test_data.coilCurr[j] = i * 12 + j * 4 + 2;
						self_test_data.coilTemp[j] = -(i * 12 + j * 4 + 3);
					}
					memcpy(response_data + sizeof(self_test_data) * i, (uint8_t*)&self_test_data, sizeof(self_test_data));
				}
				sys_state.mode = IMTQ_MODE_IDLE;
			} else {
				response_data[0] = data[0];
				response_data[1] = gen_stat(1, 0, 0x5);
				response_length = 2;
			}
		}
		break;

	case TC_DR_08: // Get Detumble data
		if (imtq_prepare_data(data, datalen, 1, 0)) {
			if (sys_state.mode == IMTQ_MODE_DETUMBLE) {
				detumble_data.operationCode = data[0];
				detumble_data.stat = gen_stat(1, 0, 0);
				for (j = 0; j < 3; j++) {
					detumble_data.calMag[j] = j * 6 + 0;
					detumble_data.filteredMag[j] = j * 6 + 1;
					detumble_data.bdot[j] = j * 6 + 2;
					detumble_data.cmdActDip[j] = j * 6 + 3;
					detumble_data.coilCurrCmd[j] = j * 6 + 4;
					detumble_data.coilCurrMeas[j] = j * 6 + 5;
				}
				memcpy(response_data, (uint8_t*)&detumble_data, sizeof(detumble_data));
				response_length = 56;
				sys_state.mode = IMTQ_MODE_IDLE;
			} else {
				response_data[0] = data[0];
				response_data[1] = gen_stat(1, 0, 0x5);
				response_length = 2;
			}
		}
		break;

	case TC_DR_09: //Get raw HK data
		if (imtq_prepare_data(data, datalen, 1, 0)) {
			if (INCREMENT_DATA) {
				shk_data.operationCode = data[0];
				shk_data.stat = gen_stat(1, 0, 0);
				shk_data.digitalVoltage = iSend * 10;
				shk_data.analogVoltage = iSend * 10;
				shk_data.digitalCurrent = iSend * 10;
				shk_data.analogCurrent = iSend * 10;
				shk_data.coilXCurrent = iSend * 10;
				shk_data.coilYCurrent = iSend * 10;
				shk_data.coilZCurrent = iSend * 10;
				shk_data.coilXTemperature = iSend * 10;
				shk_data.coilYTemperature = iSend * 10;
				shk_data.coilZTemperature = iSend * 10;
				shk_data.MCUTemperature = iSend * 10;

			} else { //fixdata to send
				shk_data.operationCode = data[0];
				shk_data.stat = gen_stat(1, 0, 0);
				shk_data.digitalVoltage = 10;
				shk_data.analogVoltage = 20;
				shk_data.digitalCurrent = 30;
				shk_data.analogCurrent = 40;
				shk_data.coilXCurrent = 10;
				shk_data.coilYCurrent = 20;
				shk_data.coilZCurrent = 30;
				shk_data.coilXTemperature = 40;
				shk_data.coilYTemperature = 50;
				shk_data.coilZTemperature = 60;
				shk_data.MCUTemperature = 70;
			}

			memcpy(response_data, (uint8_t *)&shk_data, sizeof(shk_data));
			response_length = sizeof(shk_data);
		}
		break;

	case TC_DR_10: //Get engineering HK data
		if (imtq_prepare_data(data, datalen, 1, 0)) {
			if (INCREMENT_DATA) {
				shk_data.operationCode = data[0];
				shk_data.stat = gen_stat(1, 0, 0);
				shk_data.digitalVoltage = iSend * 10;
				shk_data.analogVoltage = iSend * 10;
				shk_data.digitalCurrent = iSend * 10;
				shk_data.analogCurrent = iSend * 10;
				shk_data.coilXCurrent = iSend * 10;
				shk_data.coilYCurrent = -iSend * 10;
				shk_data.coilZCurrent = iSend * 10;
				shk_data.coilXTemperature = iSend * 10;
				shk_data.coilYTemperature = -iSend * 10;
				shk_data.coilZTemperature = iSend * 10;
				shk_data.MCUTemperature = iSend * 10;

			} else { //fixdata to send
				shk_data.operationCode = data[0];
				shk_data.stat = gen_stat(1, 0, 0);
				shk_data.digitalVoltage = 10;
				shk_data.analogVoltage = 20;
				shk_data.digitalCurrent = 30;
				shk_data.analogCurrent = 40;
				shk_data.coilXCurrent = -10;
				shk_data.coilYCurrent = 20;
				shk_data.coilZCurrent = -30;
				shk_data.coilXTemperature = 40;
				shk_data.coilYTemperature = -50;
				shk_data.coilZTemperature = 60;
				shk_data.MCUTemperature = -70;
			}

			memcpy(response_data, (uint8_t *)&shk_data, sizeof(shk_data));
			response_length = sizeof(shk_data);
		}
		break;

	case TC_CF_01: // Get parameter
		if (imtq_prepare_data(data, datalen, sizeof(ADCSParameter), 0)) {
			// Assuming that an uninitializaed value should be sent on error
			err = imtq_param_get(((ADCSParameter*)data)->paramId, &response_data[sizeof(param_reply)], (size_t*) &response_length);

			param_reply.operationCode = data[0];
			param_reply.stat = gen_stat(1, 0, err);
			param_reply.paramId = ((ADCSParameter*)data)->paramId;
			memcpy(response_data, (uint8_t *)&param_reply, sizeof(param_reply));
			response_length += sizeof(ADCSParameterReply);
		}
		break;

	case TC_CF_02: // Set parameter
		if (imtq_prepare_data(data, datalen, sizeof(ADCSParameter), 1)) {
			err = imtq_param_set(((ADCSParameter*)data)->paramId, &data[sizeof(ADCSParameter)], datalen - sizeof(ADCSParameter));

			param_reply.operationCode = data[0];
			param_reply.stat = gen_stat(1, 0, err);
			param_reply.paramId = ((ADCSParameter*)data)->paramId;
			memcpy(response_data, (uint8_t *)&param_reply, sizeof(param_reply));
			if (err == 0) {
				memcpy(&response_data[sizeof(param_reply)], &data[sizeof(ADCSParameter)], datalen - sizeof(ADCSParameter));
				response_length = sizeof(ADCSParameterReply) + datalen - sizeof(ADCSParameter);
			} else {
				// If set failed, send previous value in response (if possible)
				(void) imtq_param_get(((ADCSParameter*)data)->paramId, &response_data[sizeof(param_reply)], (size_t*) &response_length);
				response_length += sizeof(ADCSParameterReply);
			}
		}
		break;

	case TC_CF_03: // Reset parameter
		if (imtq_prepare_data(data, datalen, sizeof(ADCSParameter), 0)) {
			// Assuming that an uninitializaed value should be sent on error
			err = imtq_param_reset(((ADCSParameter*)data)->paramId, &response_data[sizeof(param_reply)], (size_t*) &response_length);

			param_reply.operationCode = data[0];
			param_reply.stat = gen_stat(1, 0, err);
			param_reply.paramId = ((ADCSParameter*)data)->paramId;
			memcpy(response_data, (uint8_t *)&param_reply, sizeof(param_reply));
			response_length += sizeof(ADCSParameterReply);
		}
		break;

	default:
		reply.operationCode = recv_cmd;
		reply.stat = gen_stat(1, 0, 0x2);
		memcpy(response_data, (uint8_t *)&reply, sizeof(reply));
		response_length = sizeof(reply);
	}
	iReceive++;
}
