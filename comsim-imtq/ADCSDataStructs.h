// Copyright (c) 2021 Andrew Berezovskyi and MIST contributors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef ADCS_DATA_STRUCTS_H_
#define ADCS_DATA_STRUCTS_H_

#include <stdint.h>

#ifndef I2C_BIT_RATE
#define I2C_BIT_RATE 400000
#endif

#define ADCS_ADDRESS 0x10

// The command codes of the implemented commands based on the
// ISIS.iMTQ.User Manual 1.5
#define TC_OP_01 0xAA // Software Reset
#define TC_OP_02 0x02 // No-OP
#define TC_OP_03 0x03 // Cancel Operation
#define TC_OP_04 0x04 // Start MTM measurements
#define TC_OP_05 0x05 // Start actuators current
#define TC_OP_06 0x06 // Start actuators dipole
#define TC_OP_07 0x07 // Start actuators PWM
#define TC_OP_08 0x08 // Start self-test
#define TC_OP_09 0x09 // Start detumbling (BDOT)
#define TC_OP_FF 0xFF //Failed read, see p. 23 of the manual

#define TC_DR_01 0x41 // Get System State
#define TC_DR_02 0x42 // Get MTM raw data
#define TC_DR_03 0x43 // Get MTM calibrated data
#define TC_DR_04 0x44 // Get Coil current
#define TC_DR_05 0x45 // Get Coil temperatures
#define TC_DR_06 0x46 // Get Commanded Actuation Dipole
#define TC_DR_07 0x47 // Get Self-test
#define TC_DR_08 0x48 // Get Detumble data
#define TC_DR_09 0x49 // Get raw HK data
#define TC_DR_10 0x4A // Get engineering HK data

#define TC_CF_01 0x81 // Get paramater
#define TC_CF_02 0x82 // Set parameter
#define TC_CF_03 0x83 // Reset parameter

// Size of the longest command that can be received.
#define ADCS_MAX_COMMAND_SIZE 9
#define ADCS_MAX_REPLY_SIZE   320

// iMTQ modes of operation
#define IMTQ_MODE_IDLE 0
#define IMTQ_MODE_SELFTEST 1
#define IMTQ_MODE_DETUMBLE 2

typedef struct __attribute__((__packed__)) ADCSHKData {
	uint8_t operationCode;
	uint8_t stat;
	uint16_t digitalVoltage;
	uint16_t analogVoltage;
	uint16_t digitalCurrent;
	uint16_t analogCurrent;
	int16_t coilXCurrent;
	int16_t coilYCurrent;
	int16_t coilZCurrent;
	int16_t coilXTemperature;
	int16_t coilYTemperature;
	int16_t coilZTemperature;
	int16_t MCUTemperature;
} ADCSHKData;

typedef struct __attribute__((__packed__)) ADCSSensorData {
	uint8_t operationCode;
	uint8_t stat;
	int32_t magX;
	int32_t magY;
	int32_t magZ;
	uint8_t coilAct;
} ADCSSensorData;

typedef struct __attribute__((__packed__)) ActuatorData {
	uint8_t operationCode;
	int16_t currentX;
	int16_t currentY;
	int16_t currentZ;
	uint16_t duration;
} ADCSActuatorData;

typedef struct __attribute__((__packed__)) DurationData {
	uint8_t operationCode;
	uint16_t duration;
} ADCSDurationData;

typedef struct __attribute__((__packed__)) Parameter {
	uint8_t operationCode;
	uint16_t paramId;
} ADCSParameter;

typedef struct __attribute__((__packed__)) SystemState {
	uint8_t operationCode;
	uint8_t stat;
	uint8_t mode;
	uint8_t err;
	uint8_t conf;
	uint32_t uptime;
} ADCSSystemState;

typedef struct __attribute__((__packed__)) CommandedActuatorData {
	uint8_t operationCode;
	uint8_t stat;
	int16_t commandedX;
	int16_t commandedY;
	int16_t commandedZ;
} ADCSCommandedActuatorData;

typedef struct __attribute__((__packed__)) CoilData {
	uint8_t operationCode;
	uint8_t stat;
	int16_t coilX;
	int16_t coilY;
	int16_t coilZ;
} ADCSCoilData;

typedef struct __attribute__((__packed__)) SelfTestData {
	uint8_t operationCode;
	uint8_t stat;
	uint8_t err;
	uint8_t step;
	int32_t rawMag[3];
	int32_t calMag[3];
	int16_t coilCurr[3];
	int16_t coilTemp[3];
} ADCSSelfTestData;

typedef struct __attribute__((__packed__)) DetumbleData {
	uint8_t operationCode;
	uint8_t stat;
	int32_t calMag[3];
	int32_t filteredMag[3];
	int32_t bdot[3];
	int16_t cmdActDip[3];
	int16_t coilCurrCmd[3];
	int16_t coilCurrMeas[3];
} ADCSDetumbleData;

typedef struct __attribute__((__packed__)) ParameterReply {
	uint8_t operationCode;
	uint8_t stat;
	uint16_t paramId;
} ADCSParameterReply;

typedef struct __attribute__((__packed__)) SimpleReply {
	uint8_t operationCode;
	uint8_t stat;
} ADCSSimpleReply;

#endif // ADCS_DATA_STRUCTS_H_
