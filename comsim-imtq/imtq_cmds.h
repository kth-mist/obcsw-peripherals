// Copyright (c) 2021 Andrew Berezovskyi and MIST contributors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/**
 * @file imtq_cmds.h
 *
 * @brief IMTQ specific commands module
 *
 * @version v0.1.0-dev
 */
#ifndef __IMTQ_CMDS_H
#define __IMTQ_CMDS_H

/* @brief Used in the QEMU/HAL-clone setup to differentiate
          between Arduino simulators */
#define ARDUINO_UNIT_ID_IMTQ    (166)

#ifdef __cplusplus
extern "C" {
#endif

#include "ADCSDataStructs.h"

/**
 * @brief Initialize the module and setup the available commands.
 *
 * @details This should be called only once. It should be called after the
 * inicialization of the Serial interface.
 *
 * @param[in] mtm_data
 */
void imtq_cmds_init(ADCSSensorData *mtm_data);

/**
 * @brief Send command to notify start dipole actuation.
 *
 * @param[in] actuators
 */
void imtq_cmds_set_act(const ADCSActuatorData *actuators);

void imtq_set_mtm(const ADCSSensorData *mtm);
int imtq_get_act(ADCSActuatorData* out);
void imtq_clean_act(void);


#ifdef __cplusplus
} // extern "C"
#endif

#endif // __IMTQ_CMDS_H
