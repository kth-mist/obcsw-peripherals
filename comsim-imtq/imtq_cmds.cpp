// Copyright (c) 2021 Andrew Berezovskyi and MIST contributors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/**
 * @file imtq_cmds.cpp
 *
 * @brief IMTQ FT commands implementation.
 *
 * @version v0.1.0-dev
 */

#include "imtq_cmds.h"
#include "ftcmd.h"
#include <Arduino.h>
#include <stdio.h>
#include <string.h>

#define ARDUINO_UNIT_ID ARDUINO_UNIT_ID_IMTQ

/* Global variables ***********************************************************/
static ADCSSensorData *s_mtm_data;
static ADCSActuatorData act_data;
static int act_status;

/* Function prototypes ********************************************************/
static void cmd_mtm_cb(const char **argv, unsigned int argn);
static void cmd_act_cb(const char **argv, unsigned int argn);
static void cmd_clean_cb(const char **argv, unsigned int argn);
static void unknown_cmd_cb(const char **argv, unsigned int argn);

static const ftcmd_cmd_t cmd_table[] = {
	{ "mtm_c", cmd_mtm_cb },
	{ "act_d", cmd_act_cb },
	{ "clean_d", cmd_clean_cb },
	{ NULL, NULL },
};
/* Callbacks ******************************************************************/
static void cmd_mtm_cb(const char **argv, unsigned int argn)
{
	if (argn != 4) {
		return;
	}

	s_mtm_data->magX = atoi(argv[1]);
	s_mtm_data->magY = atoi(argv[2]);
	s_mtm_data->magZ = atoi(argv[3]);
}

static void cmd_act_cb(const char **argv, unsigned int argn)
{
	(void) argv;
	(void) argn;

	Serial.print("+act_d;");
	Serial.print(act_status);
	Serial.print(';');
	Serial.print(act_data.currentX);
	Serial.print(';');
	Serial.print(act_data.currentY);
	Serial.print(';');
	Serial.print(act_data.currentZ);
	Serial.print(';');
	Serial.println(act_data.duration);
}
static void cmd_clean_cb(const char **argv, unsigned int argn)
{
	(void) argv;
	(void) argn;

	imtq_clean_act();
}

static void unknown_cmd_cb(const char **argv, unsigned int argn)
{
	(void) argn;

	Serial.print("Invalid cmd: ");
	Serial.println(argv[0]);
}

/* Function definitions *******************************************************/
/* Public functions ***********************************************************/
void imtq_cmds_init(ADCSSensorData *mtm_data)
{
	s_mtm_data = mtm_data;
	act_status = -1;
	ftcmd_init(cmd_table, unknown_cmd_cb);
}

void imtq_cmds_set_act(const ADCSActuatorData *actuators)
{
	memcpy(&act_data, actuators, sizeof(act_data));
	act_status = 0;
}

void imtq_set_mtm(const ADCSSensorData *mtm)
{
	s_mtm_data->magX = mtm->magX;
	s_mtm_data->magY = mtm->magY;
	s_mtm_data->magZ = mtm->magZ;
}

int imtq_get_act(ADCSActuatorData* out)
{
	out->currentX = act_data.currentX;
	out->currentY = act_data.currentY;
	out->currentZ = act_data.currentZ;
	out->duration = act_data.duration;

	return act_status;
}

void imtq_clean_act(void)
{
	act_status = -1;
	memset(&act_data, 0, sizeof(act_data));
}
