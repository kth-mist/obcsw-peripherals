// Copyright (c) 2021 Andrew Berezovskyi and MIST contributors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef VERSION_H__
#define VERSION_H__

#define STRINGIZE2(s) #s
#define STRINGIZE(s) STRINGIZE2(s)

#define PRJ_NAME "iMTQ Sim."

#define VERSION_MAJOR 1
#define VERSION_MINOR 0
#define VERSION_FIX 0
#define VERSION_SU ""

#define VERSION_STR                                                            \
	STRINGIZE(VERSION_MAJOR) "." STRINGIZE(VERSION_MINOR) "." STRINGIZE(VERSION_FIX) VERSION_SU

#define VERSION_INFO                                                           \
	PRJ_NAME " v" VERSION_STR " (Compiled " __DATE__ " " __TIME__ ")"

#endif // VERSION_H__
