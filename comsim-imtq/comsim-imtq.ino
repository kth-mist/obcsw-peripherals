// Copyright (c) 2021 Andrew Berezovskyi and MIST contributors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <Wire.h>
#include <stdio.h>
#include "ftcmd.h"
#include "imtq_cmds.h"
#include "version.h"

extern "C" {
	#include "imtq_handler.h"
}

#define MIN_SERIAL_PRINTS 1
#define SERIAL_PRINTS 1

/* Global variables ***********************************************************/
static uint8_t command[ADCS_MAX_COMMAND_SIZE];

/* Callbacks ******************************************************************/
static void requestEvent()
{
	uint8_t err = 0xFF;
	const uint8_t* buf;
	size_t len;
	imtq_request(&buf, &len);
	if (buf == NULL) {
		Wire.write(&err, 1);
	} else {
		Wire.write(buf, len);
	}

	digitalWrite(13, LOW);
}

static void receiveEvent(int howMany)
{
	digitalWrite(13, HIGH);
	if (MIN_SERIAL_PRINTS) {
		//Serial.print("r ");
		//Serial.println(iReceive);
	}
	int error = 0;
	int commandSize = 1;

	command[0] = Wire.read();

	while (Wire.available()) {
		if (commandSize < ADCS_MAX_COMMAND_SIZE) {
			command[commandSize] = Wire.read();
			commandSize++;
		} else {
			(void)Wire.read();
		}
	}

	//Serial.print("received: ");
	int i = 0;
	if (SERIAL_PRINTS) {
		while (i < commandSize) {
			Serial.print(command[i], HEX);
			Serial.print("  ");
			i++;
		}
		Serial.println("END");
	}

	imtq_receive(command, commandSize);
}



/* Main functions *************************************************************/
void setup()
{
	pinMode(13, OUTPUT);
	Serial.begin(115200);

	Serial.println(VERSION_INFO);

	Wire.setClock(I2C_BIT_RATE);
	Wire.begin(ADCS_ADDRESS);
	Wire.onRequest(requestEvent);
	Wire.onReceive(receiveEvent);

	Serial.println("ADCS Sim setup done");

	imtq_handler_init();
}

void loop()
{
	ftcmd_fire();
	//delay(100);
}
