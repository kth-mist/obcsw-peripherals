// Copyright (c) 2021 Andrew Berezovskyi and MIST contributors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/**
 * @file ftcmd.h
 *
 * @brief Serial interface command reader
 *
 * This module allows to easily decode the commands received through the UART
 * interface and to call a specific callback for each command, which receives a
 * list with all the parameters received with that command.
 *
 * This module uses Arduino's Serial interface, so it should be previously
 * initialized with the desired parameters for the correct behavior of this
 * module.
 *
 * @version v0.1.0-dev
 */
#ifndef __FTCMD_H
#define __FTCMD_H

#ifdef __cplusplus
extern "C" {
#endif

/**@brief Size of the input buffer to store the incoming characters */
#define FTCMD_IN_BUFFER_SIZE 50

/**
 * @brief Command callback
 *
 * @param argv Array with the strings received as parameters with the command.
 * @param argn Number of parameters.
 */
typedef void (*ftcmd_cmd_cb_t)(const char **argv, unsigned int argn);

/**@brief Command definition */
typedef struct ftcmd_cmd_t {
	const char *cmd; /**< Command string. */
	ftcmd_cmd_cb_t cb; /**< Command callback. */
} ftcmd_cmd_t;

/**
 * @brief Initialize the module and setup the available commands.
 *
 * @details This should be called only once. It should be called after the
 * initialization of the Serial interface.
 *
 * @param[in] command Reference to the command table.
 * @param[in] unknown_cmd_cb  Default callback when an unknown command is
 *                            received.
 *
 * @note The module does not copy the data from the *commands* parameter, it
 * just saves the reference, therefore, it should point to a permanent variable.
 *
 * @note To mark the end of the command table *commands* it is required to
 * insert a command whose string is NULL.
 */
void ftcmd_init(const ftcmd_cmd_t *commands, ftcmd_cmd_cb_t unknown_cmd_cb);

/**
 * @brief Process incoming data and calls the respective command callback if
 * apropiate.
 *
 * @details This should be called periodically in the application main loop.
 */
void ftcmd_fire();

#ifdef __cplusplus
} // extern "C"
#endif

#endif // __FTCMD_H
