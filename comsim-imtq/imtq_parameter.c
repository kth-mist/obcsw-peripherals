// Copyright (c) 2021 Andrew Berezovskyi and MIST contributors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <string.h>
#include <stdint.h>
#include <stdlib.h>

#include "ADCSDataStructs.h"

// iMTQ parameter buffers, lengths derived from the iMTQ user manual documentation
static int8_t int8_t_params[0x0];
static uint8_t uint8_t_params[0xB];
static int16_t int16_t_params[0x2E];
static uint16_t uint16_t_params[0x1];
static int32_t int32_t_params[0x3];
static uint32_t uint32_t_params[0x0];
static float float_params[0x0];
static int64_t int64_t_params[0x0];
static uint64_t uint64_t_params[0x0];
static double double_params[0x12];

// Read only parameters
static int8_t int8_t_ro_params[0x0];
static uint8_t uint8_t_ro_params[0x2] = {0, 60};
static int16_t int16_t_ro_params[0x0];
static uint16_t uint16_t_ro_params[0x1] = {ADCS_ADDRESS};
static int32_t int32_t_ro_params[0x0];
static uint32_t uint32_t_ro_params[0x1] = {0x00000100};
static float float_ro_params[0x0];
static int64_t int64_t_ro_params[0x0];
static uint64_t uint64_t_ro_params[0x0];
static double double_ro_params[0x0];

static int imtq_find_param(uint16_t id, void **param_ptr, size_t *param_len, uint8_t *read_only)
{
#define IMTQ_FIND_PARAM(dtype)                                             \
	*param_len = sizeof(dtype);                                        \
	if (index >= sizeof(dtype ## _params) / *param_len) {              \
		if (index < 0x800)                                         \
			return 1;                                          \
		index -= 0x800;                                            \
		if (index >= sizeof(dtype ## _ro_params) / *param_len) {   \
			return 1;                                          \
		} else {                                                   \
			*param_ptr = &dtype ## _ro_params[index];          \
			*read_only = 1;                                    \
		}                                                          \
	} else {                                                           \
		*param_ptr = &dtype ## _params[index];                     \
		*read_only = 0;                                            \
	}

	unsigned int code;
	unsigned int index;

	if (param_ptr == NULL || param_len == NULL || read_only == NULL)
		return 1;

	code = id >> 12;
	index = id & 0xFFF;

	switch (code) {
	case 0x1:
		IMTQ_FIND_PARAM(int8_t);
		break;
	case 0x2:
		IMTQ_FIND_PARAM(uint8_t);
		break;
	case 0x3:
		IMTQ_FIND_PARAM(int16_t);
		break;
	case 0x4:
		IMTQ_FIND_PARAM(uint16_t);
		break;
	case 0x5:
		IMTQ_FIND_PARAM(int32_t);
		break;
	case 0x6:
		IMTQ_FIND_PARAM(uint32_t);
		break;
	case 0x7:
		IMTQ_FIND_PARAM(float);
		break;
	case 0x8:
		IMTQ_FIND_PARAM(int64_t);
		break;
	case 0x9:
		IMTQ_FIND_PARAM(uint64_t);
		break;
	case 0xA:
		IMTQ_FIND_PARAM(double);
		break;
	default:
		return 1;
	}

	return 0;
}

void imtq_param_resetall(void)
{
	memset(uint8_t_params, 0, sizeof(uint8_t_params));
	memset(int16_t_params, 0, sizeof(int16_t_params));
	memset(uint16_t_params, 0, sizeof(uint16_t_params));
	memset(int32_t_params, 0, sizeof(int32_t_params));
	memset(double_params, 0, sizeof(double_params));
}

int imtq_param_get(uint16_t id, void *pval, size_t *plen)
{
	int err;
	void* out_val;
	uint8_t read_only;

	if (pval == NULL || plen == NULL)
		return 1;

	err = imtq_find_param(id, &out_val, plen, &read_only);
	if (err != 0) {
		return 1;
	}

	memcpy(pval, out_val, *plen);

	return 0;
}


int imtq_param_set(uint16_t id, const void *pval, size_t plen)
{
	int err;
	void* out_val;
	size_t out_val_len;
	uint8_t read_only;

	if (pval == NULL)
		return 1;

	err = imtq_find_param(id, &out_val, &out_val_len, &read_only);
	if (err != 0 || plen != out_val_len || read_only) {
		return 1;
	}

	memcpy(out_val, pval, out_val_len);

	return 0;
}


int imtq_param_reset(uint16_t id, void *pval, size_t *plen)
{
	int err;
	void* out_val;
	uint8_t read_only;

	if (pval == NULL || plen == NULL)
		return 1;

	err = imtq_find_param(id, &out_val, plen, &read_only);
	if (err != 0) {
		return 1;
	}

	if (!read_only) {
		// TODO Use actual default values and not just zero
		memset(out_val, 0, *plen);
	}

	memcpy(pval, out_val, *plen);

	return 0;
}
