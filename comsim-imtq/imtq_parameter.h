// Copyright (c) 2021 Andrew Berezovskyi and MIST contributors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/**
 * @file imtq_parameter.h
 *
 * @brief IMTQ digital configuration parameter module
 *
 * @version v0.1.0-dev
 */
#ifndef __IMTQ_PARAMS_H
#define __IMTQ_PARAMS_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Reset all parameters to their default values
 */
void imtq_param_resetall(void);

/**
 * @brief Get a parameter value based on its id.
 *
 * @param[in] id The id of the parameter to get
 * @param[out] pval The value of the requested parameter
 * @param[out] plen The length of the value of the requested parameter
 *
 * @returns 0 on success, otherwise status byte (STAT) command error bits
 */
int imtq_param_get(uint16_t id, void *pval, size_t *plen);

/**
 * @brief Set a parameter to a value based on its id.
 *
 * @param[in] id The id of the parameter to set
 * @param[in] pval The value to set the parameter to
 * @param[in] plen The length of the value to set the parameter to
 *
 * @returns 0 on success, otherwise status byte (STAT) command error bits
 */
int imtq_param_set(uint16_t id, const void *pval, size_t plen);

/**
 * @brief Reset a parameter to its default value based on its id.
 *
 * @param[in] id The id of the parameter to reset
 * @param[out] pval The value that the parameter was reset to
 * @param[out] plen The length of the value that the parameter was set to
 *
 * @returns 0 on success, otherwise status byte (STAT) command error bits
 */
int imtq_param_reset(uint16_t id, void *pval, size_t *plen);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // __IMTQ_PARAMS_H
