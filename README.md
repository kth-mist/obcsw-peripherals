# OBCSW - Peripheral Code
This repository acts a collection point for code that is closely related to the
on-board software, but not directly part of it. This is includes code such for
communication simulators, test scripts, etc.

While there is no review process in place for this repository (as opposed to the
process used in obcsw), please do not touch any code which you do not know the
purpose or function of.

`mspprefix.sh` is a script that can prefix MSP function names for simulators
using MSP with a given name.

## Subdirectories
 - **ants-simulator**<br />
   An Arduino Due-based simulator for the ISIS AntS, replying to commands sent
   over I2C initiating deployment and reporting on requested values. With the
   AntS Simulator shield attached to the Due, the Simulator also draws the
   power the actual AntS would from the EPS, enabling full functional testing
   of the initialization phase.
 - **comsim-eps**<br />
   An Arduino Due project that implements the GOMspace EPS I2C legacy protocol
   that can be used with the ISIS subsystem library callbacks. Implements some
   stateful behavior that replicates the EPS.
 - **comsim-trxvu**<br />
   An Arduino Due simulator for the TRXVU radio adjusted from [here](https://gitlab.com/kth-mist/sim-radio/-/tree/master/src/trxvu_due)
   to work in the OBCSW test framework. For more information, see the [README](comsim-trxvu/README.md).
 - **comsim-cubes[1-2]**<br />
   Arduino Due simulators for the two CUBES experiment units used in the
   OBCSW test framework. They are identical with the exception of their I2C addresses.
 - **comsim-siclegs**<br />
   An Arduino Due simulator for the SiC-LEGS experiment used in the
   OBCSW test framework.
 - **comsim-nanoprop**<br />
   An Arduino Due simulator for the NanoProp experiment.
 - **comsim-mpu**<br />
 - An Arduino Due simulator for the MPU 6050 on the NanoProp Helper Board.
 - **comsim-imtq**<br />
   An Arduino Due simulator for the iMTQ system that will be used during the
   development of the on-board software. It is based on the iMTQ simulator in
   the [ADCS simulator repo](https://gitlab.com/kth-mist/adcs-simulator/-/tree/master/imtq-sim/imtq-sim-arduino)
   and adjusted to work in the OBCSW test framework, but it has not been tested
   on an actual Arduino.
 - **comsim-solar-panels**<br />
   Stub code that simulates the both version 1 and version 2 (LTC2983) of the ISIS solar panel
   temperature sensors, implementing the SPI protocol. It is used in the OBCSW test framework.
 - **hdrm-simulator**<br />
   An Arduino Due simulator for the Hold Down & Release Mechanism
