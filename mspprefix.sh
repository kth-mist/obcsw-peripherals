#!/bin/bash

if [[ "$#" -ne 1 && "$#" -ne 2 ]]; then
	echo "Usage: mspprefix.sh <foldername> [<simulatorname>]"
	echo "Example: mspprefix.sh comsim-cubes1 cubes_1"
	echo "This would result in all MSP function names in the ./comsim-cubes1 folder"
	echo "being prefixed with \"cubes_1_\". If <simulatorname> is not specified, the"
	echo "prefix for all MSP function names will be removed."
	exit 1
fi

if [[ "$#" -eq 2 ]]; then
	if [[ "$2" =~ [^a-z0-9_] ]]; then
		echo "<simulatorname> may only contain lower case letters, numbers, or underscores."
		exit 2
	fi
	prefix="$2_"
else
	prefix=""
fi
upperprefix=$(echo $prefix | tr [a-z] [A-Z])

# Substitute "*msp_" with "<simulatorname>_msp_" in file names and file content
# for each file in <foldername>
sub="s/\([a-zA-Z0-9_]*\)msp_/${prefix}msp_/g"
uppersub="s/\([a-zA-Z0-9_]*\)MSP_/${upperprefix}MSP_/g"
if [[ -d "$1" ]]; then
	find "$1" -type f -print0 | xargs -0 sed -i "$sub"
	find "$1" -type f -print0 | xargs -0 sed -i "$uppersub"
	for old in $(find "$1" -name *msp_*); do
		mv $old $(echo $old | sed "$sub") 2>/dev/null;
	done
else
	echo "Directory \"$1\" does not exist.";
	exit 3
fi
