#ifndef VERSION_H__
#define VERSION_H__

#define STRINGIZE2(s) #s
#define STRINGIZE(s) STRINGIZE2(s)

#define PRJ_NAME                   "AntS Sim."

#define VERSION_MAJOR               1
#define VERSION_MINOR               0
#define VERSION_FIX                 0
#define VERSION_SU                  ""

#define VERSION_STR                 \
	STRINGIZE(VERSION_MAJOR) "."    \
	STRINGIZE(VERSION_MINOR) "."    \
	STRINGIZE(VERSION_FIX)          \
	VERSION_SU

#define VERSION_INFO                \
	PRJ_NAME " v"                   \
	VERSION_STR                     \
	" (Compiled "                   \
	__DATE__ " "                    \
	__TIME__ ")"

#endif // VERSION_H__
