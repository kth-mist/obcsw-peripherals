/**
 * @file ftcmd.cpp
 *
 * @brief Serial interface command reader implementation.
 *
 * @version v0.1.0-dev
 */

#include "ftcmd.h"
#include "Arduino.h"
#include <string.h>
#include <stdbool.h>

#define SEP_CHAR ';'
#define CMD_CHAR ':'

typedef struct ftcmd_data_t {
	const ftcmd_cmd_t* cmd_table;
	ftcmd_cmd_cb_t     unknown_cmd_cb;
	char              in_buff[FTCMD_IN_BUFFER_SIZE];
	unsigned int      pos;
} ftcmd_data_t;

/* Global variables ***********************************************************/
static ftcmd_data_t self;

// This is defined in ftcmd_common.cpp
extern const ftcmd_cmd_t common_cmd[];

/* Function prototypes ********************************************************/
static bool read_in();
static unsigned int read_cmd();
static void read_params(const char** param_buff);
static bool process_cmd(const char** argv, unsigned int argn,
		const ftcmd_cmd_t* cmd_table);
/* Callbacks ******************************************************************/
/* Function definitions *******************************************************/
static bool read_in()
{
	if (!Serial.available()) {
		return false;
	}

	char read = Serial.read();

	if (read == '\n' || read == '\r') {
		if (self.pos == 0) {
			return false;
		}
		self.in_buff[self.pos++] = '\0';
		return true;
	}

	self.in_buff[self.pos++] = read;

	return false;
}

static unsigned int read_cmd()
{
	unsigned int argn = 1;
	for (char* c = self.in_buff + 1; *c != '\0'; c++) {
		if (*c == SEP_CHAR) {
			*c = '\0';
			argn++;
		}
	}

	return argn;
}

/**
 * @brief Fills the parameter buffer placing one parameter in each position.
 *
 * @details When this functions is called the parameters of the input command
 * are separated by a NULL character in the input buffer. This creates a
 * collection of independent and consecutive null-terminated strings. This
 * function will loop across those strings (by calculating the length of each
 * one) storing the begining of them on the parameter buffer as if they were
 * independent strings.
 *
 * @param[out] param_buff Buffer to place pointers to parameters.
 */
static void read_params(const char** param_buff)
{
	int i = 1;
	int p = 0;
	while (i < self.pos) {
		char* arg = self.in_buff + i;
		param_buff[p++] = arg;
		i += 1 + strlen(arg);
	}
}

static bool process_cmd(const char** argv, unsigned int argn,
		const ftcmd_cmd_t* cmd_table)
{
	const ftcmd_cmd_t* cmd;
	for (cmd = cmd_table; cmd->cmd != NULL; cmd++) {
		if (strcmp(cmd->cmd, argv[0]) == 0) {
			cmd->cb(argv, argn);
			break;
		}
	}

	return cmd->cmd != NULL;
}
/* Public functions ***********************************************************/
void ftcmd_init(const ftcmd_cmd_t* commands, ftcmd_cmd_cb_t unknown_cmd_cb)
{
	self.cmd_table = commands;
	self.unknown_cmd_cb = unknown_cmd_cb;
	self.pos = 0;
}

void ftcmd_fire()
{
	if(!read_in()) {
		// We do not have the full line yet
		return;
	}

	if (self.in_buff[0] != CMD_CHAR) {
		self.pos = 0;
		return;
	}

	unsigned int params = read_cmd();

	const char* param_buff[params];

	read_params(param_buff);

	bool done = process_cmd(param_buff, params, common_cmd);

	if (!done) {
		done = process_cmd(param_buff, params, self.cmd_table);
	}

	if (!done) {
		self.unknown_cmd_cb(param_buff, params);
	}

	self.pos = 0;
}
