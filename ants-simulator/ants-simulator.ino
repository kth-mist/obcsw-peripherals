/**
 * @file ants-simulator.ino
 */

#include <inttypes.h>
#include <stdlib.h>

#include <Wire.h>

#include <arduino-timer.h>

#include "ftcmd.h"

extern "C" {
	#include "antssim.h"
}

#define I2C_BITRATE (400000)

//------------------------------------------------------------------------------
// Function Prototypes
//------------------------------------------------------------------------------
void ants_i2ca_receive_cb(int len);
void ants_i2ca_request_cb(void);
void ants_i2cb_receive_cb(int len);
void ants_i2cb_request_cb(void);

//------------------------------------------------------------------------------
// Variable Declarations
//------------------------------------------------------------------------------
uint8_t ants_receive_buffer[2];
uint8_t ants_send_buffer[8];

extern Timer<> timer_deployment, timer_dig_switch;


//------------------------------------------------------------------------------
// Arduino Code
//------------------------------------------------------------------------------
void setup(void)
{
	antssim_init();

	Wire.begin(ANTS_I2CADDR_SIDE_A);
	Wire.setClock(I2C_BITRATE);
	Wire.onReceive(ants_i2ca_receive_cb);
	Wire.onRequest(ants_i2ca_request_cb);

	Wire1.begin(ANTS_I2CADDR_SIDE_B);
	Wire1.setClock(I2C_BITRATE);
	Wire1.onReceive(ants_i2cb_receive_cb);
	Wire1.onRequest(ants_i2cb_request_cb);

	Serial.begin(115200);

	Serial.println("AntS Simulator started up.");
}

void loop(void)
{
	timer_deployment.tick();
	timer_dig_switch.tick();

	ftcmd_fire();
}

//------------------------------------------------------------------------------
// I2C Callbacks
//------------------------------------------------------------------------------
/**
 * AntS Sim. I2CA receive callback
 */
void ants_i2ca_receive_cb(int len)
{
	int ret;
	size_t datalen = 0;

	(void) len;

	while (Wire.available()) {
		if (datalen < sizeof(ants_receive_buffer))
			ants_receive_buffer[datalen++] = Wire.read();
		else
			(void) Wire.read();
	}

	ret = antssim_receive_i2c(ants_receive_buffer, datalen);

	/* TODO: Handle return code here. */
}

/**
 * AntS Sim. I2CA request callback.
 */
void ants_i2ca_request_cb(void)
{
	size_t datalen;

	datalen = antssim_get_i2c_response(ants_send_buffer, 
	                                   sizeof(ants_send_buffer));
	if (datalen == 0) {
		const uint8_t tmpdata[] = {0xff};
		Wire.write(tmpdata, 1);
	} else {
		Wire.write(ants_send_buffer, datalen);
	}
}

/**
 * AntS Sim. I2CB receive callback
 */
void ants_i2cb_receive_cb(int len)
{
	int ret;
	size_t datalen = 0;

	(void) len;

	while (Wire1.available()) {
		if (datalen < sizeof(ants_receive_buffer))
			ants_receive_buffer[datalen++] = Wire1.read();
		else
			(void) Wire1.read();
	}

	ret = antssim_receive_i2c(ants_receive_buffer, datalen);

	/* TODO: Handle return code here. */
}

/**
 * AntS Sim. I2CB request callback.
 */
void ants_i2cb_request_cb(void)
{
	size_t datalen;

	datalen = antssim_get_i2c_response(ants_send_buffer,
	                                   sizeof(ants_send_buffer));
	if (datalen == 0) {
		const uint8_t tmpdata[] = {0xff};
		Wire1.write(tmpdata, 1);
	} else {
		Wire1.write(ants_send_buffer, datalen);
	}
}
