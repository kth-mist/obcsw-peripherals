# AntS Simulator

[[_TOC_]]

## Introduction

This folder contains the latest version of the AntS Simulator. It is based on
previous work by Mustafa Al-Janabi and Louise Fischer.

The AntS Simulator is meant to provide a realistic simulation of the behaviour
of the Antenna System (AntS) on the MIST satellite, a subsystem designed by
Innovative Solutions In Space, B.V. AntS holds the four antennas used on MIST
in a stowed state until they are to be deployed in space. The AntS Simulator is
used during Functional Testing to simulate the power draw from the AntS without
the need to deploy the antennas and avoid a (mostly time) costly refurbishment
procedure for the AntS. The AntS Simulator implements all the I2C commands on
the AntS and uses power resistors to simulate the current drawn by the AntS
during deployment.

The AntS Simulator simulates both microcontrollers on the AntS (side A and side
B) via the I2C and I2C1 busses on the Arduino (`Wire` and `Wire1` in code).

The deployment switches on the AntS Simulator can either be simulated, the
so-called "digital deployment switches", or actual switches on the 3D-printed
part.

- Digital deployment switches are output pins on the Arduino Due which are set
  to digital _low_, as the deployment switches on the actual AntS are, after
  the deployment delay. The delay for these switches can be configured using
  [FT Commands](#ft-commands), aiding in testing various scenarios during
  functional testing.
  - **Note:** For the digital deployment switches to be read properly by the
    AntS Simulator, they need to be shorted to the deployment switch input pins
    using jumpers. See **M631-002** (version 3 onwards) for details.
- The actual switches on the 3D-printed part are triggered when the "doors" on
  the 3D-printed part are open, simulating actual deployment of an antenna on
  the AntS.
  - **Note:** If the 3D-printed part is to be used again, the switches on it
    should be switched for active-low ones. The current ones are active-high,
    which is in contradiction to the actual AntS and the current firmware
    implementation of the AntS Simulator.
    

## Getting Started

This section is a stripped-down version of document **M631-002**, see this
document for details, including pictures of the hardware setup.

These steps can be followed to perform a basic test of the Arduino Due-based
AntS Simulator:

1. Prepare (connect to your computer) an Arduino Due for the Dummy Master (for
   details, see document **M631-002** starting with v3)
2. Program this Arduino Due with the code under the `ants-simulator` [repository](https://gitlab.com/kth-mist/ants-simulator)
3. Prepare the AntS Simulator (or another Arduino Due to be used for the AntS
   Simulator, if a new one is to be set up)
   - _(Optional) Attach the AntS Simulator shield to the Arduino Due, if a new_
     _Due is to be used. See document **M631-002**, starting with v3, for_
     _details._
4. Send the `:arm` [command](https://gitlab.com/kth-mist/ftgrp/ftcmd/-/blob/master/ft-commands.md#arm)
   to the AntS Dummy Master via a Serial Monitor program, e.g., the one
   supplied with the Arduino IDE.
5. Observe the "Armed" LED turning on on the AntS Simulator.
6. Send the `:auto-seq` [command](https://gitlab.com/kth-mist/ftgrp/ftcmd/-/blob/master/ft-commands.md#auto-seq)
   to the AntS Dummy Master.
7. Observe the deployment LEDs turning on and off in sequence on the AntS
   Simulator; each LED should be on for 3 s.

_(Optional) Send a `:reset` command via the AntS Simulator, then repeat the_
_steps above, using [FT Commands](#ft-commands) to apply different deployment_
_delays and re-test. Note that the LED will turn off later or earlier than 3 s,_
_depending on the delay configured via FT commands._

## Hardware

The AntS Simulator is an Arduino Due with the shield applied to it and
programmed with the code under this folder.

- AntS Simulator electrical schematics:
  - [Link on Dropbox](https://www.dropbox.com/home/MIST/M600%20-%20Assembly%2C%20Integration%2C%20Test/M620%20-%20Test%20Equipment/M622%20-%20AntS%20Simulator/4%20-%20schematics?preview=AntS_sim_schematics.pdf)

- AntS Simulator (side view)
  
  <img src="img/ants-sim-hardware.jpg" alt="AntS Simulator Hardware" width=600>

- AntS Simulator (top view)

  <img src="img/ants-sim-hardware-top-view.jpg" alt="AntS Simulator Hardware Top View" height=600>

## Firmware

### Code Structure

- The `ants-simulator.ino` file is the main Arduino file for the AntS Simulator
  code. It contains the `setup()` and `loop()` functions, as well as the
  implementations of callback functions for the I2C busses and FT commands.
- The `antssim.c/h` files contain implementations for AntS Simulator-specific
  functions.
- The `arduino-timer-wrapper.c/h` files contain C wrapper functions for the C++
  functions of the [arduino-timer library](https://www.arduino.cc/reference/en/libraries/arduino-timer/).
- The `ftcmd*` and `version.h` files contain the library for FT commands parsing
  on character reception via the `Serial` line.
  - **Note:** The callbacks for the AntS Simulator-specific FT commands are
    implemented in `ants-simulator.ino`.

### Deployment Logic

The code flow for antenna "deployment" using the AntS Simulator is as follows:

1. `ants_i2ca_receive_cb()` or `ants_i2cb_receive_cb()` in `ants-simulator.ino`
   are called when an I2C command is received;
2. `antssim_receive_i2c()` in `antssim.c` interprets this commnad;
3. `deploy_start()` in `antssim.c` is called if the command is a "deploy";
4. `timer_deployment_cb()` counts total deployment time and checks if the
   deployment time target is reached;
5. `deploy_end()`, called from either:
   - `timer_deployment_cb()`, if deployment time sent by I2C command reached;
   - `depl_switch_isr()`, if the deployment switch (either simulated or
     actual switch on 3D-printed part) triggers;
   - `antssim_receive_i2c()`, if either _Cancel deployment_ or _Disarm_ command
     is received via I2C.

In more detail:

1. A deployment command (deploy antenna X/deploy antenna X with override/
   sequential auto-deployment) is received via I2C, either on the side A bus
   (`Wire`) or the side B bus (`Wire1`);
2. The command is decoded by the `antssim_receive_i2c()` function inside
   `antssim.c` (called by the I2C callback, `ants_i2ca_receive_cb()` or
   `ants_i2cb_receive_cb()`, depending which AntS side the command was sent
   to);
3. The `deploy_start()` function inside `antssim.c` starts the deployment,
   setting the burnwire pin _high_, which triggers the shield to consume the
   power the AntS would during deployment. A timer is started to count the
   number of seconds (the target time) when the burnwire pin should be set
   back _low_. The target time is set based on the parameter of the I2C
  deploy command. A "digital switch" timer is also started to count the number
  of milliseconds until the digital deployment switch is set _low_;
   - **Note:** The period of the deployment timer is set in 50 ms units, as the
     timer callback is used to count the total activation time, which the AntS
     reports in 50 ms units.
4. Every 50 ms the `timer_deployment_cb()` function is called, incrementing the
   total activation time.
5. Deployment ends (the burnwire is set _low_) via the `deploy_end()` function
   in either of these situations:
   - If the time target is reached in `timer_deployment_cb()`;
   - If the deployment switch for the "antenna" being "deployed" triggers. The
     `depl_switch_isr()` function is called any of the deployment switch pins
     have a falling edge transition on them;
     - **Note:** The deployment switch can be simulated ("digital switch") or
       the actual deployment switches on the 3D-printed part, should that be
       used.
   - If the _Disarm_ or _Cancel deployment_ I2C commands are received.

### Responding to Request Commands

The code flow for responding to _Request_ commands to the AntS over I2C is:

1. `ants_i2ca_request_cb()` or `ants_i2cb_request_cb()` in `ants-simulator.ino`
   is called;
2. `antssim_get_i2c_response()` is called to prepare data to be sent back on
   the I2C bus when the repeated start condition occurs;

### FT Commands

The synopsis for supported FT Commands on the AntS Simulator can be found in
the [ft-commands.md file](https://gitlab.com/kth-mist/ftgrp/ftcmd/-/blob/master/ft-commands.md#ants-simulator).

- `:set-dig-switch-delay` can be used to set the delay in milliseconds between
  the time the deployment command is reached and the time the digital
  deployment switch is pulled _low_ by the Arduino Due. If the digital
  deployment switch pins are shorted to the corresponding deployment switch
  input pins, the AntS Simulator detects a deployment and ends the deployment
  sequence (unless a "deploy with override" command has been received, in which
  case the deployment switches are ignored).
- `:set-temp` can be used to set a raw ADC value for the simulated temperature
  sensor on the AntS Simulator. The default raw ADC value corresponds to 25
  deg. C, but any value in the range shown under the table in Annex A of the
  [AntS User Manual](https://www.dropbox.com/s/w7pcx7icgr50ffh/ISIS.AntS.UM.001_Antenna%20System%20User%20Manual_v1.4.pdf?dl=0)
  can be set, according to the following formula:
  - _raw_adc = Vout/3300 * 1024_
