#ifndef ARDUINO_TIMER_WRAPPER
#define ARDUINO_TIMER_WRAPPER

#ifdef __cplusplus
#define EXTERNC extern "C"
#else
#define EXTERNC
#endif

#include <Arduino.h>

/* @brief Used in the QEMU/HAL-clone setup to differentiate
          between Arduino simulators */
#define ARDUINO_UNIT_ID_ANTS      (31)

enum timer {
    TIMER_DEPLOYMENT = 0,
    TIMER_DIG_SWITCH
};

EXTERNC void timer_every(
    unsigned long interval_ms,
    bool (*callback)(void*),
    enum timer t
);

EXTERNC void timer_in(
    unsigned long delay_ms,
    bool (*callback)(void*),
    enum timer t
);

EXTERNC void timer_cancel(enum timer t);

#undef EXTERNC

#endif // ARDUINO_TIMER_WRAPPER