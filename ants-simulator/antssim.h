/**
 * @file   antssim.h
 * @author William Stackenäs
 * @author Theodor Stana
 */

#ifndef ANTS_COMMAND_HANDLER_H
#define ANTS_COMMAND_HANDLER_H

#include <inttypes.h>

/* @brief Used in the QEMU/HAL-clone setup to differentiate
          between Arduino simulators */
#define ARDUINO_UNIT_ID_ANTS      (31)

#define ANTS_I2CADDR_SIDE_A       (0x31)
#define ANTS_I2CADDR_SIDE_B       (0x32)

#define ANTS_SAFETY_TIME_LIMIT    (30) // seconds

#define ANTS_ARMED_LED_PIN        ( 2)
#define ANTS_BURNWIRE_1_PIN       ( 3)
#define ANTS_BURNWIRE_2_PIN       ( 4)
#define ANTS_BURNWIRE_3_PIN       ( 5)
#define ANTS_BURNWIRE_4_PIN       ( 6)
#define ANTS_DEPL_SWITCH_1_PIN    (35)
#define ANTS_DEPL_SWITCH_2_PIN    (33)
#define ANTS_DEPL_SWITCH_3_PIN    (31)
#define ANTS_DEPL_SWITCH_4_PIN    (29)
#define ANTS_DIG_SWITCH_1_PIN     (28)
#define ANTS_DIG_SWITCH_2_PIN     (26)
#define ANTS_DIG_SWITCH_3_PIN     (24)
#define ANTS_DIG_SWITCH_4_PIN     (22)

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Initialize the AntS Simulator
 * 
 * Configure the various pins used by it and attach the ISR to the deployment
 * switch pins.
 */
void antssim_init();

/**
 * @brief Decode AntS I2C commands
 * 
 * @param data input data buffer
 * @param datalen number of bytes received in `data`
 * @return 1     if the data buffer does not exist, no bytes have been received,
 *               or an unknown I2C command for the AntS has been received
 *         2..4  see the `deploy_start()` function
 *         -1    see the `deploy_start()` function
 */
int antssim_receive_i2c(const uint8_t *data, size_t datalen);

/**
 * @brief Prepare the response to a "report" type I2C command to the AntS
 * 
 * @param buf output buffer containing the response data
 * @param bufsize max. size of `buf`
 * @return int number of bytes written to `buf`
 */
int antssim_get_i2c_response(uint8_t *buf, size_t bufsize);

/**
 * @brief Set the delay for the digital deployment switches
 * 
 * The digital deployment switches are output pins simulating the function of
 * the deployment switches on the AntS. A delay can be programmed to the AntS
 * Simulator between the time an antenna deployment is initiated and the time
 * the deployment switches should "trip", indicating a deployed antenna. This is
 * so that different delays can be programmed and the OBC's reaction gauged
 * during functional testing of MIST.
 * 
 * After the number of milliseconds configured via the parameter, the digital
 * switch pins are pulled _low_, indicating deployment. If the pins are shorted
 * to the deployment switch input pins, the AntS Simulator will detect a
 * deployed state and the deployment will be ended (for a normal, non-override
 * deployment command; for a "deploy with override" command the deployment will
 * continue for the requested amount of time)
 * 
 * @param ant antenna to set the digital deployment switch delay for
 * @param delay_ms number of milliseconds until "deployment"
 */
void antssim_set_dig_switch_delay(uint8_t ant, uint32_t delay_ms);

/**
 * @brief Set the temperature readout for the AntS Simulator
 * 
 * This function can be used during functional testing to set different
 * temperatures on the AntS Simulator. It can for example be used when testing
 * housekeeping readout from the sub-systems mission simulations.
 * 
 * The default temperature sensor value (before using this function) is 25 degC.
 * 
 * @param degC temperatures in degrees Celsius
 */
void antssim_set_temp(int degC);

#ifdef __cplusplus
}
#endif

#endif /* ANTS_COMMAND_HANDLER_H */
