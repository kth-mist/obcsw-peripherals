/**
 * @file ftcmd_common.cpp
 *
 * @brief Implementation of common commands for the FT serial interface.
 *
 * @version v0.1.0-dev
 */

#include "ftcmd.h"
#include "version.h"
#include "Arduino.h"

/* Global variables ***********************************************************/
extern const ftcmd_cmd_t common_cmd[];

/* Function prototypes ********************************************************/
static void cmd_id_cb(const char** argv, unsigned int argn);
static void cmd_ver_cb(const char** argv, unsigned int argn);

/* Callbacks ******************************************************************/
static void cmd_id_cb(const char** argv, unsigned int argn)
{
	Serial.print("+id;");
	Serial.println(PRJ_NAME);
}

static void cmd_ver_cb(const char** argv, unsigned int argn)
{
	Serial.print("+ver;");
	Serial.println("v" VERSION_STR);
}

/* Function definitions *******************************************************/
/* Public functions ***********************************************************/
/* Public variables ***********************************************************/
const ftcmd_cmd_t common_cmd[] = {
	{"id", cmd_id_cb},
	{"ver", cmd_ver_cb},
	{NULL, NULL},
};
