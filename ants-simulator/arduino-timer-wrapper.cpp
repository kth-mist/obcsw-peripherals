#include "arduino-timer-wrapper.h"

#include <arduino-timer.h>

#define ARDUINO_UNIT_ID ARDUINO_UNIT_ID_ANTS

Timer<> timer_deployment, timer_dig_switch;

void timer_every(
    unsigned long interval_ms,
    bool (*callback)(void*),
    enum timer t
)
{
    switch (t) {
        case TIMER_DEPLOYMENT:
            timer_deployment.every(interval_ms, callback);
            break;
        case TIMER_DIG_SWITCH:
            timer_dig_switch.every(interval_ms, callback);
            break;
        default:
            break;
    }
}

void timer_in(
    unsigned long delay_ms,
    bool (*callback)(void*),
    enum timer t
)
{
    switch (t) {
        case TIMER_DEPLOYMENT:
            timer_deployment.in(delay_ms, callback);
            break;
        case TIMER_DIG_SWITCH:
            timer_dig_switch.in(delay_ms, callback);
            break;
        default:
            break;
    } 
}

void timer_cancel(enum timer t)
{
    switch (t) {
        case TIMER_DEPLOYMENT:
            timer_deployment.cancel();
            break;
        case TIMER_DIG_SWITCH:
            timer_dig_switch.cancel();
            break;
        default:
            break;
    }
}
