/**
 * @file   ants_command_codes.h
 * @author William Stackenäs
 * @author Theodor Stana
 */

#ifndef ANTS_COMMAND_CODES_H
#define ANTS_COMMAND_CODES_H

#define ANTS_CMD_RESET  0b10101010
#define ANTS_CMD_ARM  0b10101101
#define ANTS_CMD_DISARM  0b10101100
#define ANTS_CMD_AUTO_SEQ  0b10100101
#define ANTS_CMD_CANCEL  0b10101001
#define ANTS_CMD_TEMP  0b11000000
#define ANTS_CMD_REPORT_STATUS  0b11000011
#define ANTS_CMD_UPTIME  0b11000110
#define ANTS_CMD_ALL_TELEMETRY  0b11000111

#define ANTS_CMD_ANTENNA1_DEPLOY  0b10100001
#define ANTS_CMD_ANTENNA1_DEPLOY_OVERRIDE  0b10111010
#define ANTS_CMD_ANTENNA1_ACTIVATION_COUNT  0b10110000
#define ANTS_CMD_ANTENNA1_TOTAL_ACTIVATION_TIME  0b10110100

#define ANTS_CMD_ANTENNA2_DEPLOY  0b10100010
#define ANTS_CMD_ANTENNA2_DEPLOY_OVERRIDE  0b10111011
#define ANTS_CMD_ANTENNA2_ACTIVATION_COUNT  0b10110001
#define ANTS_CMD_ANTENNA2_TOTAL_ACTIVATION_TIME  0b10110101

#define ANTS_CMD_ANTENNA3_DEPLOY  0b10100011
#define ANTS_CMD_ANTENNA3_DEPLOY_OVERRIDE  0b10111100
#define ANTS_CMD_ANTENNA3_ACTIVATION_COUNT  0b10110010
#define ANTS_CMD_ANTENNA3_TOTAL_ACTIVATION_TIME  0b10110110

#define ANTS_CMD_ANTENNA4_DEPLOY  0b10100100
#define ANTS_CMD_ANTENNA4_DEPLOY_OVERRIDE  0b10111101
#define ANTS_CMD_ANTENNA4_ACTIVATION_COUNT  0b10110011
#define ANTS_CMD_ANTENNA4_TOTAL_ACTIVATION_TIME  0b10110111


#endif /* ANTS_COMMAND_CODES_H */
