/**
 * @file   antssim.cpp
 * @author William Stackenäs
 * @author Theodor Stana
 */

#include <Arduino.h>

#include <stdlib.h>
#include <stdint.h>

#include <include/rstc.h>

#include "arduino-timer-wrapper.h"

#include "ants_command_codes.h"
#include "antssim.h"

#include "ftcmd.h"

#define ARDUINO_UNIT_ID ARDUINO_UNIT_ID_ANTS

//------------------------------------------------------------------------------
// Local Variables
//------------------------------------------------------------------------------
static uint8_t last_cmd;

// State variables
static bool armed = false;
static bool deploying[4] = {false, false, false, false};
static bool timeout[4] = {false, false, false, false};
static bool auto_deploying = false;
static bool override = false;

// Deployment variables, starting with the antenna under deployment
static int antenna = 255;
static uint16_t deploy_time = 0;
static uint16_t deploy_time_target = 0;
static uint8_t activation_count[4] = {0, 0, 0, 0};
static uint8_t activation_count_reply;
static uint16_t activation_time[4] = {0, 0, 0, 0};
static uint16_t activation_time_reply;

static uint16_t status;
static uint32_t uptime;
// Temperature raw ADC value; default corresponds to 25 deg. C: 1831/3300 * 1024
static uint16_t temp_raw_adc = 568;

/*
 * Delay to digital switch assertion, in milliseconds; defaults to 3 s, the
 * typical deployment time quoted by ISIS in the AntS datasheet.
 */
static uint32_t dig_switch_delay[4] = {3000, 3000, 3000, 3000};

// Timers
static bool timer_deployment_cb(void *);
static bool timer_dig_switch_cb(void *);

//------------------------------------------------------------------------------
// Local Function Prototypes
//------------------------------------------------------------------------------
static int deploy_start(int ant, uint8_t duration);
static void deploy_end();
static void deploy_next();
static bool any_deploying();
static bool deployed(int ant);
static void depl_switch_isr();
static void set_deployment_status();

//------------------------------------------------------------------------------
// FT Commands
//------------------------------------------------------------------------------
static void set_dig_switch_delay_cb(const char** argv, unsigned int argn);
static void set_temp_cb(const char** argv, unsigned int argn);

static const ftcmd_cmd_t cmd_table[] = {
	{"set-dig-switch-delay", set_dig_switch_delay_cb},
	{"set-temp", set_temp_cb},
	{NULL, NULL},
};

static void set_dig_switch_delay_cb(const char** argv, unsigned int argn)
{
	int err = 0;
	uint8_t antenna;
	uint32_t delay_ms;


	if (argn != 3) {
		err = 1;
	} else {
		antenna = atoi(argv[1]);
		delay_ms = atoi(argv[2]);
		if ((antenna < 1) || (antenna > 4))
			err = 2;
		else if ((delay_ms > 255000))
			err = 3;
		else
			antssim_set_dig_switch_delay(antenna-1, delay_ms);
	}

	Serial.print("+");
	Serial.print(argv[0]);
	Serial.print(";");
	Serial.println(err);
}

static void set_temp_cb(const char** argv, unsigned int argn)
{
	int err = 0;
	int temp;

	if (argn != 2) {
		err = 1;
	} else {
		temp = atoi(argv[1]);
		if ((temp < -50) || (temp > 150))
			err = 2;
		else
			antssim_set_temp(temp);
	}

	Serial.print("+");
	Serial.print(argv[0]);
	Serial.print(";");
	Serial.println(err);
}

static void unknown_cmd_cb(const char** argv, unsigned int argn)
{
	(void) argn;

	Serial.print("Invalid cmd: "); Serial.println(argv[0]);
}

//------------------------------------------------------------------------------
// Global Function Implementations
//------------------------------------------------------------------------------

/**
 * @brief Initialize the AntS Simulator
 * 
 * Configure the various pins used by it and attach the ISR to the deployment
 * switch pins.
 */
void antssim_init()
{
	for (int i = 0; i < 4; i++) {
		deploying[i] = false;
		timeout[i] = false;
		activation_count[i] = 0;
		activation_time[i] = 0;
	}
	armed = false;
	auto_deploying = false;
	override = false;
	antenna = 255;
	deploy_time = 0;
	deploy_time_target = 0;
	temp_raw_adc = 568;

	pinMode(ANTS_ARMED_LED_PIN, OUTPUT);

	/* Burnwire pins: Outputs */
	pinMode(ANTS_BURNWIRE_1_PIN, OUTPUT);
	pinMode(ANTS_BURNWIRE_2_PIN, OUTPUT);
	pinMode(ANTS_BURNWIRE_3_PIN, OUTPUT);
	pinMode(ANTS_BURNWIRE_4_PIN, OUTPUT);

	/* Digital switches: Outputs, active LOW */
	pinMode(ANTS_DIG_SWITCH_1_PIN, OUTPUT);
	pinMode(ANTS_DIG_SWITCH_2_PIN, OUTPUT);
	pinMode(ANTS_DIG_SWITCH_3_PIN, OUTPUT);
	pinMode(ANTS_DIG_SWITCH_4_PIN, OUTPUT);
	digitalWrite(ANTS_DIG_SWITCH_1_PIN, HIGH);
	digitalWrite(ANTS_DIG_SWITCH_2_PIN, HIGH);
	digitalWrite(ANTS_DIG_SWITCH_3_PIN, HIGH);
	digitalWrite(ANTS_DIG_SWITCH_4_PIN, HIGH);

	/* Deployment switches: Inputs */
	pinMode(ANTS_DEPL_SWITCH_1_PIN, INPUT);
	pinMode(ANTS_DEPL_SWITCH_2_PIN, INPUT);
	pinMode(ANTS_DEPL_SWITCH_3_PIN, INPUT);
	pinMode(ANTS_DEPL_SWITCH_4_PIN, INPUT);
	attachInterrupt(digitalPinToInterrupt(ANTS_DEPL_SWITCH_1_PIN),
		depl_switch_isr, FALLING);
	attachInterrupt(digitalPinToInterrupt(ANTS_DEPL_SWITCH_2_PIN),
		depl_switch_isr, FALLING);
	attachInterrupt(digitalPinToInterrupt(ANTS_DEPL_SWITCH_3_PIN),
		depl_switch_isr, FALLING);
	attachInterrupt(digitalPinToInterrupt(ANTS_DEPL_SWITCH_4_PIN),
		depl_switch_isr, FALLING);

	ftcmd_init(cmd_table, unknown_cmd_cb);
}


/**
 * @brief Decode AntS I2C commands
 * 
 * @param data input data buffer
 * @param datalen number of bytes received in `data`
 * @return 1     if the data buffer does not exist, no bytes have been received,
 *               or an unknown I2C command for the AntS has been received
 *         2..4  see the `deploy_start()` function
 *         -1    see the `deploy_start()` function
 */
int antssim_receive_i2c(const uint8_t *data, size_t datalen)
{
	int ret = 0;
	uint8_t cmd;

	if (data == NULL || datalen < 1)
		return 1;

	cmd = data[0];

	switch (cmd) {
	case ANTS_CMD_RESET:
		rstc_start_software_reset(RSTC);
		break;
	case ANTS_CMD_ARM:
		armed = true;
		digitalWrite(ANTS_ARMED_LED_PIN, HIGH);
		break;
	case ANTS_CMD_DISARM:
		auto_deploying = false;
		timer_cancel(TIMER_DIG_SWITCH);
		override = false;
		deploy_end();
		armed = false;
		digitalWrite(ANTS_ARMED_LED_PIN, LOW);
		break;
	case ANTS_CMD_CANCEL:
		auto_deploying = false;
		override = false;
		timer_cancel(TIMER_DIG_SWITCH);
		deploy_end();
		break;
	case ANTS_CMD_ANTENNA1_DEPLOY:
		ret = deploy_start(0, data[1]);
		break;
	case ANTS_CMD_ANTENNA2_DEPLOY:
		ret = deploy_start(1, data[1]);
		break;
	case ANTS_CMD_ANTENNA3_DEPLOY:
		ret = deploy_start(2, data[1]);
		break;
	case ANTS_CMD_ANTENNA4_DEPLOY:
		ret = deploy_start(3, data[1]);
		break;
	case ANTS_CMD_AUTO_SEQ:
	{
		int ant = 0;
		auto_deploying = true;
		while ((ret = deploy_start(ant, data[1])) == 4) {
			// Move to the next antenna if this one is already deployed
			ant++;
			if (ant > 3) {
				auto_deploying = false;
				ret = 0;
				break;
			}
		}
		break;
	}
	case ANTS_CMD_ANTENNA1_DEPLOY_OVERRIDE:
		override = true;
		ret = deploy_start(0, data[1]);
		break;
	case ANTS_CMD_ANTENNA2_DEPLOY_OVERRIDE:
		override = true;
		ret = deploy_start(1, data[1]);
		break;
	case ANTS_CMD_ANTENNA3_DEPLOY_OVERRIDE:
		override = true;
		ret = deploy_start(2, data[1]);
		break;
	case ANTS_CMD_ANTENNA4_DEPLOY_OVERRIDE:
		override = true;
		ret = deploy_start(3, data[1]);
		break;
	case ANTS_CMD_TEMP:
		/* Temp. already set */
		break;
	case ANTS_CMD_REPORT_STATUS:
		set_deployment_status();
		break;
	case ANTS_CMD_ANTENNA1_ACTIVATION_COUNT:
		activation_count_reply = activation_count[0];
		break;
	case ANTS_CMD_ANTENNA2_ACTIVATION_COUNT:
		activation_count_reply = activation_count[1];
		break;
	case ANTS_CMD_ANTENNA3_ACTIVATION_COUNT:
		activation_count_reply = activation_count[2];
		break;
	case ANTS_CMD_ANTENNA4_ACTIVATION_COUNT:
		activation_count_reply = activation_count[3];
		break;
	case ANTS_CMD_ANTENNA1_TOTAL_ACTIVATION_TIME:
		activation_time_reply = activation_time[0];
		break;
	case ANTS_CMD_ANTENNA2_TOTAL_ACTIVATION_TIME:
		activation_time_reply = activation_time[1];
		break;
	case ANTS_CMD_ANTENNA3_TOTAL_ACTIVATION_TIME:
		activation_time_reply = activation_time[2];
		break;
	case ANTS_CMD_ANTENNA4_TOTAL_ACTIVATION_TIME:
		activation_time_reply = activation_time[3];
		break;
	case ANTS_CMD_UPTIME:
		uptime = millis() / 1000;
		break;
	case ANTS_CMD_ALL_TELEMETRY:
		/* Temp. already set */
		set_deployment_status();
		uptime = millis() / 1000;
		break;
	default:
		ret = 1;
		break;
	}

	if (ret == 0)
		last_cmd = cmd;

	return ret;
}


/**
 * @brief Prepare the response to a "report" type I2C command to the AntS
 * 
 * @param buf output buffer containing the response data
 * @param bufsize max. size of `buf`
 * @return int number of bytes written to `buf`
 */
int antssim_get_i2c_response(uint8_t *buf, size_t bufsize)
{
	int len = 0;

	if (buf == NULL)
		return 0;

	switch (last_cmd) {
	case ANTS_CMD_TEMP:
		buf[0] = temp_raw_adc & 0xFF;
		buf[1] = (temp_raw_adc >> 8) & 0xFF;
		len = 2;
		break;
	case ANTS_CMD_REPORT_STATUS:
		buf[0] = status & 0xFF;
		buf[1] = (status >> 8) & 0xFF;
		len = 2;
		break;
	case ANTS_CMD_ANTENNA1_ACTIVATION_COUNT:
	case ANTS_CMD_ANTENNA2_ACTIVATION_COUNT:
	case ANTS_CMD_ANTENNA3_ACTIVATION_COUNT:
	case ANTS_CMD_ANTENNA4_ACTIVATION_COUNT:
		buf[0] = activation_count_reply;
		len = 1;
		break;
	case ANTS_CMD_ANTENNA1_TOTAL_ACTIVATION_TIME:
	case ANTS_CMD_ANTENNA2_TOTAL_ACTIVATION_TIME:
	case ANTS_CMD_ANTENNA3_TOTAL_ACTIVATION_TIME:
	case ANTS_CMD_ANTENNA4_TOTAL_ACTIVATION_TIME:
		buf[0] = activation_time_reply & 0xFF;
		buf[1] = (activation_time_reply >> 8) & 0xFF;
		len = 2;
		break;
	case ANTS_CMD_UPTIME:
		buf[0] = uptime & 0xFF;
		buf[1] = (uptime >>  8) & 0xFF;
		buf[2] = (uptime >> 16) & 0xFF;
		buf[3] = (uptime >> 24) & 0xFF;
		len = 4;
		break;
	case ANTS_CMD_ALL_TELEMETRY:
		buf[0] = temp_raw_adc & 0xFF;
		buf[1] = (temp_raw_adc >> 8) & 0xFF;
		buf[2] = status & 0xFF;
		buf[3] = (status >> 8) & 0xFf;
		buf[4] = uptime & 0xFF;
		buf[5] = (uptime >>  8) & 0xFF;
		buf[6] = (uptime >> 16) & 0xFF;
		buf[7] = (uptime >> 24) & 0xFF;
		len = 8;
		break;
	default:
		break;
	}

	/* Truncate to buffer size */
	if (len > bufsize)
		len = bufsize;

	return len;
}


/**
 * @brief Set the delay for the digital deployment switches
 * 
 * The digital deployment switches are output pins simulating the function of
 * the deployment switches on the AntS. A delay can be programmed to the AntS
 * Simulator between the time an antenna deployment is initiated and the time
 * the deployment switches should "trip", indicating a deployed antenna. This is
 * so that different delays can be programmed and the OBC's reaction gauged
 * during functional testing of MIST.
 * 
 * After the number of milliseconds configured via the parameter, the digital
 * switch pins are pulled _low_, indicating deployment. If the pins are shorted
 * to the deployment switch input pins, the AntS Simulator will detect a
 * deployed state and the deployment will be ended (for a normal, non-override
 * deployment command; for a "deploy with override" command the deployment will
 * continue for the requested amount of time)
 * 
 * @param ant antenna to set the digital deployment switch delay for
 * @param delay_ms number of milliseconds until "deployment"
 */
void antssim_set_dig_switch_delay(uint8_t ant, uint32_t delay_ms)
{
	if (ant < 4)
		dig_switch_delay[ant] = delay_ms;
}


/**
 * @brief Set the temperature readout for the AntS Simulator
 * 
 * This function can be used during functional testing to set different
 * temperatures on the AntS Simulator. It can for example be used when testing
 * housekeeping readout from the sub-systems mission simulations.
 * 
 * The default temperature sensor value (before using this function) is 25 degC.
 * 
 * @param degC temperatures in degrees Celsius
 */
void antssim_set_temp(int degC)
{
	temp_raw_adc = (uint16_t)((190.65 - degC)/0.2922);
}


//------------------------------------------------------------------------------
// Local Function Implementations
//------------------------------------------------------------------------------

/**
 * @brief Start deploying antenna X for the requested duration
 *
 * @param ant Antenna to deploy (0..3)
 * @param duration Duration of deployment (0..255 -- but any value greater than
 *                 ANTS_SAFETY_TIME_LIMIT will be truncated)
 * @return 2 if the system is not armed
 *         3 if another "antenna" is already deploying
 *         4 if at the start of the function, the deployment switch indicates
 *           the antenna is already deployed
 *        -1 in the off-case that a call to this function is made with the wrong
 *           antenna to deploy (`ant` is not one of 0..3)
 */
static int deploy_start(int ant, uint8_t duration)
{
	if (!armed)
		return 2;

	if (any_deploying())
		return 3;

	if (!override && deployed(ant))
		return 4;

	switch (ant) {
		case 0:
			digitalWrite(ANTS_BURNWIRE_1_PIN, HIGH);
			break;
		case 1:
			digitalWrite(ANTS_BURNWIRE_2_PIN, HIGH);
			break;
		case 2:
			digitalWrite(ANTS_BURNWIRE_3_PIN, HIGH);
			break;
		case 3:
			digitalWrite(ANTS_BURNWIRE_4_PIN, HIGH);
			break;
		default:
			return -1;
	}
	antenna = ant;
	deploying[antenna] = true;
	++activation_count[antenna];

	// Count deployment time in 50 ms increments, corresponding to the time
	// resolution of the activation time counter on the AntS
	deploy_time = 0;
	deploy_time_target = (duration <= ANTS_SAFETY_TIME_LIMIT) ?
							(duration * 20) :
							(ANTS_SAFETY_TIME_LIMIT * 20);
	timer_every(50, timer_deployment_cb, TIMER_DEPLOYMENT);
	timer_in(dig_switch_delay[antenna], timer_dig_switch_cb, TIMER_DIG_SWITCH);

	return 0;
}

/**
 * @brief End deployment, cancelling the timer and setting the burnwire pins low
 */
static void deploy_end()
{
	timer_cancel(TIMER_DEPLOYMENT);
	deploying[antenna] = false;
	switch (antenna) {
		case 0:
			digitalWrite(ANTS_BURNWIRE_1_PIN, LOW);
			break;
		case 1:
			digitalWrite(ANTS_BURNWIRE_2_PIN, LOW);
			break;
		case 2:
			digitalWrite(ANTS_BURNWIRE_3_PIN, LOW);
			break;
		case 3:
			digitalWrite(ANTS_BURNWIRE_4_PIN, LOW);
			break;
		default:
			break;
	}
}

/**
 * @brief Deploy next antenna for use in auto-sequence
 *
 * NOTE: Needs external `if (auto_deploying)` for proper functioning.
 */
static void deploy_next()
{
	/*
	 * Use a while loop to increment the antenna number, so that if an
	 * antenna was already deployed, we can move to the next one. Stop when
	 * max. antenna number reached.
	 *
	 * `deploy_time_target` is divided by 20 as it is already in 50 ms units
	 * from the previous deployment.
	 */
	while (deploy_start(antenna, deploy_time_target/20) == 4) {
		antenna++;
		if (antenna == 4) {
			auto_deploying = false;
			break;
		}
	}
}

/**
 * @brief Report whether any antenna is deploying
 * 
 * @return true if any of the antennas are currently deploying
 * @return false if none of the antennas are currently deploying
 */
static bool any_deploying()
{
	return (deploying[0] || deploying[1] || deploying[2] || deploying[3]);
}

/**
 * @brief Report on the deployment status of a particular antenna
 * 
 * @param ant the antenna to check the deploymnet state of, range 0..3
 * @return true if the antenna is currently deployed (if its depl. switch is '0')
 * @return false if the antenna is currently not deployed (depl. switch is '1')
 */
static bool deployed(int ant)
{
	int pin;

	switch (ant) {
		case 0:
			pin = ANTS_DEPL_SWITCH_1_PIN;
			break;
		case 1:
			pin = ANTS_DEPL_SWITCH_2_PIN;
			break;
		case 2:
			pin = ANTS_DEPL_SWITCH_3_PIN;
			break;
		case 3:
			pin = ANTS_DEPL_SWITCH_4_PIN;
			break;
		default:
			return false;
	}

	return (digitalRead(pin) == LOW);
}

/**
 * @brief Deployment timer callback function
 * 
 * This function is used to count the total activation time for an antenna, that
 * can be reported on request command, and to end the deployment when the
 * deployment time requested via command has been reached. If an automatic
 * sequential deployment has been requested by command, deployment of the next
 * antenna is initiated automatically.
 * 
 * @param arg **unused** void pointer to accommodate to Arduino Timer library
 * @return true **always** -- this means the timer will always be restarted
 * @return false **never** -- the timer is stopped via `deploy_end()`
 */
static bool timer_deployment_cb(void *arg)
{
	(void)arg;

	if (++deploy_time < deploy_time_target) {
		++activation_time[antenna];
	} else {
		deploy_end();
		timeout[antenna] = true;
		override = false;
		if (auto_deploying)
			deploy_next();
	}

	return true;
}

/**
 * @brief Digital switch timer callback function
 * 
 * @param arg **unused** void pointer to accommodate to Arduino Timer library
 * @return true **never** -- this is a one-shot timer, it stops after one run
 * @return false **always** -- this is a one-shot timer, it stops after one run
 */
static bool timer_dig_switch_cb(void *arg)
{
	switch (antenna) {
		case 0:
			digitalWrite(ANTS_DIG_SWITCH_1_PIN, LOW);
			break;
		case 1:
			digitalWrite(ANTS_DIG_SWITCH_2_PIN, LOW);
			break;
		case 2:
			digitalWrite(ANTS_DIG_SWITCH_3_PIN, LOW);
			break;
		case 3:
			digitalWrite(ANTS_DIG_SWITCH_4_PIN, LOW);
			break;
		default:
			break;
	}
	return false;
}

/**
 * @brief Interrupt Service Routine for the deployment switches
 * 
 * This function executes when any of the deployment switch pins detects 
 * falling edge transition. It stops the on-going deployment and starts
 * deploying the next "antenna" if an automatic deployment was requested by the
 * user.
 */
static void depl_switch_isr()
{
	deploy_end();
	if (auto_deploying)
		deploy_next();
}

/**
 * @brief Store deployment status
 */
static void set_deployment_status()
{
	status = (deployed(0)    ? 0x0000 : 0x8000) |  // fcn. negated == pin status
	         (timeout[0]     ? 0x4000 : 0x0000) |
	         (deploying[0]   ? 0x2000 : 0x0000) |
	         (0x0)      /* NOT USED */      |
	         (deployed(1)    ? 0x0000 : 0x0800) |  // fcn. negated == pin status
	         (timeout[1]     ? 0x0400 : 0x0000) |
	         (deploying[1]   ? 0x0200 : 0x0000) |
	         (override       ? 0x0100 : 0x0000) |
	         (deployed(2)    ? 0x0000 : 0x0080) |  // fcn. negated == pin status
	         (timeout[2]     ? 0x0040 : 0x0000) |
	         (deploying[2]   ? 0x0020 : 0x0000) |
	         (auto_deploying ? 0x0010 : 0x0000) |
	         (deployed(3)    ? 0x0000 : 0x0008) |  // fcn. negated == pin status
	         (timeout[3]     ? 0x0004 : 0x0000) |
	         (deploying[3]   ? 0x0002 : 0x0000) |
	         (armed          ? 0x0001 : 0x0000);
}
