/**
 * @file ftcmd.h
 *
 * @brief Serial interface command reader
 *
 * This module allows to easily decode the commands received through the UART
 * interface and to call a specific callback for each command, which receives a
 * list with all the parameters received with that command.
 *
 * This module uses Arduino's Serial interface, so it should be previously
 * initialized with the desired parameters for the correct behavior of this
 * module.
 */
#ifndef __FTCMD_H
#define __FTCMD_H

#ifdef __cplusplus
extern "C"{
#endif

/**@brief Size of the input buffer to store the incoming characters */
#define FTCMD_IN_BUFFER_SIZE 50

/**
 * @brief Command callback
 *
 * @param argv Array with the strings received as parameters with the command.
 * @param argn Number of parameters.
 */
typedef void (*ftcmd_cmd_cb_t)(const char** argv, unsigned int argn);


/**@brief Command definition */
typedef struct ftcmd_cmd_t {
	const char* cmd;      /**< Command string. */
	ftcmd_cmd_cb_t cb;    /**< Command callback. */
} ftcmd_cmd_t;


/**
 * @brief Initialize the module and setup the available commands.
 *
 * @details This should be called only once. It should be called after the
 * initialization of the Serial interface.
 *
 * @param[in] command Reference to the command table.
 * @param[in] unknown_cmd_cb  Default callback when an unknown command is
 *                            received.
 *
 * @note The module does not copy the data from the *commands* parameter, it
 * just saves the reference, therefore, it should point to a permanent variable.
 *
 * @note To mark the end of the command table *commands* it is required to
 * insert a command whose string is NULL.
 */
void ftcmd_init(const ftcmd_cmd_t* commands, ftcmd_cmd_cb_t unknown_cmd_cb);

/**
 * @brief Process incoming data and calls the respective command callback if
 * apropiate.
 *
 * @details This should be called periodically in the application main loop.
 */
void ftcmd_fire();

#ifdef __cplusplus
} // extern "C"
#endif

#endif // __FTCMD_H
