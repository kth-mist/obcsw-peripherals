/**
 * @file      cubes_2_msp_crc.h
 * @author    John Wikman
 * @copyright MIT License
 * @brief     Functions for calculating CRC values.
 */

#ifndef CUBES_2_MSP_CRC_H
#define CUBES_2_MSP_CRC_H

unsigned long cubes_2_msp_crc32(const unsigned char *data, unsigned long len, unsigned long start_remainder);

#endif /* CUBES_2_MSP_CRC_H */
