/**
 * @file      cubes_2_handler.cpp
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     CUBES 2 experiment handler functions
 */

extern "C" {
	#include <stdio.h>
	#include <inttypes.h>
	#include <stdlib.h>
	#include <string.h>

	#include "cubes_2_defines.h"
	#include "cubes_2_handler.h"
	#include "cubes_2_msp_exp.h"
}

#include <Arduino.h>
#include <DueFlashStorage.h>

#define ARDUINO_UNIT_ID ARDUINO_UNIT_ID_CUBES_2

#define ASSERT_LEN(l, val)                                  \
    if (l != val) {                                         \
        fprintf(stderr, "[CUBES] Received bad len: %d", l); \
        return;                                             \
    }

static uint8_t daq_active;
static uint8_t daqdur;

// Bin configuration
static uint8_t ch0_hg;
static uint8_t ch0_lg;
static uint8_t ch16_hg;
static uint8_t ch16_lg;
static uint8_t ch31_hg;
static uint8_t ch31_lg;

static uint8_t hvps_active;
static uint16_t hvps_cmds;
static volatile uint8_t powered_off;
static uint32_t timestamp;

static int16_t hvps_dtp1;
static int16_t hvps_dtp2;
static uint16_t hvps_dt1;
static uint16_t hvps_dt2;
static uint16_t hvps_vb;
static uint16_t hvps_tb;
static uint8_t citi_conf[256][CITI_CONF_SIZE];
static uint8_t citi_conf_selected;
static uint8_t prob_conf[PROB_CONF_SIZE];
static uint8_t gateware_conf;
static uint32_t calib_pulse_conf;

static struct {
	struct {
		uint8_t dirty; // Indicates whether any of the config values are valid
	} meta;

	struct {
		cubes_2_msp_seqflags_t seqflags;
	} msp_state;
} _persistent_data;

// Static assert that the _persistent_data struct fits into the expected available flash space
// See https://stackoverflow.com/a/42966734
typedef char p__LINE__[(sizeof(_persistent_data) < IFLASH1_SIZE - CUBES_2_FLASH_START_ADDR) ? 1 : -1];

static DueFlashStorage due_flash_storage;

static uint16_t rand_uint16(uint16_t min, uint16_t max);

void cubes_2_setup()
{
	uint8_t *flash_ptr;

	daqdur = 0;
	daq_active = 0;
	hvps_active = 0;
	hvps_cmds = 0;
	powered_off = 0;
	timestamp = 0;

	hvps_dtp1 = 0;
	hvps_dtp2 = 0;
	hvps_dt1 = 0;
	hvps_dt2 = 0;
	hvps_vb = 0;
	hvps_tb = 0;

	ch0_hg = 0;
	ch0_lg = 0;
	ch16_hg = 0;
	ch16_lg = 0;
	ch31_hg = 0;
	ch31_lg = 0;

	memset(citi_conf, 0, sizeof(citi_conf));
	memset(prob_conf, 0, sizeof(prob_conf));

	citi_conf_selected = 0;

	gateware_conf = 0;
	calib_pulse_conf = 0;

	flash_ptr = due_flash_storage.readAddress(CUBES_2_FLASH_START_ADDR);
	memcpy(&_persistent_data.meta, flash_ptr, sizeof(_persistent_data.meta));

	cubes_2_restore_seqflags();

	_persistent_data.meta.dirty = 0;

	due_flash_storage.write(CUBES_2_FLASH_START_ADDR, (uint8_t *) &_persistent_data, sizeof(_persistent_data));

}

/**
 * @brief Send callback for MSP communication.
 *
 * @param[in] opcode The MSP opcode that should be handled.
 * @param[out] data A buffer that should be filled with data to send. It
 *                  must be at least CUBES_2_MSP_SENDBUF_SIZE bytes large.
 * @param[out] len Where to store the amount of bytes to send.
 */
void cubes_2_handler_send(uint8_t opcode, uint8_t *data, size_t *len)
{
	uint8_t b;
	uint16_t hk;
	int historam_pl_size;

	switch (opcode) {
	case CUBES_2_MSP_OP_REQ_PAYLOAD:
		if (daqdur > 0) {
			// Write "pseudo random" dummy data into the buffer with the
			// daq duration as the seed
			b = daqdur;
			historam_pl_size = (4096 >> ch0_hg) +
			                   (4096 >> ch0_lg) +
			                   (4096 >> ch16_hg) +
			                   (4096 >> ch16_lg) +
			                   (4096 >> ch31_hg) +
			                   (4096 >> ch31_lg);
			for (int i = 0; i < historam_pl_size; i++) {
				data[i] = b;
				b *= 101;
			}
			*len = historam_pl_size;
		} else {
			*len = 0;
		}
		break;
	case CUBES_2_MSP_OP_REQ_HK:
		cubes_2_msp_to_bigendian32(&data[0], timestamp);                 // TIMESTAMP
		cubes_2_msp_to_bigendian32(&data[4], 0);                         // RESET_COUNTER
		cubes_2_msp_to_bigendian32(&data[8], (rand() << 16) | rand());   // TRIG_COUNT_CH0
		cubes_2_msp_to_bigendian32(&data[12], (rand() << 16) | rand());  // TRIG_COUNT_CH16
		cubes_2_msp_to_bigendian32(&data[16], (rand() << 16) | rand());  // TRIG_COUNT_CH31
		cubes_2_msp_to_bigendian32(&data[20], (rand() << 16) | rand());  // TRIG_COUNT_OR32
		hk = rand_uint16(20, 10000);
		data[24] = (hk >> 8) & 0xff;                                   // HVPS_VOLT
		data[25] = hk & 0xff;
		hk = rand_uint16(100, 1000);
		data[26] = (hk >> 8) & 0xff;                                   // HVPS_CURR
		data[27] = hk & 0xff;
		hk = rand_uint16(0xA000, 0xC000);
		data[28] = (hk >> 8) & 0xff;                                   // HVPS_TEMP
		data[29] = hk & 0xff;
		data[30] = 0x5C;                                               // HVPS_STATUS
		data[31] = 0x4B;
		hvps_cmds += 4;
		data[32] = (hvps_cmds >> 8) & 0xff;                            // Num_HVPS_Cmds_Sent
		data[33] = hvps_cmds & 0xff;
		data[34] = (hvps_cmds >> 8) & 0xff;                            // Num_HVPS_Cmds_Acked
		data[35] = hvps_cmds & 0xff;
		data[36] = 0;                                                  // Num_HVPS_Cmds_Failed
		data[37] = 0;
		data[38] = 0;                                                  // Last_HVPS_Cmd_Err
		data[39] = 0;
		hk = rand_uint16(20, 10000);
		data[40] = (hk >> 8) & 0xff;                                   // ADC_VOLT
		data[41] = hk & 0xff;
		hk = rand_uint16(100, 1000);
		data[42] = (hk >> 8) & 0xff;                                   // ADC_CURR
		data[43] = hk & 0xff;
		hk = rand_uint16(0xA000, 0xC000);
		data[44] = (hk >> 8) & 0xff;                                   // ADC_CITI_TEMP
		data[46] = hk & 0xff;
		*len = 46;
		break;
	case CUBES_2_MSP_OP_REQ_CUBES_ID:
		data[0] = (CUBES_2_BOARD_REV >> 8) & 0xff;
		data[1] = CUBES_2_BOARD_REV & 0xff;
		sprintf((char *) &data[2], "|2022-10-29|%s", __DATE__);
		*len = 26;
		break;
	case CUBES_2_MSP_OP_REQ_CUBES_HVPS_TEMP_COMP:
		data[0] = (hvps_dtp1 >> 8) & 0xff;
		data[1] = hvps_dtp1 & 0xff;
		data[2] = (hvps_dtp2 >> 8) & 0xff;
		data[3] = hvps_dtp2 & 0xff;
		data[4] = (hvps_dt1 >> 8) & 0xff;
		data[5] = hvps_dt1 & 0xff;
		data[6] = (hvps_dt2 >> 8) & 0xff;
		data[7] = hvps_dt2 & 0xff;
		data[8] = (hvps_vb >> 8) & 0xff;
		data[9] = hvps_vb & 0xff;
		data[10] = (hvps_tb >> 8) & 0xff;
		data[11] = hvps_tb & 0xff;
		*len = 12;
		break;
	default:
		*len = 0;
		break;
	}
}

/**
 * @brief Receive callback for MSP communication.
 *
 * @param[in] opcode The MSP opcode that should be handled.
 * @param[out] data A buffer containing the bytes that were received.
 * @param[out] len The amount of bytes that was received.
 */
void cubes_2_handler_recv(uint8_t opcode, const uint8_t *data, size_t len)
{
	uint8_t reset;

	switch (opcode) {
	case CUBES_2_MSP_OP_SEND_TIME:
		ASSERT_LEN(len, 4);
		timestamp = cubes_2_msp_from_bigendian32(data);
		break;
	case CUBES_2_MSP_OP_SEND_HVPS_CONF:
		ASSERT_LEN(len, HVPS_CONF_SIZE);
		hvps_active = data[0] & 0x01;
		reset = data[0] & 0x02;

		if (hvps_active && !reset) {
			hvps_dtp1 = (int16_t) ((data[1] << 8) | data[2]);
			hvps_dtp2 = (int16_t) ((data[3] << 8) | data[4]);
			hvps_dt1 = (uint16_t) ((data[5] << 8) | data[6]);
			hvps_dt2 = (uint16_t) ((data[7] << 8) | data[8]);
			hvps_vb = (uint16_t)  ((data[9] << 8) | data[10]);
			hvps_tb = (uint16_t) ((data[11] << 8) | data[12]);
		}
		hvps_cmds++; // Incremented by one for simplicity, would actually be increased by more
		break;
	case CUBES_2_MSP_OP_SEND_CITI_CONF:
		ASSERT_LEN(len, CITI_CONF_SIZE);
		citi_conf_selected = 255;
		memcpy(citi_conf[citi_conf_selected], data, sizeof(citi_conf[citi_conf_selected]));
		break;
	case CUBES_2_MSP_OP_SEND_PROB_CONF:
		ASSERT_LEN(len, PROB_CONF_SIZE);
		memcpy(prob_conf, data, sizeof(prob_conf));
		break;
	case CUBES_2_MSP_OP_SEND_DAQ_CONF:
		ASSERT_LEN(len, DAQ_CONF_SIZE);
		daqdur = data[0];
		ch0_hg = data[1];
		ch0_lg = data[2];
		ch16_hg = data[3];
		ch16_lg = data[4];
		ch31_hg = data[5];
		ch31_lg = data[6];
		break;
	case CUBES_2_MSP_OP_SEND_HVPS_TMP_VOLT:
		ASSERT_LEN(len, HVPS_TMP_VOLT_SIZE);
		hvps_active = data[0] & 0x01;
		reset = data[0] & 0x02;

		if (hvps_active && !reset) {
			hvps_vb = (uint16_t) ((data[1] << 8) | data[2]);
		}
		hvps_cmds++; // Incremented by one for simplicity, would actually be increased by more
		break;
	case CUBES_2_MSP_OP_SEND_CUBES_GATEWARE_CONF:
		ASSERT_LEN(len, GATEWARE_CONF_SIZE);
		gateware_conf = data[0];
		break;
	case CUBES_2_MSP_OP_SEND_CUBES_CALIB_PULSE_CONF:
		ASSERT_LEN(len, CALIB_PULSE_CONF_SIZE);
		calib_pulse_conf = cubes_2_msp_from_bigendian32(&data[0]);
		break;
	case CUBES_2_MSP_OP_SEND_NVM_CITI_CONF:
		ASSERT_LEN(len, CITI_CONF_SIZE);
		memcpy(citi_conf[data[CITI_CONF_SIZE - 1]], data, sizeof(citi_conf[data[CITI_CONF_SIZE - 1]]));
		break;
	case CUBES_2_MSP_OP_SELECT_NVM_CITI_CONF:
		ASSERT_LEN(len, CITI_CONF_SELECT_NVM_SIZE);
		citi_conf_selected = data[0];
		break;
	default:
		break;
	}
}

/**
 * @brief Sys callback for MSP communication.
 *
 * @param[in] opcode The MSP opcode that should be handled.
 */
void cubes_2_handler_sys(uint8_t opcode)
{
	switch (opcode) {
	case CUBES_2_MSP_OP_ACTIVE:
		hvps_active = 1;
		hvps_cmds++;
		break;
	case CUBES_2_MSP_OP_SLEEP:
		hvps_active = 0;
		daq_active = 0;
		hvps_cmds++;
		break;
	case CUBES_2_MSP_OP_POWER_OFF:
		hvps_active = 0;
		daq_active = 0;
		powered_off = 1;
		hvps_cmds++;
		break;
	case CUBES_2_MSP_OP_DAQ_START:
		// Should probably check that the daq duration was set first
		daq_active = 1;
		break;
	case CUBES_2_MSP_OP_DAQ_STOP:
		daq_active = 0;
		break;
	default:
		break;
	}
}

uint8_t cubes_2_hvps_active()
{
	return hvps_active;
}

volatile uint8_t cubes_2_powered_off()
{
	return powered_off;
}

uint32_t cubes_2_timestamp()
{
	return timestamp;
}

int16_t cubes_2_hvps_dtp1()
{
	return hvps_dtp1;
}

int16_t cubes_2_hvps_dtp2()
{
	return hvps_dtp2;
}

uint16_t cubes_2_hvps_dt1()
{
	return hvps_dt1;
}

uint16_t cubes_2_hvps_dt2()
{
	return hvps_dt2;
}

uint16_t cubes_2_hvps_vb()
{
	return hvps_vb;
}

uint16_t cubes_2_hvps_tb()
{
	return hvps_tb;
}

uint8_t cubes_2_citi_conf_selected()
{
	return citi_conf_selected;
}

void cubes_2_citi_conf(uint8_t id, uint8_t *dst)
{
	if (dst == NULL)
		return;
	memcpy(dst, citi_conf[id], sizeof(citi_conf[id]));
}

void cubes_2_prob_conf(uint8_t *dst)
{
	if (dst == NULL)
		return;
	memcpy(dst, prob_conf, sizeof(prob_conf));
}

uint8_t cubes_2_gateware_conf()
{
	return gateware_conf;
}

uint32_t cubes_2_calib_pulse_conf()
{
	return calib_pulse_conf;
}

void cubes_2_restore_seqflags()
{
	uint8_t *flash_ptr;
	uint32_t offset;

	if (_persistent_data.meta.dirty) {
		// Initialize the MSP state with a blank set of flags.
		_persistent_data.msp_state.seqflags = cubes_2_msp_seqflags_init();
	} else {
		offset = (uint32_t) &_persistent_data.msp_state - (uint32_t) &_persistent_data;
		flash_ptr = due_flash_storage.readAddress(CUBES_2_FLASH_START_ADDR + offset);

		memcpy(&_persistent_data.msp_state, flash_ptr, sizeof(_persistent_data.msp_state));
	}
	cubes_2_msp_exp_state_initialize(_persistent_data.msp_state.seqflags);
}

void cubes_2_save_seqflags()
{
	uint32_t offset = (uint32_t) &_persistent_data.msp_state - (uint32_t) &_persistent_data;

	_persistent_data.msp_state.seqflags = cubes_2_msp_exp_state_get_seqflags();

	due_flash_storage.write(CUBES_2_FLASH_START_ADDR + offset,
	                        (uint8_t *) &_persistent_data.msp_state,
	                        sizeof(_persistent_data.msp_state));

	powered_off = 0;
}


static uint16_t rand_uint16(uint16_t min, uint16_t max)
{
	if (min >= max)
		return min;
	return (uint16_t) ((rand() % (max - min)) + min);
}
