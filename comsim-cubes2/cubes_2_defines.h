/**
 * @file      cubes_2_defines.h
 * @author    William Stackenäs
 * @brief     Custom defines used for CUBES 2
 */

#ifndef CUBES_2_DEFINES_H
#define CUBES_2_DEFINES_H

/* Board rev, not accurate but doesn't really matter */
#define CUBES_2_BOARD_REV      4

/* MSP Custom Commands */
#define CUBES_2_MSP_OP_DAQ_START                    0x51
#define CUBES_2_MSP_OP_DAQ_STOP                     0x52

#define CUBES_2_MSP_OP_REQ_CUBES_ID                 0x61
#define CUBES_2_MSP_OP_REQ_CUBES_HVPS_TEMP_COMP     0x62

#define CUBES_2_MSP_OP_SEND_HVPS_CONF               0x71
#define CUBES_2_MSP_OP_SEND_CITI_CONF               0x72
#define CUBES_2_MSP_OP_SEND_PROB_CONF               0x73
#define CUBES_2_MSP_OP_SEND_DAQ_CONF                0x74
#define CUBES_2_MSP_OP_SEND_HVPS_TMP_VOLT           0x75
#define CUBES_2_MSP_OP_SEND_READ_REG_DEBUG          0x76
#define CUBES_2_MSP_OP_SEND_CUBES_GATEWARE_CONF     0x77
#define CUBES_2_MSP_OP_SEND_CUBES_CALIB_PULSE_CONF  0x78
#define CUBES_2_MSP_OP_SEND_NVM_CITI_CONF           0x79
#define CUBES_2_MSP_OP_SELECT_NVM_CITI_CONF         0x7A

/* Data Constraints */
#define HISTORAM_PAYLOAD_SIZE             24832
#define HVPS_CONF_SIZE                    13
#define CITI_CONF_SIZE                    144
#define CITI_CONF_SELECT_NVM_SIZE         1
#define PROB_CONF_SIZE                    32
#define DAQ_CONF_SIZE                     7
#define HVPS_TMP_VOLT_SIZE                3
#define HVPS_TMP_VOLT_SIZE                3
#define GATEWARE_CONF_SIZE                1
#define CALIB_PULSE_CONF_SIZE             4

#define CUBES_2_MSP_RECVBUF_SIZE  (CITI_CONF_SIZE)
#define CUBES_2_MSP_SENDBUF_SIZE  (HISTORAM_PAYLOAD_SIZE)

#endif /* CUBES_2_DEFINES_H */
