/**
 * @file      cubes_2_handler.h
 * @author    William Stackenäs
 * @copyright MIT License
 * @brief     CUBES 2 state utility functions
 */
#ifndef CUBES_2_EXP_HANDLER_H
#define CUBES_2_EXP_HANDLER_H

#include <stdint.h>

#define ARDUINO_UNIT_ID_CUBES_2 (145)

// IFLASH1 starts at 0xC0000 and ends at 0xFFFFF
// This address is relative to the start of IFLASH1
#define CUBES_2_FLASH_START_ADDR   (0x3F000)

void cubes_2_setup();
uint8_t cubes_2_hvps_active();
volatile uint8_t cubes_2_powered_off();
uint32_t cubes_2_timestamp();
int16_t cubes_2_hvps_dtp1();
int16_t cubes_2_hvps_dtp2();
uint16_t cubes_2_hvps_dt1();
uint16_t cubes_2_hvps_dt2();
uint16_t cubes_2_hvps_vb();
uint16_t cubes_2_hvps_tb();
uint8_t cubes_2_citi_conf_selected();
void cubes_2_citi_conf(uint8_t id, uint8_t *dst);
void cubes_2_prob_conf(uint8_t *dst);
uint8_t cubes_2_gateware_conf();
uint32_t cubes_2_calib_pulse_conf();
void cubes_2_restore_seqflags();
void cubes_2_save_seqflags();

void cubes_2_handler_send(uint8_t opcode, uint8_t *data, size_t *len);
void cubes_2_handler_recv(uint8_t opcode, const uint8_t *data, size_t len);
void cubes_2_handler_sys(uint8_t opcode);

#endif /* CUBES_2_EXP_HANDLER_H */
