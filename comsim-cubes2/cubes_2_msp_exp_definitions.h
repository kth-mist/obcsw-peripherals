/**
 * @file      cubes_2_msp_exp_definitions.h
 * @author    John Wikman
 * @copyright MIT License
 * @brief     Contains defines for the experiment side of MSP.
 *
 * @details
 * Contains definitions that the experiment side of MSP uses during
 * communication. Some definitions can come from other files, in which case
 * this header only checks that they are actually defined.
 */

#ifndef CUBES_2_MSP_EXP_DEFINITIONS_H
#define CUBES_2_MSP_EXP_DEFINITIONS_H

/* Import CUBES_2_MSP_EXP_ADDR and CUBES_2_MSP_EXP_MTU from the configuration file */
#include "cubes_2_msp_configuration.h"

#ifndef CUBES_2_MSP_EXP_ADDR
#error CUBES_2_MSP_EXP_ADDR not set
#endif

#ifndef CUBES_2_MSP_EXP_MTU
#error CUBES_2_MSP_EXP_MTU not set
#else
/**
 * @brief The maximum size an MSP frame can have.
 *
 * This definition should be used to determine minimum size of the buffers used
 * to send or receive MSP frames.
 */
#define CUBES_2_MSP_EXP_MAX_FRAME_SIZE (((CUBES_2_MSP_EXP_MTU) + 5) > 9 ? ((CUBES_2_MSP_EXP_MTU) + 5) : 9)
#endif


#endif /* CUBES_2_MSP_EXP_DEFINITIONS_H */
