/**
 * @file      cubes_2_msp_endian.h
 * @author    John Wikman 
 * @copyright MIT License
 * @brief     Functions for endian conversion.
 *
 * @details
 * Declares function prototypes for converting a byte sequence from Big-Endian
 * into system defined integers.
 */

#ifndef CUBES_2_MSP_ENDIAN_H
#define CUBES_2_MSP_ENDIAN_H

void cubes_2_msp_to_bigendian32(unsigned char *dest, unsigned long number);
unsigned long cubes_2_msp_from_bigendian32(const unsigned char *src);

#endif /* CUBES_2_MSP_ENDIAN_H */
