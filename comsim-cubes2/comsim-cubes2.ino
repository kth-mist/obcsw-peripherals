/**
 * @file comsim-cubes.ino
 */

extern "C" {
	#include "cubes_2_msp_i2c_slave.h"
	#include "cubes_2_handler.h"
}

#define I2C_CLOCKSPEED (400000)

/** Arduino Setup */
void setup(void)
{
	Serial.begin(9600);
	cubes_2_msp_i2c_setup(I2C_CLOCKSPEED);

	cubes_2_setup();

	Serial.print("Setup Complete\n");
}

void loop(void)
{
	if (cubes_2_powered_off()) {
		cubes_2_save_seqflags();
	}

	delay(10);
}
