/**
 * @file      cubes_2_msp_exp.h
 * @author    John Wikman
 * @copyright MIT License
 * @brief     A header that includes all the commonly used headers in the
 *            experiment side of MSP.
 */

#ifndef CUBES_2_MSP_EXP_H
#define CUBES_2_MSP_EXP_H

#ifdef __cplusplus
extern "C" {
#endif
#include "cubes_2_msp_configuration.h"
#include "cubes_2_msp_constants.h"
#include "cubes_2_msp_crc.h"
#include "cubes_2_msp_endian.h"
#include "cubes_2_msp_opcodes.h"
#include "cubes_2_msp_seqflags.h"
#include "cubes_2_msp_exp_callback.h"
#include "cubes_2_msp_exp_definitions.h"
#include "cubes_2_msp_exp_error.h"
#include "cubes_2_msp_exp_frame.h"
#include "cubes_2_msp_exp_handler.h"
#include "cubes_2_msp_exp_state.h"
#ifdef __cplusplus
}
#endif

#endif /* CUBES_2_MSP_EXP_H */
