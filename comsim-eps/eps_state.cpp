/**
 * @file eps_state.cpp
 */

extern "C" {
	#include <inttypes.h>
	#include <stdlib.h>
	#include <string.h>

	#include "eps_error.h"
	#include "eps_fmt.h"
}

#include "eps_state.h"

#include <Arduino.h>
#include <DueFlashStorage.h>

#define ARDUINO_UNIT_ID ARDUINO_UNIT_ID_EPS

#define CONFIG_CMD_RESTORE_DEFAULT (1)
#define CONFIG_PPTMODE_AUTO  (1)
#define CONFIG_PPTMODE_FIXED (2)
#define CONFIG_BATTHEATER_MODE_MANUAL (0)
#define CONFIG_BATTHEATER_MODE_AUTO   (1)
#define CONFIG2_CMD_RESTORE_DEFAULT        (1)
#define CONFIG2_CMD_CONFIRM_CURRENT_CONFIG (2)
#define CONFIG3_CMD_RESTORE_DEFAULT        (1)
#define CONFIG3_CMD_CONFIRM_CURRENT_CONFIG (2)
#define CONFIG3_CMD_GET                    (3)
#define CONFIG3_CMD_SET                    (4)

static uint8_t output[8];
static uint16_t output_toggle_delay[8];

static uint16_t photovoltaic[3]; //! should be just above 400 by default
static uint8_t pv_mode; // 0 = HW default | 1 = Max PPT | 2 = Fixed software PPT (photovoltaic array)

static uint8_t heater[2]; // heater[0] = BP4 | heater[1] = Onboard

static uint32_t wdt_i2c;
static uint32_t wdt_i2c_counter;

static struct {
	struct {
		uint8_t dirty; // Indicates whether any of the config values are valid

		uint32_t boot_counter;
	} meta;

	struct {
		uint8_t pptmode;
		uint8_t battheater_mode;
		int8_t battheater_low;
		int8_t battheater_high;
		uint8_t output_normal_value[8];
		uint8_t output_safe_value[8];
		uint16_t output_initial_on_delay[8];
		uint16_t output_initial_off_delay[8];
		uint16_t fixed_ppt_vboost[3];
	} config;

	struct {
		uint16_t batt_maxvoltage;
		uint16_t batt_safevoltage;
		uint16_t batt_criticalvoltage;
		uint16_t batt_normalvoltage;
	} config2;

	struct {
		uint16_t cur_lim[8];
		uint8_t cur_ema_gain;
		uint8_t cspwdt_channel[2];
		uint8_t cspwdt_address[2];
	} config3;
} _persistent_data;

// Static assert that the _persistent_data struct fits into the expected available flash space
// See https://stackoverflow.com/a/42966734
typedef char p__LINE__[(sizeof(_persistent_data) < IFLASH1_SIZE - EPS_FLASH_START_ADDR) ? 1 : -1];

static DueFlashStorage due_flash_storage;

static void load_default_config(void);
static void load_default_config2(void);
static void load_default_config3(void);
static void confirm_current_config(void);
static void confirm_current_config2(void);
static void confirm_current_config3(void);
static void eps_state_config3_get(sim_eps_config3_t *config3);
static void eps_state_config3_set(const sim_eps_config3_t *config3);

static void get_vboost(uint16_t vboost[]);
static void get_vbatt(uint16_t *vbatt);
static void get_curin(uint16_t curin[]);
static void get_cursun(uint16_t *cursun);
static void get_cursys(uint16_t *cursys);
static void get_curout(uint16_t curout[]);
static void get_output(uint8_t output_get[]);
static void get_output_on_delta(uint16_t output_on_delta[]);
static void get_output_off_delta(uint16_t output_off_delta[]);
static void get_latchup(uint16_t latchup[]);
static void get_wdt_i2c_time_left(uint32_t *wdt_i2c_time_left);
static void get_wdt_gnd_time_left(uint32_t *wdt_gnd_time_left);
static void get_wdt_csp_pings_left(uint8_t wdt_csp_pings_left[]);
static void get_counter_wdt_i2c(uint32_t *counter_wdt_i2c);
static void get_counter_wdt_gnd(uint32_t *counter_wdt_gnd);
static void get_counter_wdt_csp(uint32_t counter_wdt_csp[]);
static void get_counter_boot(uint32_t *counter_boot);
static void get_temp(int16_t temp[]);
static void get_bootcause(uint8_t *bootcause);
static void get_battmode(uint8_t *battmode);
static void get_pptmode(uint8_t *pptmode_out);
static void get_channel_status(uint8_t *channel_status);

static uint16_t randBetween(uint16_t min, uint16_t max)
{
	if (min >= max)
		return min;
	return (uint16_t) ((rand() % (max - min)) + min);
}

void eps_state_init(void)
{
	memset(output, 0, 8 * sizeof(output[0]));
	memset(output_toggle_delay, 0, 8 * sizeof(output_toggle_delay[0]));

	output[0] = 1; // Set OBC to "on" by default

	photovoltaic[0] = 405;
	photovoltaic[1] = 404;
	photovoltaic[2] = 406;
	pv_mode = 0; // HW Default

	heater[0] = 0;
	heater[1] = 0;

	wdt_i2c = 0;
	wdt_i2c_counter = 0; // ?? persistent value here perhaps?

	uint8_t *flash_ptr = due_flash_storage.readAddress(EPS_FLASH_START_ADDR);
	memcpy(&_persistent_data.meta, flash_ptr, sizeof(_persistent_data.meta));

	if (_persistent_data.meta.dirty) {
		_persistent_data.meta.boot_counter = 0;
	} else {
		_persistent_data.meta.boot_counter++;
	}

	load_default_config();
	load_default_config2();
	load_default_config3();

	_persistent_data.meta.dirty = 0;

	due_flash_storage.write(EPS_FLASH_START_ADDR, (uint8_t *) &_persistent_data, sizeof(_persistent_data));
}

void eps_state_hk_legacy(hkparam_t *dst)
{
	int16_t tempbuf[6];
	uint32_t tmp_bootcount;

	get_vboost(dst->pv);
	get_cursun(&dst->pc);
	get_vbatt(&dst->bv);
	get_cursys(&dst->sc);

	get_temp(tempbuf);
	dst->temp[0] = tempbuf[0];
	dst->temp[1] = tempbuf[1];
	dst->temp[2] = tempbuf[2];
	dst->temp[3] = tempbuf[3];
	dst->batt_temp[0] = tempbuf[4];
	dst->batt_temp[1] = tempbuf[5];

	get_latchup(dst->latchup);
	get_bootcause(&dst->reset);

	get_counter_boot(&tmp_bootcount);
	dst->bootcount = (uint16_t) tmp_bootcount;

	get_pptmode(&dst->ppt_mode);
	get_channel_status(&dst->channel_status);

	dst->sw_errors = 0; // Unsure about how this value should be set...
}

void eps_state_hk_full(eps_hk_t *dst)
{
	get_vboost(dst->vboost);
	get_vbatt(&dst->vbatt);
	get_curin(dst->curin);
	get_cursun(&dst->cursun);
	get_cursys(&dst->cursys);
	dst->reserved1 = 0;
	get_curout(dst->curout);
	get_output(dst->output);
	get_output_on_delta(dst->output_on_delta);
	get_output_off_delta(dst->output_off_delta);
	get_latchup(dst->latchup);
	get_wdt_i2c_time_left(&dst->wdt_i2c_time_left);
	get_wdt_gnd_time_left(&dst->wdt_gnd_time_left);
	get_wdt_csp_pings_left(dst->wdt_csp_pings_left);
	get_counter_wdt_i2c(&dst->counter_wdt_i2c);
	get_counter_wdt_gnd(&dst->counter_wdt_gnd);
	get_counter_wdt_csp(dst->counter_wdt_csp);
	get_counter_boot(&dst->counter_boot);
	get_temp(dst->temp);
	get_bootcause(&dst->bootcause);
	get_battmode(&dst->battmode);
	get_pptmode(&dst->pptmode);
	dst->reserved2 = 0;
}

void eps_state_hk_vi(eps_hk_vi_t *dst)
{
	get_vboost(dst->vboost);
	get_vbatt(&dst->vbatt);
	get_curin(dst->curin);
	get_cursun(&dst->cursun);
	get_cursys(&dst->cursys);
	dst->reserved1 = 0;
}

void eps_state_hk_out(eps_hk_out_t *dst)
{
	get_curout(dst->curout);
	get_output(dst->output);
	get_output_on_delta(dst->output_on_delta);
	get_output_off_delta(dst->output_off_delta);
	get_latchup(dst->latchup);
}

void eps_state_hk_wdt(eps_hk_wdt_t *dst)
{
	get_wdt_i2c_time_left(&dst->wdt_i2c_time_left);
	get_wdt_gnd_time_left(&dst->wdt_gnd_time_left);
	get_wdt_csp_pings_left(dst->wdt_csp_pings_left);
	get_counter_wdt_i2c(&dst->counter_wdt_i2c);
	get_counter_wdt_gnd(&dst->counter_wdt_gnd);
	get_counter_wdt_csp(dst->counter_wdt_csp);
}

void eps_state_hk_basic(eps_hk_basic_t *dst)
{
	get_counter_boot(&dst->counter_boot);
	get_temp(dst->temp);
	get_bootcause(&dst->bootcause);
	get_battmode(&dst->battmode);
	get_pptmode(&dst->pptmode);
	dst->reserved2 = 0;
}

int eps_state_set_outputs(uint8_t output_bitvector)
{
	int i;
	uint8_t mask = 0x01;

	for (i = 0; i < 6; i++) {
		if (mask & output_bitvector) {
			output[i] = 1;
		} else {
			output[i] = 0;
		}
		output_toggle_delay[i] = 0;

		mask = mask << 1;
	}

	return 0;
}

int eps_state_set_output(uint8_t channel, uint8_t value, int16_t delay)
{
	if (channel >= 8)
		return EPSERR_INVALID_CHANNEL;

	if (value >= 2)
		return EPSERR_INVALID_OUTPUT_VALUE;

	if (delay < 1) {
		output[channel] = value;
		output_toggle_delay[channel] = 0;
	} else if (output[channel] == value) {
		output_toggle_delay[channel] = 0;
	} else {
		output_toggle_delay[channel] = delay;
	}

	return 0;
}

int eps_state_set_pv_volt(uint16_t voltage1, uint16_t voltage2, uint16_t voltage3)
{
	photovoltaic[0] = voltage1;
	photovoltaic[1] = voltage2;
	photovoltaic[2] = voltage3;

	return 0;
}

int eps_state_set_pv_auto(uint8_t mode)
{
	if (mode >= 3)
		return EPSERR_INVALID_PV_MODE;

	pv_mode = mode;

	return 0;
}

int eps_state_set_heater(uint8_t cmd, uint8_t heater_idx, uint8_t mode)
{
	int err = 0;

	switch (cmd) {
	case 0: // turn heater on/off
		if (heater_idx >= 3) {
			err = EPSERR_INVALID_HEATER;
			break;
		}
		if (mode >= 2) {
			err = EPSERR_INVALID_HEATER_MODE;
			break;
		}
		if (heater_idx < 2) {
			heater[heater_idx] = mode;
		} else {
			heater[0] = mode;
			heater[1] = mode;
		}
		break;
	default:
		err = EPSERR_INVALID_HEATER_COMMAND;
		break;
	}

	return err;
}

size_t eps_state_heater_info(uint8_t *dst)
{
	dst[0] = heater[0];
	dst[1] = heater[1];

	return 2;
}

void eps_state_reset_counters(void)
{
	wdt_i2c_counter = 0;
	_persistent_data.meta.boot_counter = 0;

	due_flash_storage.write(EPS_FLASH_START_ADDR, (uint8_t *) &_persistent_data.meta, sizeof(_persistent_data.meta));
}

void eps_state_kick_i2c_wdt(void)
{
	wdt_i2c = 0;
}

int eps_state_config_cmd(uint8_t cmd)
{
	int ret = 0;

	switch (cmd) {
	case CONFIG_CMD_RESTORE_DEFAULT:
		load_default_config();
		break;
	default:
		ret = EPSERR_INVALID_CONFIG_COMMAND;
		break;
	}

	return ret;
}

void eps_state_config_get(sim_eps_config_t *config)
{
	int i;

	config->ppt_mode = _persistent_data.config.pptmode;
	config->battheater_mode = _persistent_data.config.battheater_mode;
	config->battheater_low = _persistent_data.config.battheater_low;
	config->battheater_high = _persistent_data.config.battheater_high;

	for (i = 0; i < 8; ++i) {
		config->output_normal_value[i] = _persistent_data.config.output_normal_value[i];
		config->output_safe_value[i] = _persistent_data.config.output_safe_value[i];
		config->output_initial_on_delay[i] = _persistent_data.config.output_initial_on_delay[i];
		config->output_initial_off_delay[i] = _persistent_data.config.output_initial_off_delay[i];
	}
	config->vboost[0] = _persistent_data.config.fixed_ppt_vboost[0];
	config->vboost[1] = _persistent_data.config.fixed_ppt_vboost[1];
	config->vboost[2] = _persistent_data.config.fixed_ppt_vboost[2];
}

int eps_state_config_set(const sim_eps_config_t *config)
{
	int i;
	int ret = 0;

	if ((config->ppt_mode != CONFIG_PPTMODE_AUTO) &&
		(config->ppt_mode != CONFIG_PPTMODE_FIXED)) {
		ret = EPSERR_INVALID_PPT_MODE;
	}

	if ((config->battheater_mode != CONFIG_BATTHEATER_MODE_MANUAL) &&
		(config->battheater_mode != CONFIG_BATTHEATER_MODE_AUTO)) {
		ret = EPSERR_INVALID_BATTHEATER_MODE;
	}

	for (i = 0; i < 8; ++i) {
		if (config->output_normal_value[i] >= 2)
			ret = EPSERR_INVALID_OUTPUT_VALUE;
		if (config->output_safe_value[i] >= 2)
			ret = EPSERR_INVALID_OUTPUT_VALUE;
	}

	if (ret != 0)
		return ret;

	// Checks passed, set config
	_persistent_data.config.pptmode = config->ppt_mode;
	_persistent_data.config.battheater_mode = config->battheater_mode;
	_persistent_data.config.battheater_low = config->battheater_low;
	_persistent_data.config.battheater_high = config->battheater_high;

	for (i = 0; i < 8; ++i) {
		_persistent_data.config.output_normal_value[i] = config->output_normal_value[i];
		_persistent_data.config.output_safe_value[i] = config->output_safe_value[i];
		_persistent_data.config.output_initial_on_delay[i] = config->output_initial_on_delay[i];
		_persistent_data.config.output_initial_off_delay[i] = config->output_initial_off_delay[i];
	}
	_persistent_data.config.fixed_ppt_vboost[0] = config->vboost[0];
	_persistent_data.config.fixed_ppt_vboost[1] = config->vboost[1];
	_persistent_data.config.fixed_ppt_vboost[2] = config->vboost[2];

	return 0;
}


int eps_state_config2_cmd(uint8_t cmd)
{
	int ret = 0;

	switch (cmd) {
	case CONFIG2_CMD_RESTORE_DEFAULT:
		load_default_config2();
		break;
	case CONFIG2_CMD_CONFIRM_CURRENT_CONFIG:
		confirm_current_config2();
		break;
	default:
		ret = EPSERR_INVALID_CONFIG2_COMMAND;
		break;
	}

	return ret;
}

void eps_state_config2_get(sim_eps_config2_t *config2)
{
	config2->batt_maxvoltage = _persistent_data.config2.batt_maxvoltage;
	config2->batt_safevoltage = _persistent_data.config2.batt_safevoltage;
	config2->batt_criticalvoltage = _persistent_data.config2.batt_criticalvoltage;
	config2->batt_normalvoltage = _persistent_data.config2.batt_normalvoltage;
	memset(config2->reserved1, 0, sizeof(config2->reserved1));
	memset(config2->reserved2, 0, sizeof(config2->reserved2));
}

void eps_state_config2_set(const sim_eps_config2_t *config2)
{
	_persistent_data.config2.batt_maxvoltage = config2->batt_maxvoltage;
	_persistent_data.config2.batt_safevoltage = config2->batt_safevoltage;
	_persistent_data.config2.batt_criticalvoltage = config2->batt_criticalvoltage;
	_persistent_data.config2.batt_normalvoltage = config2->batt_normalvoltage;
}

int eps_state_config3_cmd(sim_eps_config3_t *config3)
{
	int ret = 0;

	switch (config3->cmd) {
	case CONFIG3_CMD_RESTORE_DEFAULT:
		load_default_config3();
		break;
	case CONFIG3_CMD_CONFIRM_CURRENT_CONFIG:
		confirm_current_config3();
		break;
	case CONFIG3_CMD_GET:
		break;
	case CONFIG3_CMD_SET:
		eps_state_config3_set(config3);
		break;
	default:
		ret = EPSERR_INVALID_CONFIG3_COMMAND;
		break;
	}

	// assmuing that the current config3 is always returned
	eps_state_config3_get(config3);

	return ret;
}


/**
 * Sets the config 1 parameters to their default value.
 */
static void load_default_config(void)
{
	uint32_t config_offset;
	uint8_t *flash_ptr;

	if (_persistent_data.meta.dirty) {
		_persistent_data.config.pptmode = CONFIG_PPTMODE_AUTO;
		_persistent_data.config.battheater_mode = CONFIG_BATTHEATER_MODE_AUTO;
		_persistent_data.config.battheater_low = -5;
		_persistent_data.config.battheater_high = 40;
		memset(_persistent_data.config.output_normal_value, 0, sizeof(_persistent_data.config.output_normal_value));
		memset(_persistent_data.config.output_safe_value, 0, sizeof(_persistent_data.config.output_safe_value));
		memset(_persistent_data.config.output_initial_on_delay, 0, sizeof(_persistent_data.config.output_initial_on_delay));
		memset(_persistent_data.config.output_initial_off_delay, 0, sizeof(_persistent_data.config.output_initial_off_delay));

		_persistent_data.config.output_normal_value[0] = 1;
		_persistent_data.config.output_safe_value[0] = 1;
		_persistent_data.config.fixed_ppt_vboost[0] = 405;
		_persistent_data.config.fixed_ppt_vboost[1] = 405;
		_persistent_data.config.fixed_ppt_vboost[2] = 405;
	} else {
		config_offset = (uint32_t) &_persistent_data.config - (uint32_t) &_persistent_data;
		flash_ptr = due_flash_storage.readAddress(EPS_FLASH_START_ADDR + config_offset);

		memcpy(&_persistent_data.config, flash_ptr, sizeof(_persistent_data.config));
	}
}

/**
 * Sets the config 2 parameters to their default value.
 */
static void load_default_config2(void)
{
	uint32_t config_offset;
	uint8_t *flash_ptr;

	if (_persistent_data.meta.dirty) {
		_persistent_data.config2.batt_maxvoltage = 16000;
		_persistent_data.config2.batt_safevoltage = 4000;
		_persistent_data.config2.batt_criticalvoltage = 2000;
		_persistent_data.config2.batt_normalvoltage = 12000;
	} else {
		config_offset = (uint32_t) &_persistent_data.config2 - (uint32_t) &_persistent_data;
		flash_ptr = due_flash_storage.readAddress(EPS_FLASH_START_ADDR + config_offset);

		memcpy(&_persistent_data.config2, flash_ptr, sizeof(_persistent_data.config2));
	}
}

/**
 * Sets the config 3 parameters to their default value.
 */
static void load_default_config3(void)
{
	uint32_t config_offset;
	uint8_t *flash_ptr;

	if (_persistent_data.meta.dirty) {
		_persistent_data.config3.cur_lim[0] = 1000; // No idea if these values make sense
		_persistent_data.config3.cur_lim[1] = 1001;
		_persistent_data.config3.cur_lim[2] = 1002;
		_persistent_data.config3.cur_lim[3] = 1003;
		_persistent_data.config3.cur_lim[4] = 1004;
		_persistent_data.config3.cur_lim[5] = 1005;
		_persistent_data.config3.cur_lim[6] = 1006;
		_persistent_data.config3.cur_lim[7] = 1007;
		_persistent_data.config3.cur_ema_gain = 53;
		_persistent_data.config3.cspwdt_channel[0] = 22;
		_persistent_data.config3.cspwdt_channel[1] = 23;
		_persistent_data.config3.cspwdt_address[0] = 21;
		_persistent_data.config3.cspwdt_address[1] = 24;
	} else {
		config_offset = (uint32_t) &_persistent_data.config3 - (uint32_t) &_persistent_data;
		flash_ptr = due_flash_storage.readAddress(EPS_FLASH_START_ADDR + config_offset);

		memcpy(&_persistent_data.config3, flash_ptr, sizeof(_persistent_data.config3));
	}
}

void eps_state_config_confirm(void)
{
	confirm_current_config();
}

static void confirm_current_config(void)
{
	uint32_t config_offset = (uint32_t) &_persistent_data.config - (uint32_t) &_persistent_data;

	due_flash_storage.write(EPS_FLASH_START_ADDR + config_offset, (uint8_t *) &_persistent_data.config, sizeof(_persistent_data.config));
}

static void confirm_current_config2(void)
{
	uint32_t config_offset = (uint32_t) &_persistent_data.config2 - (uint32_t) &_persistent_data;

	due_flash_storage.write(EPS_FLASH_START_ADDR + config_offset, (uint8_t *) &_persistent_data.config2, sizeof(_persistent_data.config2));
}

static void confirm_current_config3(void)
{
	uint32_t config_offset = (uint32_t) &_persistent_data.config3 - (uint32_t) &_persistent_data;

	due_flash_storage.write(EPS_FLASH_START_ADDR + config_offset, (uint8_t *) &_persistent_data.config3, sizeof(_persistent_data.config3));
}

/**
 * Gets the config 3 parameters
 * Echoes the given command and returns version, length and flags
 * in accordance with page 21 in the p32us manual
 */
static void eps_state_config3_get(sim_eps_config3_t *config3)
{
	config3->version = 1;
	config3->length = 11;
	config3->flags = 0;
	config3->cur_lim[0] = _persistent_data.config3.cur_lim[0];
	config3->cur_lim[1] = _persistent_data.config3.cur_lim[1];
	config3->cur_lim[2] = _persistent_data.config3.cur_lim[2];
	config3->cur_lim[3] = _persistent_data.config3.cur_lim[3];
	config3->cur_lim[4] = _persistent_data.config3.cur_lim[4];
	config3->cur_lim[5] = _persistent_data.config3.cur_lim[5];
	config3->cur_lim[6] = _persistent_data.config3.cur_lim[6];
	config3->cur_lim[7] = _persistent_data.config3.cur_lim[7];
	config3->cur_ema_gain = _persistent_data.config3.cur_ema_gain;
	config3->cspwdt_channel[0] = _persistent_data.config3.cspwdt_channel[0];
	config3->cspwdt_channel[1] = _persistent_data.config3.cspwdt_channel[1];
	config3->cspwdt_address[0] = _persistent_data.config3.cspwdt_address[0];
	config3->cspwdt_address[1] = _persistent_data.config3.cspwdt_address[1];
}

/**
 * Sets the given config 3 parameters
 * Ignores version, length, and flags instead of rejecting unexpected values
 */
static void eps_state_config3_set(const sim_eps_config3_t *config3)
{
	_persistent_data.config3.cur_lim[0] = config3->cur_lim[0];
	_persistent_data.config3.cur_lim[1] = config3->cur_lim[1];
	_persistent_data.config3.cur_lim[2] = config3->cur_lim[2];
	_persistent_data.config3.cur_lim[3] = config3->cur_lim[3];
	_persistent_data.config3.cur_lim[4] = config3->cur_lim[4];
	_persistent_data.config3.cur_lim[5] = config3->cur_lim[5];
	_persistent_data.config3.cur_lim[6] = config3->cur_lim[6];
	_persistent_data.config3.cur_lim[7] = config3->cur_lim[7];
	_persistent_data.config3.cur_ema_gain = config3->cur_ema_gain;
	_persistent_data.config3.cspwdt_channel[0] = config3->cspwdt_channel[0];
	_persistent_data.config3.cspwdt_channel[1] = config3->cspwdt_channel[1];
	_persistent_data.config3.cspwdt_address[0] = config3->cspwdt_address[0];
	_persistent_data.config3.cspwdt_address[1] = config3->cspwdt_address[1];
}


/*****************************************************************/
/***********                                           ***********/
/**********  HOUSEKEEPING PARAMETER ACCESSOR FUNCTIONS  **********/
/***********                                           ***********/
/*****************************************************************/
static void get_vboost(uint16_t vboost[3])
{
	switch (pv_mode) {
	case 0: // HW default
		vboost[0] = 405;
		vboost[1] = 404;
		vboost[2] = 406;
		break;
	case 1: // Max PPT
		vboost[0] = 430;
		vboost[1] = 430;
		vboost[2] = 430;
		break;
	case 2: // Fixed Software PPT
		vboost[0] = photovoltaic[0];
		vboost[1] = photovoltaic[1];
		vboost[2] = photovoltaic[2];
		break;
	default: // invalid mode
		break;
	}
}

static void get_vbatt(uint16_t *vbatt)
{
	*vbatt = randBetween(15700, 15950);
}

static void get_curin(uint16_t curin[3])
{
	curin[0] = 0;
	curin[1] = randBetween(55, 60);
	curin[2] = randBetween(80, 90);
}

static void get_cursun(uint16_t *cursun)
{
	*cursun = 1;
}

static void get_cursys(uint16_t *cursys)
{
	*cursys = randBetween(160, 170);
}

static void get_curout(uint16_t curout[6])
{
	int i;

	curout[0] = randBetween(5, 7);
	curout[1] = randBetween(15, 20);
	curout[2] = randBetween(15, 20);
	curout[3] = randBetween(43, 49);
	curout[4] = randBetween(45, 49);
	curout[5] = 0;

	// Set current to 0 if they are turned off
	for (i = 0; i < 6; ++i) {
		if (output[i] == 0) // 0 = off, 1 = on
			curout[i] = 0;
	}
}

static void get_output(uint8_t output_get[8])
{
	int i;

	for (i = 0; i < 8; ++i)
		output_get[i] = output[i];
}

static void get_output_on_delta(uint16_t output_on_delta[8])
{
	int i;
	for (i = 0; i < 8; ++i) {
		if (output[i] == 0) {
			// 0 = off
			output_on_delta[i] = output_toggle_delay[i];
		} else {
			// 1 = on
			output_on_delta[i] = 0;
		}
	}
}

static void get_output_off_delta(uint16_t output_off_delta[8])
{
	int i;
	for (i = 0; i < 8; ++i) {
		if (output[i] == 0) {
			// 0 = off
			output_off_delta[i] = 0;
		} else {
			// 1 = on
			output_off_delta[i] = output_toggle_delay[i];
		}
	}
}

static void get_latchup(uint16_t latchup[6])
{
	int i;

	latchup[0] = 1;
	for (i = 1; i < 6; ++i)
		latchup[i] = 0;
}

static void get_wdt_i2c_time_left(uint32_t *wdt_i2c_time_left)
{
	*wdt_i2c_time_left = wdt_i2c;
}

static void get_wdt_gnd_time_left(uint32_t *wdt_gnd_time_left)
{
	*wdt_gnd_time_left = 0;
}

static void get_wdt_csp_pings_left(uint8_t wdt_csp_pings_left[2])
{
	wdt_csp_pings_left[0] = 5;
	wdt_csp_pings_left[1] = 5;
}

static void get_counter_wdt_i2c(uint32_t *counter_wdt_i2c)
{
	*counter_wdt_i2c = wdt_i2c_counter;
}

static void get_counter_wdt_gnd(uint32_t *counter_wdt_gnd)
{
	*counter_wdt_gnd = 0;
}

static void get_counter_wdt_csp(uint32_t counter_wdt_csp[2])
{
	counter_wdt_csp[0] = 0;
	counter_wdt_csp[1] = 0;
}

static void get_counter_boot(uint32_t *counter_boot)
{
	*counter_boot = _persistent_data.meta.boot_counter;
}

static void get_temp(int16_t temp[6])
{
	int i;
	for (i = 0; i < 6; ++i)
		temp[i] = randBetween(22, 26);
}

static void get_bootcause(uint8_t *bootcause)
{
	*bootcause = 0;
}

static void get_battmode(uint8_t *battmode)
{
	*battmode = 3; // nominal
}

static void get_pptmode(uint8_t *pptmode_out)
{
	*pptmode_out = _persistent_data.config.pptmode;
}

static void get_channel_status(uint8_t *channel_status)
{
	*channel_status = (output[3] & 1) |
	                  ((output[4] & 1) << 1) |
	                  ((output[5] & 1) << 2) |
	                  ((output[0] & 1) << 3) |
	                  ((output[1] & 1) << 4) |
	                  ((output[2] & 1) << 5) |
	                  ((output[6] & 1) << 6) |
	                  ((output[7] & 1) << 7);
}
