/**
 * @file eps_handler.h
 */

#ifndef EPS_HANDLER_H
#define EPS_HANDLER_H

#include <stdlib.h>

#define ARDUINO_UNIT_ID_EPS  (220)

#ifdef __cplusplus
extern "C" {
#endif

void eps_handler_init(void);
uint8_t eps_handler_soft_reset_triggered(void);
uint8_t eps_handler_hard_reset_triggered(void);
int eps_handler_last_err(void);

void eps_handler_send(const uint8_t **buf, size_t *len);
int eps_handler_receive(uint8_t *data, size_t len);

#ifdef __cplusplus
}
#endif

#endif /* EPS_HANDLER_H */
