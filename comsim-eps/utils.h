/**
 * @file utils.h
 */

#ifndef UTILS_H
#define UTILS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <inttypes.h>

uint32_t decbe32(const uint8_t *in);
void encbe32(uint8_t *out, uint32_t in);
uint16_t decbe16(const uint8_t *in);
void encbe16(uint8_t *out, uint16_t in);

#ifdef __cplusplus
}
#endif

#endif /* UTILS_H */
