/**
 * @file eps_handler.cpp
 */

extern "C" {
	#include <inttypes.h>
	#include <stdlib.h>

	#include "eps_error.h"
	#include "utils.h"
}

#include <Arduino.h>

#include "eps_state.h"
#include "eps_handler.h"

#include "ftcmd.h"

#define ARDUINO_UNIT_ID ARDUINO_UNIT_ID_EPS

#define EPSPORT_PING              (1)
#define EPSPORT_REBOOT            (4)
#define EPSPORT_GET_HK            (8)
#define EPSPORT_SET_OUTPUT        (9)
#define EPSPORT_SET_SINGLE_OUTPUT (10)
#define EPSPORT_SET_PV_VOLT       (11)
#define EPSPORT_SET_PV_AUTO       (12)
#define EPSPORT_SET_HEATER        (13)
#define EPSPORT_RESET_COUNTERS    (15)
#define EPSPORT_RESET_WDT         (16)
#define EPSPORT_CONFIG_CMD        (17)
#define EPSPORT_CONFIG_GET        (18)
#define EPSPORT_CONFIG_SET        (19)
#define EPSPORT_HARD_RESET        (20)
#define EPSPORT_CONFIG2_CMD       (21)
#define EPSPORT_CONFIG2_GET       (22)
#define EPSPORT_CONFIG2_SET       (23)
#define EPSPORT_CONFIG3_CMD       (25)

#define EPSHK_TYPE_FULL  (0)
#define EPSHK_TYPE_VI    (1)
#define EPSHK_TYPE_OUT   (2)
#define EPSHK_TYPE_WDT   (3)
#define EPSHK_TYPE_BASIC (4)

static uint8_t eps_init = 0;

static uint8_t sendbuf[256];
static size_t sendlen;

static int last_err = 0;

static uint8_t trigger_soft_reset = 0;
static uint8_t trigger_hard_reset = 0;

static void cmd_confirm_config_cb(const char **argv, unsigned int argn);
static void unknown_cmd_cb(const char **argv, unsigned int argn);

static const ftcmd_cmd_t cmd_table[] = {
	{ "eps_confirm_conf", cmd_confirm_config_cb },
	{ NULL, NULL },
};


/**
 * @brief Asserts the length and sets sendlen and last_err on mismatch.
 */
static uint8_t assertlen(size_t got, size_t expected)
{
	if (expected != got) {
		sendlen = 0;
		last_err = EPSERR_INVALID_DATA_LEN;
		return 0;
	} else {
		return 1;
	}
}

/**
 * Initializes (or resets) the EPS
 */
void eps_handler_init()
{
	eps_state_init();
	eps_init = 1;
	last_err = 0;
	sendlen = 0;

	ftcmd_init(cmd_table, unknown_cmd_cb);
}

/**
 * Returns whether the EPS was soft reset and clears the
 * soft reset flag
 */
uint8_t eps_handler_soft_reset_triggered(void)
{
	uint8_t triggered = trigger_soft_reset;
	trigger_soft_reset = 0;
	return triggered;
}


/**
 * Returns whether the EPS was hard reset and clears the
 * hard reset flag
 */
uint8_t eps_handler_hard_reset_triggered(void)
{
	uint8_t triggered = trigger_hard_reset;
	trigger_hard_reset = 0;
	return triggered;
}

/**
 * Returns the current (last) error code.
 */
int eps_handler_last_err(void)
{
	return last_err;
}

/**
 * @brief Handles outgoing data
 */
void eps_handler_send(const uint8_t **buf, size_t *len)
{
	if (sendlen == 0) {
		*buf = NULL;
		*len = 0;
	} else {
		*buf = sendbuf;
		*len = sendlen;
	}
}

/**
 * @brief Handles incoming data on the specified port.
 *        data is expected to contain the following
 *
 *        data[0]:       CSP Port number
 *        data[1,len-1]: Payload data
 *        len - 1:       Payload length
 */
int eps_handler_receive(uint8_t *data, size_t len)
{
	if (!eps_init) {
		eps_handler_init();
	}

	if (len == 0) {
		last_err = EPSERR_ZERO_LENGTH_DATA;
		return last_err;
	}

	static union {
		hkparam_t hkparam;
		eps_hk_t eps_hk;
		eps_hk_vi_t eps_hk_vi;
		eps_hk_out_t eps_hk_out;
		eps_hk_wdt_t eps_hk_wdt;
		eps_hk_basic_t eps_hk_basic;
		sim_eps_config_t config;
		sim_eps_config2_t config2;
		sim_eps_config3_t config3;
	} s;

	int ret = 0;
	size_t outlen;
	uint8_t *outbuf = &sendbuf[2];
	uint8_t port = data[0];
	data = &data[1];
	len--;

	switch (port) {
	case EPSPORT_PING:
		// in:  {uint8_t pingbyte}
		// out: {in.pingbyte}
		if (assertlen(len, 1)) {
			outlen = 1;
			outbuf[0] = data[0];
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_REBOOT:
		// in:  {uint8_t magic[4]}
		// out: none
		if (assertlen(len, 4)) {
			// Must match the magic number
			if (decbe32(data) == 0x80078007) {
				sendlen = 0;
				last_err = 0;
				eps_handler_init();
				trigger_soft_reset = 1;
			} else {
				ret = EPSERR_INVALID_MAGIC;
			}
			outlen = 0;
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_GET_HK:
		// in:  0 bytes = legacy hk | 1 bytes = {uint8_t hktype}
		// out: {uint8_t hk[]}
		if (len == 0) {
			// legacy
			eps_state_hk_legacy(&s.hkparam);
			outlen = hkparam_encode(outbuf, &s.hkparam);
		} else if (len == 1) {
			switch (data[0]) {
			case EPSHK_TYPE_FULL:
				eps_state_hk_full(&s.eps_hk);
				outlen = eps_hk_encode(outbuf, &s.eps_hk);
				break;
			case EPSHK_TYPE_VI:
				eps_state_hk_vi(&s.eps_hk_vi);
				outlen = eps_hk_vi_encode(outbuf, &s.eps_hk_vi);
				break;
			case EPSHK_TYPE_OUT:
				eps_state_hk_out(&s.eps_hk_out);
				outlen = eps_hk_out_encode(outbuf, &s.eps_hk_out);
				break;
			case EPSHK_TYPE_WDT:
				eps_state_hk_wdt(&s.eps_hk_wdt);
				outlen = eps_hk_wdt_encode(outbuf, &s.eps_hk_wdt);
				break;
			case EPSHK_TYPE_BASIC:
				eps_state_hk_basic(&s.eps_hk_basic);
				outlen = eps_hk_basic_encode(outbuf, &s.eps_hk_basic);
				break;
			default:
				ret = EPSERR_INVALID_HK_TYPE;
				outlen = 0;
				break;
			}
		} else {
			outlen = 0;
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_SET_OUTPUT:
		// in:  {uint8_t output_bitmask}
		// out: none
		if (assertlen(len, 1)) {
			ret = eps_state_set_outputs(data[0]);
			outlen = 0;
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_SET_SINGLE_OUTPUT:
		// in:  {uint8_t channel, uint8_t value, int16_t delay}
		// out: none
		if (assertlen(len, 4)) {
			ret = eps_state_set_output(data[0], data[1], (int16_t) decbe16(&data[2]));
			outlen = 0;
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_SET_PV_VOLT:
		// in:  {uint16_t voltage1, uint16_t voltage2, uint16_t voltage3}
		// out: none
		if (assertlen(len, 6)) {
			ret = eps_state_set_pv_volt(decbe16(&data[0]), decbe16(&data[2]), decbe16(&data[4]));
			outlen = 0;
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_SET_PV_AUTO:
		// in:  {uint8_t mode}
		// out: none
		if (assertlen(len, 1)) {
			ret = eps_state_set_pv_auto(data[0]);
			outlen = 0;
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_SET_HEATER:
		// in: 0 bytes = return info only | 3 bytes = {uint8_t cmd, uint8_t heater, uint8_t mode}
		// out: none
		if (len == 0) {
			outlen = eps_state_heater_info(outbuf);
		} else if (len == 3) {
			ret = eps_state_set_heater(data[0], data[1], data[2]);
			outlen = eps_state_heater_info(outbuf);
		} else {
			outlen = 0;
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_RESET_COUNTERS:
		// in:  {uint8_t magic}
		// out: none
		if (assertlen(len, 1)) {
			if (data[0] == 0x42) {
				eps_state_reset_counters();
				outlen = 0;
			} else {
				ret = EPSERR_INVALID_MAGIC;
				outlen = 0;
			}
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_RESET_WDT:
		// in:  {uint8_t magic}
		// out: none
		if (assertlen(len, 1)) {
			if (data[0] == 0x78) {
				eps_state_kick_i2c_wdt();
				outlen = 0;
			} else {
				ret = EPSERR_INVALID_MAGIC;
				outlen = 0;
			}
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_CONFIG_CMD:
		// in:  {uint8_t cmd}
		// out: none
		if (assertlen(len, 1)) {
			ret = eps_state_config_cmd(data[0]);
			outlen = 0;
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_CONFIG_GET:
		// in:  none
		// out: {uint8_t config[]}
		if (assertlen(len, 0)) {
			eps_state_config_get(&s.config);
			outlen = eps_config_encode(outbuf, &s.config);
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_CONFIG_SET:
		// in:  {uint8_t config[]}
		// out: none
		if (assertlen(len, sizeof(sim_eps_config_t))) {
			eps_config_decode(&s.config, data);
			ret = eps_state_config_set(&s.config);
			outlen = 0;
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_HARD_RESET:
		// in:  none
		// out: none
		if (assertlen(len, 0)) {
			outlen = 0;
			eps_handler_init();
			trigger_hard_reset = 1;
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_CONFIG2_CMD:
		// in:  {uint8_t cmd}
		// out: none
		if (assertlen(len, 1)) {
			ret = eps_state_config2_cmd(data[0]);
			outlen = 0;
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_CONFIG2_GET:
		// in:  none
		// out: {uint8_t config2[]}
		if (assertlen(len, 0)) {
			eps_state_config2_get(&s.config2);
			outlen = eps_config2_encode(outbuf, &s.config2);
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_CONFIG2_SET:
		// in:  {uint8_t config2[]}
		// out: none
		if (assertlen(len, sizeof(sim_eps_config2_t))) {
			eps_config2_decode(&s.config2, data);
			eps_state_config2_set(&s.config2);
			outlen = 0;
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	case EPSPORT_CONFIG3_CMD:
		// in: {uint8_t config3[]}
		// out: {uint8_t config3[]}
		if (assertlen(len, sizeof(sim_eps_config3_t))) {
			eps_config3_decode(&s.config3, data);
			ret = eps_state_config3_cmd(&s.config3);
			outlen = eps_config3_encode(outbuf, &s.config3);
		} else {
			ret = EPSERR_INVALID_DATA_LEN;
		}
		break;
	default:
		ret = EPSERR_UNKNOWN_PORT;
		break;
	}
	if (ret != 0)
		last_err = ret;

	// Prefix with commandReply
	sendbuf[0] = port;
	sendbuf[1] = last_err;
	sendlen = outlen + sizeof(uint16_t);

	return ret;
}

static void cmd_confirm_config_cb(const char **argv, unsigned int argn)
{
	(void) argn;

	eps_state_config_confirm();

	Serial.print("+");
	Serial.print(argv[0]);
	Serial.println(";0");
}

static void unknown_cmd_cb(const char **argv, unsigned int argn)
{
	(void) argn;

	Serial.print("Invalid cmd: ");
	Serial.println(argv[0]);
}
