// Copyright (c) 2021 Andrew Berezovskyi and MIST contributors
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

/**
 * @file ftcmd_common.cpp
 *
 * @brief Implementation of common commands for the FT serial interface.
 *
 * @version v0.1.0-dev
 */

#include "ftcmd.h"
#include "version.h"
#include "Arduino.h"

/* Global variables ***********************************************************/
extern const ftcmd_cmd_t common_cmd[];

/* Function prototypes ********************************************************/
static void cmd_id_cb(const char **argv, unsigned int argn);
static void cmd_ver_cb(const char **argv, unsigned int argn);

/* Callbacks ******************************************************************/
static void cmd_id_cb(const char **argv, unsigned int argn)
{
	Serial.print("+id;");
	Serial.println(PRJ_NAME);
}

static void cmd_ver_cb(const char **argv, unsigned int argn)
{
	Serial.print("+ver;");
	Serial.println("v" VERSION_STR);
}

/* Function definitions *******************************************************/
/* Public functions ***********************************************************/
/* Public variables ***********************************************************/
const ftcmd_cmd_t common_cmd[] = {
	{ "id", cmd_id_cb },
	{ "ver", cmd_ver_cb },
	{ NULL, NULL },
};
