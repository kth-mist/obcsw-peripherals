/**
 * @file comsim-eps.ino
 */

#include <inttypes.h>
#include <stdlib.h>

#include <Wire.h>

#include "ftcmd.h"

extern "C" {
	#include "eps_handler.h"
}

#if (BUFFER_LENGTH < 245)
#error You must change the BUFFER_LENGTH constant in Wire.h to at least 245 before you can use the simulator.
#endif

#define I2C_BITRATE (400000)
#define I2C_ADDRESS (0x02)


// Software Self Reset Definitions
// See 10.21 (AIRCR) on p.165 in http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-11057-32-bit-Cortex-M3-Microcontroller-SAM3X-SAM3A_Datasheet.pdf
#define SYSRESETREQ    (1 << 2)
#define VECTKEY        (0x05FA0000UL)
#define VECTKEY_MASK   (0x0000FFFFUL)
#define AIRCR          (*((uint32_t*) 0xE000ED0CUL))
static void request_system_reset(void)
{
	// Resets all major components except for debug
	AIRCR = (AIRCR & VECTKEY_MASK) | VECTKEY | SYSRESETREQ;
}

static uint8_t recvbuf[256];
static size_t recvlen;

/**
 * @brief I2C receive callback.
 *
 * Inserts data in a buffer and invokes the appropriate handler function.
 */
static void i2c_recv_cb(int len)
{
	recvlen = 0;
	while (Wire.available()) {
		if (recvlen < sizeof(recvbuf))
			recvbuf[recvlen++] = Wire.read();
		else
			(void) Wire.read();
	}

	(void) eps_handler_receive(recvbuf, recvlen);
}

/**
 * @brief I2C send (request) callback.
 *
 * Reads from a staged send buffer and sends it over I2C.
 */
static void i2c_send_cb(void)
{
	const uint8_t* sendbuf;
	size_t sendlen;
	eps_handler_send(&sendbuf, &sendlen);
	
	Wire.write(sendbuf, sendlen);
}

void setup(void)
{
	pinMode(LED_BUILTIN, OUTPUT);

	Wire.begin(I2C_ADDRESS);
	Wire.setClock(I2C_BITRATE);
	Wire.onReceive(i2c_recv_cb);
	Wire.onRequest(i2c_send_cb);

	eps_handler_init();
}

void loop(void)
{
	static int err_code = 0;
	static int err_timer = 0;
	static int err_cooldown = 0;

	if (eps_handler_soft_reset_triggered()) {
		err_code = 0;
		err_timer = 0;
	} else if (eps_handler_hard_reset_triggered()) {
		request_system_reset();
	}

	// Blink the builtin led the corrsponding number of times to the error code
	// Cooldown 2 seconds in between
	err_code = eps_handler_last_err();

	if (err_timer > 0) {
		if (err_timer & 1)
			digitalWrite(LED_BUILTIN, LOW);
		else
			digitalWrite(LED_BUILTIN, HIGH);

		err_timer--;
	} else if (err_cooldown > 0) {
		err_cooldown--;
	} else {
		err_timer = 2 * err_code;
		err_cooldown = 8 * 2; // 125 * 8 * 2 = 2000 ms
	}

	ftcmd_fire();

	delay(125);
}
