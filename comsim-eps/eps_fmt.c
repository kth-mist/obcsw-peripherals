/**
 * @file eps_fmt.c
 */

#include <inttypes.h>
#include <stdlib.h>

#include "eps_fmt.h"

#include "utils.h"


void hkparam_decode(hkparam_t *out, const uint8_t *src)
{
	out->pv[0] = decbe16(&src[0]);
	out->pv[1] = decbe16(&src[2]);
	out->pv[2] = decbe16(&src[4]);
	out->pc = decbe16(&src[6]);
	out->bv = decbe16(&src[8]);
	out->sc = decbe16(&src[10]);
	out->temp[0] = (int16_t) decbe16(&src[12]);
	out->temp[1] = (int16_t) decbe16(&src[14]);
	out->temp[2] = (int16_t) decbe16(&src[16]);
	out->temp[3] = (int16_t) decbe16(&src[18]);
	out->batt_temp[0] = (int16_t) decbe16(&src[20]);
	out->batt_temp[1] = (int16_t) decbe16(&src[22]);
	out->latchup[0] = decbe16(&src[24]);
	out->latchup[1] = decbe16(&src[26]);
	out->latchup[2] = decbe16(&src[28]);
	out->latchup[3] = decbe16(&src[30]);
	out->latchup[4] = decbe16(&src[32]);
	out->latchup[5] = decbe16(&src[34]);
	out->reset = src[36];
	out->bootcount = decbe16(&src[37]);
	out->sw_errors = decbe16(&src[39]);
	out->ppt_mode = src[41];
	out->channel_status = src[42];
}

size_t hkparam_encode(uint8_t *dst, const hkparam_t *hk)
{
	encbe16(&dst[0], hk->pv[0]);
	encbe16(&dst[2], hk->pv[1]);
	encbe16(&dst[4], hk->pv[2]);
	encbe16(&dst[6], hk->pc);
	encbe16(&dst[8], hk->bv);
	encbe16(&dst[10], hk->sc);
	encbe16(&dst[12], (uint16_t) hk->temp[0]);
	encbe16(&dst[14], (uint16_t) hk->temp[1]);
	encbe16(&dst[16], (uint16_t) hk->temp[2]);
	encbe16(&dst[18], (uint16_t) hk->temp[3]);
	encbe16(&dst[20], (uint16_t) hk->batt_temp[0]);
	encbe16(&dst[22], (uint16_t) hk->batt_temp[1]);
	encbe16(&dst[24], hk->latchup[0]);
	encbe16(&dst[26], hk->latchup[1]);
	encbe16(&dst[28], hk->latchup[2]);
	encbe16(&dst[30], hk->latchup[3]);
	encbe16(&dst[32], hk->latchup[4]);
	encbe16(&dst[34], hk->latchup[5]);
	dst[36] = hk->reset;
	encbe16(&dst[37], hk->bootcount);
	encbe16(&dst[39], hk->sw_errors);
	dst[41] = hk->ppt_mode;
	dst[42] = hk->channel_status;

	return 43;
}

void eps_hk_decode(eps_hk_t *out, const uint8_t *src)
{
	out->vboost[0] = decbe16(&src[0]);
	out->vboost[1] = decbe16(&src[2]);
	out->vboost[2] = decbe16(&src[4]);
	out->vbatt = decbe16(&src[6]);
	out->curin[0] = decbe16(&src[8]);
	out->curin[1] = decbe16(&src[10]);
	out->curin[2] = decbe16(&src[12]);
	out->cursun = decbe16(&src[14]);
	out->cursys = decbe16(&src[16]);
	out->reserved1 = decbe16(&src[18]);
	out->curout[0] = decbe16(&src[20]);
	out->curout[1] = decbe16(&src[22]);
	out->curout[2] = decbe16(&src[24]);
	out->curout[3] = decbe16(&src[26]);
	out->curout[4] = decbe16(&src[28]);
	out->curout[5] = decbe16(&src[30]);
	out->output[0] = src[32];
	out->output[1] = src[33];
	out->output[2] = src[34];
	out->output[3] = src[35];
	out->output[4] = src[36];
	out->output[5] = src[37];
	out->output[6] = src[38];
	out->output[7] = src[39];
	out->output_on_delta[0] = decbe16(&src[40]);
	out->output_on_delta[1] = decbe16(&src[42]);
	out->output_on_delta[2] = decbe16(&src[44]);
	out->output_on_delta[3] = decbe16(&src[46]);
	out->output_on_delta[4] = decbe16(&src[48]);
	out->output_on_delta[5] = decbe16(&src[50]);
	out->output_on_delta[6] = decbe16(&src[52]);
	out->output_on_delta[7] = decbe16(&src[54]);
	out->output_off_delta[0] = decbe16(&src[56]);
	out->output_off_delta[1] = decbe16(&src[58]);
	out->output_off_delta[2] = decbe16(&src[60]);
	out->output_off_delta[3] = decbe16(&src[62]);
	out->output_off_delta[4] = decbe16(&src[64]);
	out->output_off_delta[5] = decbe16(&src[66]);
	out->output_off_delta[6] = decbe16(&src[68]);
	out->output_off_delta[7] = decbe16(&src[70]);
	out->latchup[0] = decbe16(&src[72]);
	out->latchup[1] = decbe16(&src[74]);
	out->latchup[2] = decbe16(&src[76]);
	out->latchup[3] = decbe16(&src[78]);
	out->latchup[4] = decbe16(&src[80]);
	out->latchup[5] = decbe16(&src[82]);
	out->wdt_i2c_time_left = decbe32(&src[84]);
	out->wdt_gnd_time_left = decbe32(&src[88]);
	out->wdt_csp_pings_left[0] = src[92];
	out->wdt_csp_pings_left[1] = src[93];
	out->counter_wdt_i2c = decbe32(&src[94]);
	out->counter_wdt_gnd = decbe32(&src[98]);
	out->counter_wdt_csp[0] = decbe32(&src[102]);
	out->counter_wdt_csp[1] = decbe32(&src[106]);
	out->counter_boot = decbe32(&src[110]);
	out->temp[0] = (int16_t) decbe16(&src[114]);
	out->temp[1] = (int16_t) decbe16(&src[116]);
	out->temp[2] = (int16_t) decbe16(&src[118]);
	out->temp[3] = (int16_t) decbe16(&src[120]);
	out->temp[4] = (int16_t) decbe16(&src[122]);
	out->temp[5] = (int16_t) decbe16(&src[124]);
	out->bootcause = src[126];
	out->battmode = src[127];
	out->pptmode = src[128];
	out->reserved2 = decbe16(&src[129]);
}

size_t eps_hk_encode(uint8_t *dst, const eps_hk_t *hk)
{
	encbe16(&dst[0], hk->vboost[0]);
	encbe16(&dst[2], hk->vboost[1]);
	encbe16(&dst[4], hk->vboost[2]);
	encbe16(&dst[6], hk->vbatt);
	encbe16(&dst[8], hk->curin[0]);
	encbe16(&dst[10], hk->curin[1]);
	encbe16(&dst[12], hk->curin[2]);
	encbe16(&dst[14], hk->cursun);
	encbe16(&dst[16], hk->cursys);
	encbe16(&dst[18], hk->reserved1);
	encbe16(&dst[20], hk->curout[0]);
	encbe16(&dst[22], hk->curout[1]);
	encbe16(&dst[24], hk->curout[2]);
	encbe16(&dst[26], hk->curout[3]);
	encbe16(&dst[28], hk->curout[4]);
	encbe16(&dst[30], hk->curout[5]);
	dst[32] = hk->output[0];
	dst[33] = hk->output[1];
	dst[34] = hk->output[2];
	dst[35] = hk->output[3];
	dst[36] = hk->output[4];
	dst[37] = hk->output[5];
	dst[38] = hk->output[6];
	dst[39] = hk->output[7];
	encbe16(&dst[40], hk->output_on_delta[0]);
	encbe16(&dst[42], hk->output_on_delta[1]);
	encbe16(&dst[44], hk->output_on_delta[2]);
	encbe16(&dst[46], hk->output_on_delta[3]);
	encbe16(&dst[48], hk->output_on_delta[4]);
	encbe16(&dst[50], hk->output_on_delta[5]);
	encbe16(&dst[52], hk->output_on_delta[6]);
	encbe16(&dst[54], hk->output_on_delta[7]);
	encbe16(&dst[56], hk->output_off_delta[0]);
	encbe16(&dst[58], hk->output_off_delta[1]);
	encbe16(&dst[60], hk->output_off_delta[2]);
	encbe16(&dst[62], hk->output_off_delta[3]);
	encbe16(&dst[64], hk->output_off_delta[4]);
	encbe16(&dst[66], hk->output_off_delta[5]);
	encbe16(&dst[68], hk->output_off_delta[6]);
	encbe16(&dst[70], hk->output_off_delta[7]);
	encbe16(&dst[72], hk->latchup[0]);
	encbe16(&dst[74], hk->latchup[1]);
	encbe16(&dst[76], hk->latchup[2]);
	encbe16(&dst[78], hk->latchup[3]);
	encbe16(&dst[80], hk->latchup[4]);
	encbe16(&dst[82], hk->latchup[5]);
	encbe32(&dst[84], hk->wdt_i2c_time_left);
	encbe32(&dst[88], hk->wdt_gnd_time_left);
	dst[92] = hk->wdt_csp_pings_left[0];
	dst[93] = hk->wdt_csp_pings_left[1];
	encbe32(&dst[94], hk->counter_wdt_i2c);
	encbe32(&dst[98], hk->counter_wdt_gnd);
	encbe32(&dst[102], hk->counter_wdt_csp[0]);
	encbe32(&dst[106], hk->counter_wdt_csp[1]);
	encbe32(&dst[110], hk->counter_boot);
	encbe16(&dst[114], (uint16_t) hk->temp[0]);
	encbe16(&dst[116], (uint16_t) hk->temp[1]);
	encbe16(&dst[118], (uint16_t) hk->temp[2]);
	encbe16(&dst[120], (uint16_t) hk->temp[3]);
	encbe16(&dst[122], (uint16_t) hk->temp[4]);
	encbe16(&dst[124], (uint16_t) hk->temp[5]);
	dst[126] = hk->bootcause;
	dst[127] = hk->battmode;
	dst[128] = hk->pptmode;
	encbe16(&dst[129], hk->reserved2);

	return 131;
}

void eps_hk_vi_decode(eps_hk_vi_t *out, const uint8_t *src)
{
	out->vboost[0] = decbe16(&src[0]);
	out->vboost[1] = decbe16(&src[2]);
	out->vboost[2] = decbe16(&src[4]);
	out->vbatt = decbe16(&src[6]);
	out->curin[0] = decbe16(&src[8]);
	out->curin[1] = decbe16(&src[10]);
	out->curin[2] = decbe16(&src[12]);
	out->cursun = decbe16(&src[14]);
	out->cursys = decbe16(&src[16]);
	out->reserved1 = decbe16(&src[18]);
}

size_t eps_hk_vi_encode(uint8_t *dst, const eps_hk_vi_t *hk)
{
	encbe16(&dst[0], hk->vboost[0]);
	encbe16(&dst[2], hk->vboost[1]);
	encbe16(&dst[4], hk->vboost[2]);
	encbe16(&dst[6], hk->vbatt);
	encbe16(&dst[8], hk->curin[0]);
	encbe16(&dst[10], hk->curin[1]);
	encbe16(&dst[12], hk->curin[2]);
	encbe16(&dst[14], hk->cursun);
	encbe16(&dst[16], hk->cursys);
	encbe16(&dst[18], hk->reserved1);

	return 20;
}

void eps_hk_out_decode(eps_hk_out_t *out, const uint8_t *src)
{
	out->curout[0] = decbe16(&src[0]);
	out->curout[1] = decbe16(&src[2]);
	out->curout[2] = decbe16(&src[4]);
	out->curout[3] = decbe16(&src[6]);
	out->curout[4] = decbe16(&src[8]);
	out->curout[5] = decbe16(&src[10]);
	out->output[0] = src[12];
	out->output[1] = src[13];
	out->output[2] = src[14];
	out->output[3] = src[15];
	out->output[4] = src[16];
	out->output[5] = src[17];
	out->output[6] = src[18];
	out->output[7] = src[19];
	out->output_on_delta[0] = decbe16(&src[20]);
	out->output_on_delta[1] = decbe16(&src[22]);
	out->output_on_delta[2] = decbe16(&src[24]);
	out->output_on_delta[3] = decbe16(&src[26]);
	out->output_on_delta[4] = decbe16(&src[28]);
	out->output_on_delta[5] = decbe16(&src[30]);
	out->output_on_delta[6] = decbe16(&src[32]);
	out->output_on_delta[7] = decbe16(&src[34]);
	out->output_off_delta[0] = decbe16(&src[36]);
	out->output_off_delta[1] = decbe16(&src[38]);
	out->output_off_delta[2] = decbe16(&src[40]);
	out->output_off_delta[3] = decbe16(&src[42]);
	out->output_off_delta[4] = decbe16(&src[44]);
	out->output_off_delta[5] = decbe16(&src[46]);
	out->output_off_delta[6] = decbe16(&src[48]);
	out->output_off_delta[7] = decbe16(&src[50]);
	out->latchup[0] = decbe16(&src[52]);
	out->latchup[1] = decbe16(&src[54]);
	out->latchup[2] = decbe16(&src[56]);
	out->latchup[3] = decbe16(&src[58]);
	out->latchup[4] = decbe16(&src[60]);
	out->latchup[5] = decbe16(&src[62]);
}

size_t eps_hk_out_encode(uint8_t *dst, const eps_hk_out_t *hk)
{
	encbe16(&dst[0], hk->curout[0]);
	encbe16(&dst[2], hk->curout[1]);
	encbe16(&dst[4], hk->curout[2]);
	encbe16(&dst[6], hk->curout[3]);
	encbe16(&dst[8], hk->curout[4]);
	encbe16(&dst[10], hk->curout[5]);
	dst[12] = hk->output[0];
	dst[13] = hk->output[1];
	dst[14] = hk->output[2];
	dst[15] = hk->output[3];
	dst[16] = hk->output[4];
	dst[17] = hk->output[5];
	dst[18] = hk->output[6];
	dst[19] = hk->output[7];
	encbe16(&dst[20], hk->output_on_delta[0]);
	encbe16(&dst[22], hk->output_on_delta[1]);
	encbe16(&dst[24], hk->output_on_delta[2]);
	encbe16(&dst[26], hk->output_on_delta[3]);
	encbe16(&dst[28], hk->output_on_delta[4]);
	encbe16(&dst[30], hk->output_on_delta[5]);
	encbe16(&dst[32], hk->output_on_delta[6]);
	encbe16(&dst[34], hk->output_on_delta[7]);
	encbe16(&dst[36], hk->output_off_delta[0]);
	encbe16(&dst[38], hk->output_off_delta[1]);
	encbe16(&dst[40], hk->output_off_delta[2]);
	encbe16(&dst[42], hk->output_off_delta[3]);
	encbe16(&dst[44], hk->output_off_delta[4]);
	encbe16(&dst[46], hk->output_off_delta[5]);
	encbe16(&dst[48], hk->output_off_delta[6]);
	encbe16(&dst[50], hk->output_off_delta[7]);
	encbe16(&dst[52], hk->latchup[0]);
	encbe16(&dst[54], hk->latchup[1]);
	encbe16(&dst[56], hk->latchup[2]);
	encbe16(&dst[58], hk->latchup[3]);
	encbe16(&dst[60], hk->latchup[4]);
	encbe16(&dst[62], hk->latchup[5]);

	return 64;
}

void eps_hk_wdt_decode(eps_hk_wdt_t *out, const uint8_t *src)
{
	out->wdt_i2c_time_left = decbe32(&src[0]);
	out->wdt_gnd_time_left = decbe32(&src[4]);
	out->wdt_csp_pings_left[0] = src[8];
	out->wdt_csp_pings_left[1] = src[9];
	out->counter_wdt_i2c = decbe32(&src[10]);
	out->counter_wdt_gnd = decbe32(&src[14]);
	out->counter_wdt_csp[0] = decbe32(&src[18]);
	out->counter_wdt_csp[1] = decbe32(&src[22]);
}

size_t eps_hk_wdt_encode(uint8_t *dst, const eps_hk_wdt_t *hk)
{
	encbe32(&dst[0], hk->wdt_i2c_time_left);
	encbe32(&dst[4], hk->wdt_gnd_time_left);
	dst[8] = hk->wdt_csp_pings_left[0];
	dst[9] = hk->wdt_csp_pings_left[1];
	encbe32(&dst[10], hk->counter_wdt_i2c);
	encbe32(&dst[14], hk->counter_wdt_gnd);
	encbe32(&dst[18], hk->counter_wdt_csp[0]);
	encbe32(&dst[22], hk->counter_wdt_csp[1]);

	return 26;
}

void eps_hk_basic_decode(eps_hk_basic_t *out, const uint8_t *src)
{
	out->counter_boot = decbe32(&src[0]);
	out->temp[0] = (int16_t) decbe16(&src[4]);
	out->temp[1] = (int16_t) decbe16(&src[6]);
	out->temp[2] = (int16_t) decbe16(&src[8]);
	out->temp[3] = (int16_t) decbe16(&src[10]);
	out->temp[4] = (int16_t) decbe16(&src[12]);
	out->temp[5] = (int16_t) decbe16(&src[14]);
	out->bootcause = src[16];
	out->battmode = src[17];
	out->pptmode = src[18];
	out->reserved2 = decbe16(&src[19]);
}

size_t eps_hk_basic_encode(uint8_t *dst, const eps_hk_basic_t *hk)
{
	encbe32(&dst[0], hk->counter_boot);
	encbe16(&dst[4], (uint16_t) hk->temp[0]);
	encbe16(&dst[6], (uint16_t) hk->temp[1]);
	encbe16(&dst[8], (uint16_t) hk->temp[2]);
	encbe16(&dst[10], (uint16_t) hk->temp[3]);
	encbe16(&dst[12], (uint16_t) hk->temp[4]);
	encbe16(&dst[14], (uint16_t) hk->temp[5]);
	dst[16] = hk->bootcause;
	dst[17] = hk->battmode;
	dst[18] = hk->pptmode;
	encbe16(&dst[19], hk->reserved2);

	return 21;
}


void eps_config_decode(sim_eps_config_t *out, const uint8_t *src)
{
	out->ppt_mode = src[0];
	out->battheater_mode = src[1];
	out->battheater_low = (int8_t) src[2];
	out->battheater_high = (int8_t) src[3];
	out->output_normal_value[0] = src[4];
	out->output_normal_value[1] = src[5];
	out->output_normal_value[2] = src[6];
	out->output_normal_value[3] = src[7];
	out->output_normal_value[4] = src[8];
	out->output_normal_value[5] = src[9];
	out->output_normal_value[6] = src[10];
	out->output_normal_value[7] = src[11];
	out->output_safe_value[0] = src[12];
	out->output_safe_value[1] = src[13];
	out->output_safe_value[2] = src[14];
	out->output_safe_value[3] = src[15];
	out->output_safe_value[4] = src[16];
	out->output_safe_value[5] = src[17];
	out->output_safe_value[6] = src[18];
	out->output_safe_value[7] = src[19];
	out->output_initial_on_delay[0] = decbe16(&src[20]);
	out->output_initial_on_delay[1] = decbe16(&src[22]);
	out->output_initial_on_delay[2] = decbe16(&src[24]);
	out->output_initial_on_delay[3] = decbe16(&src[26]);
	out->output_initial_on_delay[4] = decbe16(&src[28]);
	out->output_initial_on_delay[5] = decbe16(&src[30]);
	out->output_initial_on_delay[6] = decbe16(&src[32]);
	out->output_initial_on_delay[7] = decbe16(&src[34]);
	out->output_initial_off_delay[0] = decbe16(&src[36]);
	out->output_initial_off_delay[1] = decbe16(&src[38]);
	out->output_initial_off_delay[2] = decbe16(&src[40]);
	out->output_initial_off_delay[3] = decbe16(&src[42]);
	out->output_initial_off_delay[4] = decbe16(&src[44]);
	out->output_initial_off_delay[5] = decbe16(&src[46]);
	out->output_initial_off_delay[6] = decbe16(&src[48]);
	out->output_initial_off_delay[7] = decbe16(&src[50]);
	out->vboost[0] = decbe16(&src[52]);
	out->vboost[1] = decbe16(&src[54]);
	out->vboost[2] = decbe16(&src[56]);
}

size_t eps_config_encode(uint8_t *dst, const sim_eps_config_t *config)
{
	dst[0] = config->ppt_mode;
	dst[1] = config->battheater_mode;
	dst[2] = (uint8_t) config->battheater_low;
	dst[3] = (uint8_t) config->battheater_high;
	dst[4] = config->output_normal_value[0];
	dst[5] = config->output_normal_value[1];
	dst[6] = config->output_normal_value[2];
	dst[7] = config->output_normal_value[3];
	dst[8] = config->output_normal_value[4];
	dst[9] = config->output_normal_value[5];
	dst[10] = config->output_normal_value[6];
	dst[11] = config->output_normal_value[7];
	dst[12] = config->output_safe_value[0];
	dst[13] = config->output_safe_value[1];
	dst[14] = config->output_safe_value[2];
	dst[15] = config->output_safe_value[3];
	dst[16] = config->output_safe_value[4];
	dst[17] = config->output_safe_value[5];
	dst[18] = config->output_safe_value[6];
	dst[19] = config->output_safe_value[7];
	encbe16(&dst[20], config->output_initial_on_delay[0]);
	encbe16(&dst[22], config->output_initial_on_delay[1]);
	encbe16(&dst[24], config->output_initial_on_delay[2]);
	encbe16(&dst[26], config->output_initial_on_delay[3]);
	encbe16(&dst[28], config->output_initial_on_delay[4]);
	encbe16(&dst[30], config->output_initial_on_delay[5]);
	encbe16(&dst[32], config->output_initial_on_delay[6]);
	encbe16(&dst[34], config->output_initial_on_delay[7]);
	encbe16(&dst[36], config->output_initial_off_delay[0]);
	encbe16(&dst[38], config->output_initial_off_delay[1]);
	encbe16(&dst[40], config->output_initial_off_delay[2]);
	encbe16(&dst[42], config->output_initial_off_delay[3]);
	encbe16(&dst[44], config->output_initial_off_delay[4]);
	encbe16(&dst[46], config->output_initial_off_delay[5]);
	encbe16(&dst[48], config->output_initial_off_delay[6]);
	encbe16(&dst[50], config->output_initial_off_delay[7]);
	encbe16(&dst[52], config->vboost[0]);
	encbe16(&dst[54], config->vboost[1]);
	encbe16(&dst[56], config->vboost[2]);

	return 58;
}

void eps_config2_decode(sim_eps_config2_t *out, const uint8_t *src)
{
	out->batt_maxvoltage = decbe16(&src[0]);
	out->batt_safevoltage = decbe16(&src[2]);
	out->batt_criticalvoltage = decbe16(&src[4]);
	out->batt_normalvoltage = decbe16(&src[6]);
	out->reserved1[0] = decbe32(&src[8]);
	out->reserved1[1] = decbe32(&src[12]);
	out->reserved2[0] = src[16];
	out->reserved2[1] = src[17];
	out->reserved2[2] = src[18];
	out->reserved2[3] = src[19];
}

size_t eps_config2_encode(uint8_t *dst, const sim_eps_config2_t *config2)
{
	encbe16(&dst[0], config2->batt_maxvoltage);
	encbe16(&dst[2], config2->batt_safevoltage);
	encbe16(&dst[4], config2->batt_criticalvoltage);
	encbe16(&dst[6], config2->batt_normalvoltage);
	encbe32(&dst[8], config2->reserved1[0]);
	encbe32(&dst[12], config2->reserved1[1]);
	dst[16] = config2->reserved2[0];
	dst[17] = config2->reserved2[1];
	dst[18] = config2->reserved2[2];
	dst[19] = config2->reserved2[3];

	return 20;
}

void eps_config3_decode(sim_eps_config3_t *out, const uint8_t *src)
{
	out->version = src[0];
	out->cmd = src[1];
	out->length = src[2];
	out->flags = src[3];
	out->cur_lim[0] = decbe16(&src[4]);
	out->cur_lim[1] = decbe16(&src[6]);
	out->cur_lim[2] = decbe16(&src[8]);
	out->cur_lim[3] = decbe16(&src[10]);
	out->cur_lim[4] = decbe16(&src[12]);
	out->cur_lim[5] = decbe16(&src[14]);
	out->cur_lim[6] = decbe16(&src[16]);
	out->cur_lim[7] = decbe16(&src[18]);
	out->cur_ema_gain = src[20];
	out->cspwdt_channel[0] = src[21];
	out->cspwdt_channel[1] = src[22];
	out->cspwdt_address[0] = src[23];
	out->cspwdt_address[1] = src[24];
}

size_t eps_config3_encode(uint8_t *dst, const sim_eps_config3_t *config3)
{
	dst[0] = config3->version;
	dst[1] = config3->cmd;
	dst[2] = config3->length;
	dst[3] = config3->flags;
	encbe16(&dst[4], config3->cur_lim[0]);
	encbe16(&dst[6], config3->cur_lim[1]);
	encbe16(&dst[8], config3->cur_lim[2]);
	encbe16(&dst[10], config3->cur_lim[3]);
	encbe16(&dst[12], config3->cur_lim[4]);
	encbe16(&dst[14], config3->cur_lim[5]);
	encbe16(&dst[16], config3->cur_lim[6]);
	encbe16(&dst[18], config3->cur_lim[7]);
	dst[20] = config3->cur_ema_gain;
	dst[21] = config3->cspwdt_channel[0];
	dst[22] = config3->cspwdt_channel[1];
	dst[23] = config3->cspwdt_address[0];
	dst[24] = config3->cspwdt_address[1];

	return 25;
}
