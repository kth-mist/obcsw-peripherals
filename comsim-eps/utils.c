/**
 * @file utils.c
 */

#include <inttypes.h>

#include "utils.h"

uint32_t decbe32(const uint8_t *in)
{
	uint32_t out;

	out = (in[0] << 24) |
	      (in[1] << 16) |
	      (in[2] << 8) |
	      in[3];

	return out;
}

void encbe32(uint8_t *out, uint32_t in)
{
	out[0] = (uint8_t) ((in >> 24) & 0xff);
	out[1] = (uint8_t) ((in >> 16) & 0xff);
	out[2] = (uint8_t) ((in >> 8) & 0xff);
	out[3] = (uint8_t) (in & 0xff);
}

uint16_t decbe16(const uint8_t *in)
{
	uint16_t out;

	out = (in[0] << 8) |
	      in[1];

	return out;
}

void encbe16(uint8_t *out, uint16_t in)
{
	out[0] = (uint8_t) ((in >> 8) & 0xff);
	out[1] = (uint8_t) (in & 0xff);
}
