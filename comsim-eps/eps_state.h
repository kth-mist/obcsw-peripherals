/**
 * @file eps_state.h
 */

#ifndef EPS_STATE_H
#define EPS_STATE_H

#include <inttypes.h>
#include <stdlib.h>

#include "eps_fmt.h"

#define ARDUINO_UNIT_ID_EPS  (220)

// IFLASH1 starts at 0xC0000 and ends at 0xFFFFF
// This address is relative to the start of IFLASH1
#define EPS_FLASH_START_ADDR   (0x3F000)

#ifdef __cplusplus
extern "C" {
#endif

void eps_state_init(void);

void eps_state_hk_legacy(hkparam_t *dst);
void eps_state_hk_full(eps_hk_t *dst);
void eps_state_hk_vi(eps_hk_vi_t *dst);
void eps_state_hk_out(eps_hk_out_t *dst);
void eps_state_hk_wdt(eps_hk_wdt_t *dst);
void eps_state_hk_basic(eps_hk_basic_t *dst);

int eps_state_set_outputs(uint8_t output_bitvector);
int eps_state_set_output(uint8_t channel, uint8_t value, int16_t delay);

int eps_state_set_pv_volt(uint16_t voltage1, uint16_t voltage2, uint16_t voltage3);
int eps_state_set_pv_auto(uint8_t mode);

int eps_state_set_heater(uint8_t cmd, uint8_t heater_idx, uint8_t mode);
size_t eps_state_heater_info(uint8_t *dst);

void eps_state_reset_counters(void);
void eps_state_kick_i2c_wdt(void);

void eps_state_config_confirm(void);
int eps_state_config_cmd(uint8_t cmd);
void eps_state_config_get(sim_eps_config_t *config);
int eps_state_config_set(const sim_eps_config_t *config);

int eps_state_config2_cmd(uint8_t cmd);
void eps_state_config2_get(sim_eps_config2_t *config2);
void eps_state_config2_set(const sim_eps_config2_t *config2);

int eps_state_config3_cmd(sim_eps_config3_t *config3);

#ifdef __cplusplus
}
#endif

#endif /* EPS_STATE_H */
