/**
 * @file reg_functions.c
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "reg_functions.h"

static uint8_t active_reg;
static uint8_t reg_arr[0x44];     //Registers 0 to 0x41 are used,
                                  //but also 0x7A (and 0x7B), so to save
                                  //space these are mapped to 0x42 (+1)

static int bad_regs[0x44] = {
	[0x06]1, [0x08]1, [0x09]1, [0x0A]1, [0x0C]1, [0x0E]1,
	[0x12]1, [0x14]1, [0x15]1, [0x16]1, [0x18]1, [0x1A]1,
	[0x1E]1, [0x20]1, [0x21]1, [0x22]1, [0x24]1, [0x26]1,
	[0x2A]1, [0x2C]1, [0x2D]1, [0x2E]1, [0x30]1, [0x32]1,
	[0x35]1, [0x37]1, [0x38]1, [0x39]1, [0x3A]1, [0x3B]1,
	[0x3C]1, [0x3D]1, [0x43]1
};

static int double_regs[0x44] = {
	[0x05]1, [0x07]1, [0x0B]1, [0x0D]1,
	[0x11]1, [0x13]1, [0x17]1, [0x19]1,
	[0x1D]1, [0x1F]1, [0x23]1, [0x25]1,
	[0x29]1, [0x2B]1, [0x2F]1, [0x31]1,
	[0x34]1, [0x42]1
};


/*
 * @brief Setting up firmware version and other misc. static values
 */
void init_regs(void)
{
	active_reg = 0x01;
	memset(reg_arr, 0x0, sizeof(reg_arr));
	reg_arr[ARM_TEMP] = 40;
	reg_arr[FIRMWARE_VERSION] = VERSION;
	return;
}


/*
 * @brief Returns size of register array
 */
int size_of_regs(void) {
  return sizeof(reg_arr);
}


/**
 * @brief set_active_reg changes the active register
 *
 * @param[in] reg_adr new address to set
 */
int set_active_reg(uint8_t reg_adr)
{
        if (reg_adr == 0x7A)
		reg_adr = 0x42;

        if (invalid_register(reg_adr))
                return 1;

        active_reg = reg_adr;
        return 0;
}


/**
 * @brief wr_reg writes received data to register
 *
 * @param[in] r reg address to write to
 * @param[in] data byte of data to write
 */
void wr_reg(uint8_t r, uint8_t data)
{
	reg_arr[r] = data;
	return;
}


/**
 * @brief Checks if a register is valid w/r-able
 *
 * @param[in] r register to check
 *
 * @param[out] int yes or no
 */
int invalid_register(uint8_t r)
{
	if ((bad_regs[r]) || (r >= sizeof(reg_arr)))
                return 1;

	return 0;
}


/**
 * @brief Checks if a register is 2 bytes
 *
 * @param[in] r register to check
 *
 * @param[out] int yes or no
 */
int reg_is_double(uint8_t r)
{
	return double_regs[r];
}


/**
 * @brief Simple check if active_reg is 0
 */
int read_all_regs(void)
{
	return (active_reg == 0x00);
}


/**
 * @brief Returns the value of the current active reg
 *
 * @param[in] r register to read
 *
 * @param[out] uint8_t value of the register
 */
uint8_t rd_reg(uint8_t r)
{
        return reg_arr[r];
}

/*
 * @brief Returns the value of the current active reg
 */
uint8_t get_active_reg(void)
{
        return active_reg;
}
