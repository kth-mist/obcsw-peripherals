/**
 * @file nanoprop_handler.c
 */

#include <inttypes.h>
#include <stdlib.h>

#include "nanoprop_handler.h"
#include "reg_functions.h"
#include "time_functions.h"

static uint8_t init = 0;


void nanoprop_setup(void)
{
	init_regs();
}

int nanoprop_handler_send(uint8_t *buf, size_t len)
{
	if (!init) {
		init_regs();
		init = 1;
	}
	uint8_t r;

	if (read_all_regs()) {
		uint8_t i = 0;
		for (size_t c = 1; c < 0x44 && i < len; c++) {
			if (!set_active_reg(c)) {
				buf[i++] = rd_reg(c);
				if (reg_is_double(c) && i < len)
					buf[i++] = rd_reg(c + 1);
			}
		}
		set_active_reg(0x00);

		return i;
	}
	else if (len > 0) {
		r = get_active_reg();
		buf[0] = rd_reg(r);
		if (reg_is_double(r) && len > 1) {
			buf[1] = rd_reg(r + 1);
			return 2;
		} else {
			return 1;
		}
	}

	return 0;
}

int nanoprop_handler_receive(uint8_t *data, size_t len)
{
	if (!init) {
		init_regs();
		init = 1;
	}
	uint8_t a_reg;
	if (len > 0) {
		a_reg = data[0];
		if (set_active_reg(a_reg))
			return 1;

		if (len > 1) {
			if (reg_is_double(a_reg)) {
				if (len < 3)
					return 1;

				wr_reg(a_reg + 1, data[2]);
			}
			wr_reg(a_reg, data[1]);
		}
	}
	return 0;
}
