/**
 * @file reg_functions.h
 */

#ifndef REG_FUNCTIONS_H
#define REG_FUNCTIONS_H

#define FIRMWARE_VERSION        (0x43)
#define ARM_TEMP                (0x41)
#define VERSION                 (151) 	//Simulating version 1.5.1

void init_regs(void);
int size_of_regs(void);
void wr_reg(uint8_t r, uint8_t data);
int set_active_reg(uint8_t reg_adr);
int invalid_register(uint8_t r);
int reg_is_double(uint8_t r);
uint8_t rd_reg(uint8_t r);
int read_all_regs(void);
uint8_t get_active_reg(void);

#endif /* REG_FUNCTIONS_H */
