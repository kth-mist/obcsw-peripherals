/**
 * @file time_functions.c
 */

#include <stdlib.h>
#include <stdint.h>
#include "reg_functions.h"
#include "time_functions.h"


/*
 * @brief Calls all recurring functions
 */
void tick_function(void)
{
        tank_temp_updater();
        thr_status_updater();

}


/*
 * @brief Increases and decreases tank temperature
 */
void tank_temp_updater(void)
{
        static int counter = 0;
        uint8_t tempC = rd_reg(TANK_TEMP);
        int heater_on = (int) rd_reg(TANK_HEATER);
        if(counter == 180) {
                if(heater_on) {
                        tempC++;
                        if(tempC == 40) {
                                wr_reg(TANK_HEATER, 0);
                                wr_reg(MANF_HEATER, 0);
                        }
                } else if (tempC > 0) {
                        tempC--;
                }
                counter = 0;
                wr_reg(TANK_TEMP, tempC);
        } else {
                counter++;
        }
}


/*
 * @brief Increases and decreases thruster temperatures, and updates status reg
 */
void thr_status_updater(void)
{
        uint8_t thr_status, thr_temp;
        uint8_t ready_temp = rd_reg(ARM_TEMP);

        for (int i=0; i<=3; i++) {
                thr_status = rd_reg(THRA_CTRL + 12*(i));
                thr_temp = rd_reg(THRA_TEMP + 12*(i));
                if (thr_status & 0x80) {
                        if (thr_status & 0x70) {
                                if (thr_temp < ready_temp) {
                                        thr_temp++;
                                } else {
                                        thr_status |= 0x02;
                                }
                                if ((thr_status & 0x02) && thr_firing(i)) {
                                        thr_status |= 0x01;
                                }
                        } else if (thr_temp > 1){
                                thr_temp--;
                        }

                }
                wr_reg(THRA_CTRL + 12*(i), thr_status);
                wr_reg(THRA_TEMP + 12*(i), thr_temp);
        }
}


/*
 * @brief Maps THRFIRE to thruster index
 * @param[in] int i thruster index
 * @param[out] int 1 if thruster i is firing, otherwise 0
 */
int thr_firing(int i)
{
        uint8_t tf = rd_reg(THRFIRE);
        switch (i) {
                case 0:
                        return(tf & 0x80);
                case 1:
                        return(tf & 0x20);
                case 2:
                        return(tf & 0x08);
                case 3:
                        return(tf & 0x02);
                default:
                        return 0;
        }
}
