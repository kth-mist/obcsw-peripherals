/**
 * @file nanoprop_handlers.h
 */

#ifndef NANOPROP_HANDLERS_H
#define NANOPROP_HANDLERS_H

void nanoprop_setup(void);
int nanoprop_handler_send(uint8_t *buf, size_t len);
int nanoprop_handler_receive(uint8_t *data, size_t len);

#endif /* NANOPROP_HANDLERS_H */
