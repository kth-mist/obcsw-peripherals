/**
 * @file time_functions.h
 */

#ifndef TIME_FUNCTIONS_H
#define TIME_FUNCTIONS_H

#define THRFIRE         (0x02)
#define THRA_CTRL       (0x03)
#define THRA_TEMP       (0x04)
#define TANK_TEMP       (0x33)
#define TANK_HEATER     (0x36)
#define MANF_HEATER     (0x3E)
#define ARM_TEMP        (0x41)

void tick_function(void);
void tank_temp_updater(void);
void thr_status_updater(void);
int thr_firing(int i);

#endif /* TIME_FUNCTIONS_H */
