# NanoProp Simulator

This folder contains a limited NanoProp simulator to be run on an arduino .
It is still in development, and fuller NanoProp simulation will be implemented.
Current version (0.7) will write data (1 or 2 byte(s)) to any register that is
not the second of a two-byte register. It will read data (1 or 2 byte(s)) from
any register that is not the second of a two-byte register. It will read all
registers when 0x00 is read. Once per second, a number of time dependent
functions are called. As of now, these will update the temperatures of tanks and
thrusters depending on if heaters are on, and update registers as needed.

## Setup
This setup process assumes the following:
 - You are able to compile and flash Arduino Due software.
 - You have access to an Arduino Due.
 - You have access to an OBC or similar.
 - You have the necessary cables and wires to connect the devices.

### 1. Compiling the simulator code
Before you can compile the simulator, you must first increase the default
buffer size for I2C communications to at least 245 bytes. This is done
by opening up the **Wire.h** file and changing the `BUFFER_LENGTH` constant
**from 32 to 245**. The NanoProp simulator will not actually need more than 70
bytes, but the value of 245 is inherited from the trxvu, so it is used rather
than changing it back and forth.

On Arch Linux you can find the Wire.h file under
`$HOME/.arduino15/packages/arduino/hardware/sam/<version>/libraries/Wire/src`.
It should probably look quite similar on other systems. For example, on
Windows it can be found under `C:\Users\<user>\AppData\Local` followed by the
relative path from `$HOME` on Arch Linux.

Now compile and flash the simulator onto the Arduino Due.

### 2. Connecting the simulator hardware
Connect the **SDA (20)** and **SDA1** pins on the Arduino to the SDA line
coming from the iOBC (ISIS on-board computer). Connect the **SCL (21)** and
**SCL1** pins on the Arduino to the SCL line coming from the iOBC. Also make
sure that you connect the Arduino to ground. **The Arduino can experience
frequent I2C bit-errors if not connected to ground!**

Connect the **Native USB** port on the Arduino to any of the USB ports on your
laptop. As you may notice, there are two USB ports on the Arduino. The
Native USB port is the one closest to the reset button (the "left" port).
This is for debugging only.
Check the **Device Manager** in Windows and see which COM port you connected
the Arduino to. Under the COM submenu you should find something along the lines
of **Arduino Due (COM#)** where **#** is a placeholder for its number. Let
**COM#** be the COM port that was connected to your Arduino Due.

### 3. Run the OBCSW
Any communication that is normally done to the actual NanoProp module should
now be handled by the arduino.

Initial register values can be set in the init functions.
They can be changed while running by connecting over serial interface and sending the register address and new value e.g. "0x02 0xC0" to set register 2 to value 0xC0. Base 10 and 16 (by prepending the values with 0x) are accepted for both alues. If only a register address is sent the current value is read and replied in bases 10 and 2.

## Limitations and constraints
Currently, all registers are represented as unsigned ints. Several registers
store values such as temperature, pressure, and impulse, and while most are
unsigned on the actual NanoProp, the temperatures are not. In practice, the
simulator temperature will never drop below 0, which is the threshold operating
temperature for the tank. The thermal simulations give an estimated minimum temp
of -8, so modifications might be necessary to test those cases.
The thruster impulse counter is not implemented, and as such the simulator will
not turn of the thruster automatically in any mode.
