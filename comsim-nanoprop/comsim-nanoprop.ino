/**
 * @file nanoprop_due.ino
 */

#include <inttypes.h>
#include <stdlib.h>

#include <Wire.h>
#include "debug.hpp"

extern "C" {
	#include "reg_functions.h"
	#include "time_functions.h"
	#include "nanoprop_handler.h"
}

#define I2C_BITRATE (400000)
#define I2C_ADDRESS (0x12)


void receiveEvent(int msg_size);
void requestEvent(void);

char inputString[16];

/**
 * Arduino setup
 */
void setup(void)
{
	Wire.begin(I2C_ADDRESS);
	Wire.setClock(I2C_BITRATE);
	Wire.onReceive(receiveEvent);
	Wire.onRequest(requestEvent);

 Serial.begin(9600);
 while(!Serial) {
  ;
 }
 Serial.println("Nanoprop sim started.");

	init_regs();

	DEBUG_INIT();
	DEBUG_PRINTLN("NANOPROP SIM Started. Firmware version:");
	DEBUG_PRINTINT(rd_reg(0x43));
	DEBUG_FLUSH();
}


/**
 * Arduino loop
 */
void loop(void)
{
	delay(1000);
	tick_function();
}


/**
 * @brief Handler of i2c receive
 *
 * @param[in] msg_size
 */
void receiveEvent(int msg_size)
{
	int err;
	uint8_t tmpdata[3] = {0};
	size_t len;

	for (len = 0; Wire.available() && len < sizeof(tmpdata); len++) {
		tmpdata[len] = Wire.read();
	}

	if (Wire.available()) {
		DEBUG_PRINTLN("TOO MANY BYTES RECEIVED. IGNORING EXCESS. REG:");
		DEBUG_PRINTBYTE(tmpdata[0]);
		DEBUG_PRINTLN("MSG_SIZE:");
		DEBUG_PRINTINT(msg_size);
		while(Wire.available()) {
			(void) Wire.read();
		}
	}

	err = nanoprop_handler_receive(tmpdata, len);
	if (err != 0) {
		DEBUG_PRINTLN("ERROR SETTING REGISTER. FLUSHING BUFFER. REG:");
		DEBUG_PRINTBYTE(get_active_reg());
	}
}

/**
 * @brief Handler of i2c request
 *
 */
void requestEvent(void)
{
	uint8_t send_buf[0x44];
	size_t len = nanoprop_handler_send(send_buf, sizeof(send_buf));
	for (size_t i = 0; i < len; i++) {
		Wire.write(send_buf[i]);
	}
}

void serialEvent() {
  memset(inputString, 0x0, sizeof(inputString));
  int ireg, ival;
  Serial.readBytesUntil('\n', inputString, 16);
  char* separator = strchr(inputString, ' ');
  ireg = strtol(inputString, NULL, 0);
  if ((ireg == 0x7A) || (ireg == 0x7B)) {
    ireg -= 0x38;
  }
  if ((ireg < 0) || (ireg > size_of_regs())) {
    Serial.println("Register out of bounds.");
    return;
  }
  if (separator != NULL) {
    ival = strtol(separator, NULL, 0);
    wr_reg(ireg, ival);
  }
  Serial.print("REG: ");
  Serial.println(ireg, HEX);
  Serial.print("VAL: ");
  Serial.println(rd_reg(ireg), DEC);
  Serial.print("BIN: ");
  Serial.println(rd_reg(ireg), BIN);
}
