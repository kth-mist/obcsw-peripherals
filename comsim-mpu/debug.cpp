/**
 * @file   debug.cpp
 * @author John Wikman
 */

#include <Arduino.h>

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "debug.hpp"

#define ARDUINO_UNIT_ID ARDUINO_UNIT_ID_MPU

/** @brief Convert a 4-bit nibble to a lower case hex character. */
#define NIBBLE_TO_LCHEX(n) (((n) < 0xa) ? (((char) (n)) + '0') : (((char) ((n) - 0xa)) + 'a'))

static bool _debug_is_initiialized = false;

static size_t _debugbufidx = 0;
static char _debugbuf[DEBUGDEF_BUFSIZE + 1];

/**
 * @brief Initializes the debug functionality.
 */
void _debug_init(void)
{
	if (_debug_is_initiialized)
		return;

	Serial.begin(DEBUGDEF_SERIAL_CLOCK);
	_debugbufidx = 0;

	_debug_is_initiialized = 1;
}

/**
 * @brief De-initialized the debug functionality.
 */
void _debug_deinit(void)
{
	if (!_debug_is_initiialized)
		return;

	Serial.end();

	_debug_is_initiialized = 0;
}

/**
 * @brief Prints a string through the debug functionality.
 */
void _debug_print(const char *str)
{
	if (str == NULL)
		return;

	_debug_nprint(str, strlen(str));
}

/**
 * @brief Prints a string and a newline character through the debug
 *        functionality.
 */
void _debug_println(const char *str)
{
	if (str == NULL)
		return;

	_debug_nprint(str, strlen(str));
	_debug_nprint("\n\r", 2);
}

/**
 * @brief Prints a string with a known length through the debug functionality.
 */
void _debug_nprint(const char *str, size_t n)
{
	if (!_debug_is_initiialized)
		return;
	if (_debugbufidx >= DEBUGDEF_BUFSIZE)
		return;

	if (str == NULL)
		return;
	if ((_debugbufidx + n) > DEBUGDEF_BUFSIZE)
		n = DEBUGDEF_BUFSIZE - _debugbufidx;

	strncpy(&_debugbuf[_debugbufidx], str, n);
	_debugbufidx += n;
}

/**
 * @brief Prints an int through the debug functionality.
 */
void _debug_printint(int v)
{
	static char buf[16];
	int len;

	len = snprintf(buf, sizeof(buf), "%d", v);
	if (len < 1)
		return;

	_debug_nprint(buf, (size_t) len);
}

/**
 * @brief Prints a byte in hex format through the debug functionality.
 */
void _debug_printbyte(uint8_t b)
{
	char buf[2];

	buf[0] = NIBBLE_TO_LCHEX((b >> 4) & 0xf);
	buf[1] = NIBBLE_TO_LCHEX(b & 0xf);

	_debug_nprint(buf, 2);
}

/**
 * @brief Prints a sequence of bytes in hex format through the debug
 *        functionality.
 */
void _debug_printbytes(const uint8_t *arr, size_t n)
{
	size_t i;

	if (arr == NULL)
		return;

	_debug_print("[");
	for (i = 0; i < n; i++) {
		if (i != 0)
			_debug_print(", 0x");
		else
			_debug_print("0x");

		_debug_printbyte(arr[i]);
	}
	_debug_print("]");
}

/**
 * @brief Print a 7-character callsign through the debug functionality.
 */
void _debug_printcallsign(const uint8_t *cs)
{
	int i;
	char buf[6];
	bool isvalid = true;

	if (cs == NULL)
		return;

	for (i = 0; i < 6; i++) {
		if ((cs[i] & 0x1) != 0)
			isvalid = false;

		buf[i] = (char) ((cs[i] >> 1) & 0x7f);
	}

	if (isvalid)
		_debug_nprint(buf, 6);
	else
		_debug_print("invalid callsign");
}

/**
 * @brief Prints an SSID through the debug functionality. Indicating whether it
 *        is a destination or source SSID.
 */
void _debug_printssid(uint8_t ssid)
{
	if ((ssid & 0xe0) != 0x60) {
		_debug_print("invalid SSID");
		return;
	}

	if ((ssid & 0x1) == 0)
		_debug_print("dst(");
	else
		_debug_print("src(");

	_debug_printint((int) ((ssid >> 1) & 0xf));
	_debug_print(")");
}

/**
 * @brief Flush the debug buffer, printing its contents.
 */
void _debug_flush(void)
{
	if (!_debug_is_initiialized)
		return;
	if (_debugbufidx == 0)
		return;

	if (_debugbufidx > DEBUGDEF_BUFSIZE)
		_debugbufidx = DEBUGDEF_BUFSIZE;

	_debugbuf[_debugbufidx] = '\0';
	Serial.print(_debugbuf);
	Serial.flush();

	_debugbufidx = 0;
}
