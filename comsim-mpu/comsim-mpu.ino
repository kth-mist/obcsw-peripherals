/**
 * @file comsim_mpu.ino
 */

#include <inttypes.h>
#include <stdlib.h>

#include <Wire.h>
#include "debug.hpp"

extern "C" {
	#include "reg_functions.h"
	#include "mpu_handler.h"
}

#define I2C_BITRATE (400000)



void receiveEvent(int msg_size);
void requestEvent(void);

char inputString[16];

/**
 * Arduino setup
 */
void setup(void)
{
	Wire.begin(MPU_I2C_ADDRESS);
	Wire.setClock(I2C_BITRATE);
	Wire.onReceive(receiveEvent);
	Wire.onRequest(requestEvent);

	mpu_init_regs();

	Serial.begin(9600);
	while (!Serial) {

	}

	mpu_init_regs();

	Serial.println("MPU Sim started.");

	DEBUG_INIT();
	DEBUG_PRINTLN("Debugging enabled.");
	DEBUG_FLUSH();
}


/**
 * Arduino loop
 */
void loop(void)
{
	delay(100);
}


/**
 * @brief Handler of i2c receive
 *
 * @param[in] msg_size
 */
void receiveEvent(int msg_size)
{
	uint8_t tmpdata[3] = {0};
	size_t len;
	int err;

	for (len = 0; Wire.available() && len < sizeof(tmpdata); len++) {
		tmpdata[len] = Wire.read();
	}

	if (Wire.available()) {
		DEBUG_PRINTLN("TOO MANY BYTES RECEIVED. IGNORING EXCESS. REG:");
		DEBUG_PRINTBYTE(tmpdata[0]);
		DEBUG_PRINTLN("MSG_SIZE:");
		DEBUG_PRINTINT(msg_size);
		while(Wire.available()) {
			(void) Wire.read();
		}
	}

	err = mpu_handler_receive(tmpdata, len);
	if (err != 0) {
		DEBUG_PRINTLN("ERROR SETTING REGISTER. FLUSHING BUFFER. REG:");
		DEBUG_PRINTBYTE(mpu_get_active_reg());
	}
}

/**
 * @brief Handler of i2c request
 *
 */
void requestEvent(void)
{
	uint8_t send_buf[2];
	size_t len = mpu_handler_send(send_buf, sizeof(send_buf));
	for (size_t i = 0; i < len; i++) {
		Wire.write(send_buf[i]);
	}
}

void serialEvent() {
  memset(inputString, 0x0, sizeof(inputString));
  int ireg, ival;
  Serial.readBytesUntil('\n', inputString, 16);
  char* separator = strchr(inputString, ' ');
  ireg = strtol(inputString, NULL, 0) - 0x41;
  if ((ireg < 0) || (ireg > mpu_size_of_regs())) {
    Serial.println("Register out of bounds.");
    return;
  }
  if (separator != NULL) {
    ival = strtol(separator, NULL, 0);
    mpu_wr_reg(ireg, ival);
  }
  Serial.print("REG: ");
  Serial.println(ireg + 0x41, HEX);
  Serial.print("VAL: ");
  Serial.println(mpu_rd_reg(ireg), DEC);
  Serial.print("BIN: ");
  Serial.println(mpu_rd_reg(ireg), BIN);
}
