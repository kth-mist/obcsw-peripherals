/**
 * @file mpu_handlers.h
 */

#ifndef MPU_HANDLERS_H
#define MPU_HANDLERS_H

void mpu_setup(void);
void mpu_set_gyro_x(int16_t val);
void mpu_set_gyro_y(int16_t val);
void mpu_set_gyro_z(int16_t val);
void mpu_set_self_test_regs(uint8_t x, uint8_t y, uint8_t z);
int mpu_handler_send(uint8_t *buf, size_t len);
int mpu_handler_receive(uint8_t *data, size_t len);

#endif /* MPU_HANDLERS_H */
