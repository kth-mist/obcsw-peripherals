/**
 * @file reg_functions.c
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "reg_functions.h"

static uint8_t active_reg;
static uint8_t reg_arr[MPU_REG_LENGTH];

static int double_regs[MPU_REG_LENGTH] = {
	[MPU_TEMP_H]1, [MPU_GYRO_X_H]1, [MPU_GYRO_Y_H]1, [MPU_GYRO_Z_H]1
};

static int bad_regs[MPU_REG_LENGTH] = {
	[MPU_TEMP_L]1, [MPU_GYRO_X_L]1, [MPU_GYRO_Y_L]1, [MPU_GYRO_Z_L]1
};

/*
 * @brief Setting up firmware version and other misc. static values
 */
void mpu_init_regs(void)
{
	active_reg = 0x01;
	memset(reg_arr, 0x0, sizeof(reg_arr));

	reg_arr[MPU_TEMP_H] = 0b11010000;	// (-12080)
	reg_arr[MPU_TEMP_L] = 0b11010000;	// Represents a measured temp of 1 deg C.
	reg_arr[MPU_PWR_MGMT1] = 0x40;		// SLEEP bit is set on power up.
	reg_arr[MPU_WHOAMI] = MPU_I2C_ADDRESS;

	return;
}


/*
 * @brief Returns the size of the register array
 */
int mpu_size_of_regs(void)
{
	return(sizeof(reg_arr));
}

/**
 * @brief set_active_reg changes the active register
 *
 * @param[in] reg_adr new address to set
 */
int mpu_set_active_reg(uint8_t reg_adr)
{
    if (mpu_invalid_register(reg_adr))
        return 1;

    active_reg = reg_adr;
    return 0;
}


/**
 * @brief wr_reg writes received data to register
 *
 * @param[in] r reg address to write to
 * @param[in] data byte of data to write
 */
void mpu_wr_reg(uint8_t r, uint8_t data)
{
	reg_arr[r] = data;
	return;
}


/**
 * @brief Checks if a register is valid w/r-able
 *
 * @param[in] r register to check
 *
 * @param[out] int yes or no
 */
int mpu_invalid_register(uint8_t r)
{
	if ((bad_regs[r] == 1) || (r >= MPU_REG_LENGTH))
		return 1;

	return 0;
}


/**
 * @brief Checks if a register is 2 bytes
 *
 * @param[in] r register to check
 *
 * @param[out] int yes or no
 */
int mpu_reg_is_double(uint8_t r)
{
	return double_regs[r];
}


/**
 * @brief Returns the value of the current active reg
 *
 * @param[in] r register to read
 *
 * @param[out] uint8_t value of the register
 */
uint8_t mpu_rd_reg(uint8_t r)
{
    return reg_arr[r];
}

/*
 * @brief Returns the value of the current active reg
 */
uint8_t mpu_get_active_reg(void)
{
    return active_reg;
}
