/**
 * @file reg_functions.h
 */

#ifndef REG_FUNCTIONS_H
#define REG_FUNCTIONS_H

#define MPU_I2C_ADDRESS (0x68)

// 0x0D is the "start address" of the MPU
#define MPU_SELF_TEST_X			(0x0D)
#define MPU_SELF_TEST_Y			(0x0E)
#define MPU_SELF_TEST_Z			(0x0F)
#define MPU_SMPLRT_DIV			(0x19)
#define MPU_CONFIG              (0x1A)
#define MPU_GYRO_CONF           (0x1B)
#define MPU_TEMP_H              (0x41)
#define MPU_TEMP_L              (0x42)
#define MPU_GYRO_X_H            (0x43)
#define MPU_GYRO_X_L            (0x44)
#define MPU_GYRO_Y_H            (0x45)
#define MPU_GYRO_Y_L            (0x46)
#define MPU_GYRO_Z_H            (0x47)
#define MPU_GYRO_Z_L            (0x48)
#define MPU_PWR_MGMT1           (0x6B)
#define MPU_PWR_MGMT2           (0x6C)
#define MPU_WHOAMI              (0x75)

#define MPU_REG_LENGTH          (0x76)

void mpu_init_regs(void);
int mpu_size_of_regs(void);
void mpu_wr_reg(uint8_t r, uint8_t data);
int mpu_set_active_reg(uint8_t reg_adr);
int mpu_invalid_register(uint8_t r);
int mpu_reg_is_double(uint8_t r);
uint8_t mpu_rd_reg(uint8_t r);
uint8_t mpu_get_active_reg(void);

#endif /* REG_FUNCTIONS_H */
