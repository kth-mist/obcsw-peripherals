/**
 * @file mpu_handler.c
 */

#include <inttypes.h>
#include <stdlib.h>

#include "mpu_handler.h"
#include "reg_functions.h"

static uint8_t init = 0;


void mpu_setup(void)
{
	mpu_init_regs();
}

void mpu_set_gyro_x(int16_t val)
{
	if (!init) {
		mpu_init_regs();
		init = 1;
	}
	mpu_wr_reg(MPU_GYRO_X_H, (val >> 8) & 0xff);
	mpu_wr_reg(MPU_GYRO_X_L, val & 0xff);
}

void mpu_set_gyro_y(int16_t val)
{
	if (!init) {
		mpu_init_regs();
		init = 1;
	}
	mpu_wr_reg(MPU_GYRO_Y_H, (val >> 8) & 0xff);
	mpu_wr_reg(MPU_GYRO_Y_L, val & 0xff);
}

void mpu_set_gyro_z(int16_t val)
{
	if (!init) {
		mpu_init_regs();
		init = 1;
	}
	mpu_wr_reg(MPU_GYRO_Z_H, (val >> 8) & 0xff);
	mpu_wr_reg(MPU_GYRO_Z_L, val & 0xff);
}

void mpu_set_self_test_regs(uint8_t x, uint8_t y, uint8_t z)
{
	if (!init) {
		mpu_init_regs();
		init = 1;
	}
	mpu_wr_reg(MPU_SELF_TEST_X, x);
	mpu_wr_reg(MPU_SELF_TEST_Y, y);
	mpu_wr_reg(MPU_SELF_TEST_Z, z);
}

int mpu_handler_send(uint8_t *buf, size_t len)
{
	if (!init) {
		mpu_init_regs();
		init = 1;
	}
	uint8_t r = mpu_get_active_reg();

	if (len > 0) {
		buf[0] = mpu_rd_reg(r);
		if (len > 1 && mpu_reg_is_double(r)) {
			buf[1] = mpu_rd_reg(r + 1);
			return 2;
		}

		return 1;
	}
	return 0;
}

int mpu_handler_receive(uint8_t *data, size_t len)
{
	if (!init) {
		mpu_init_regs();
		init = 1;
	}
	uint8_t a_reg;
	if (len > 0) {
		a_reg = data[0];
		if (mpu_set_active_reg(a_reg))
			return 1;

		if (len > 1) {
			if (mpu_reg_is_double(a_reg)) {
				if (len < 3)
					return 1;

				mpu_wr_reg(a_reg + 1, data[2]);
			}
			mpu_wr_reg(a_reg, data[1]);
		}
	}
	return 0;
}
