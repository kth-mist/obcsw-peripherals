# MPU Reg Simulator

This folder contains a very basic simulator of the register access of the MPU 6050.
The 4 registers simulated are of the measured temperature, and 3 gyro axes.
All of these are 2 byte registers.

## Setup
This setup process assumes the following:
 - You are able to compile and flash Arduino Due software.
 - You have access to an Arduino Due.
 - You have access to an OBC or similar.
 - You have the necessary cables and wires to connect the devices.

### 1. Compiling the simulator code
No special action needed.

### 2. Connecting the simulator hardware
Connect the **SDA (20)** and **SDA1** pins on the Arduino to the SDA line
coming from the iOBC (ISIS on-board computer). Connect the **SCL (21)** and
**SCL1** pins on the Arduino to the SCL line coming from the iOBC. Also make
sure that you connect the Arduino to ground. **The Arduino can experience
frequent I2C bit-errors if not connected to ground!**

Connect the **Native USB** port on the Arduino to any of the USB ports on your
laptop. As you may notice, there are two USB ports on the Arduino. The
Native USB port is the one closest to the reset button (the "left" port).
This is for debugging only.
Check the **Device Manager** in Windows and see which COM port you connected
the Arduino to. Under the COM submenu you should find something along the lines
of **Arduino Due (COM#)** where **#** is a placeholder for its number. Let
**COM#** be the COM port that was connected to your Arduino Due.

### 3. Run the OBCSW
Any communication that is normally done to the MPU 6050 should
now be handled by the arduino.

The initial register values can be manually set in the init\_regs function in reg\_functions.c
They can be changed while running by connecting over serial interface and sending the register address and new value e.g. "0x02 0xC0" to set register 2 to value 0xC0. Base 10 and 16 (by prepending the values with 0x) are accepted for both alues. If only a register address is sent the current value is read and replied in bases 10 and 2.
