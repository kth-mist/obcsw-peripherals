/**
 * @file   debug.hpp
 * @author John Wikman
 */

#ifndef DEBUG_HPP
#define DEBUG_HPP

#include <inttypes.h>

// Uncomment the following line to enable debug functionality
//#define ENABLE_DEBUG

/* @brief Used in the QEMU/HAL-clone setup to differentiate
          between Arduino simulators */
#define ARDUINO_UNIT_ID_MPU    (19)

#define DEBUGDEF_SERIAL_CLOCK (9600)
#define DEBUGDEF_BUFSIZE (4000)

#ifdef ENABLE_DEBUG
#    define DEBUG_INIT() (_debug_init())
#    define DEBUG_DEINIT() (_debug_deinit())
#    define DEBUG_PRINT(s) (_debug_print(s))
#    define DEBUG_PRINTLN(s) (_debug_println(s))
#    define DEBUG_PRINTINT(v) (_debug_printint(v))
#    define DEBUG_PRINTBYTE(b) (_debug_printbyte(b))
#    define DEBUG_PRINTBYTES(arr, n) (_debug_printbytes(arr, n))
#    define DEBUG_PRINTSSID(ssid) (_debug_printssid(ssid))
#    define DEBUG_PRINTCALLSIGN(cs) (_debug_printcallsign(cs))
#    define DEBUG_FLUSH() (_debug_flush())
#else
#    define DEBUG_INIT()
#    define DEBUG_DEINIT()
#    define DEBUG_PRINT(s)
#    define DEBUG_PRINTLN(s)
#    define DEBUG_PRINTINT(v)
#    define DEBUG_PRINTBYTE(b)
#    define DEBUG_PRINTBYTES(arr, n)
#    define DEBUG_PRINTSSID(ssid)
#    define DEBUG_PRINTCALLSIGN(cs)
#    define DEBUG_FLUSH()
#endif

void _debug_init(void);
void _debug_deinit(void);
void _debug_print(const char *str);
void _debug_println(const char *str);
void _debug_nprint(const char *str, size_t n);
void _debug_printint(int v);
void _debug_printbyte(uint8_t b);
void _debug_printbytes(const uint8_t *arr, size_t n);
void _debug_printcallsign(const uint8_t *cs);
void _debug_printssid(uint8_t ssid);
void _debug_flush(void);

#endif /* DEBUG_HPP */
