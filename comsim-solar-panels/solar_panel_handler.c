/**
 * @file   solar_panel_handler.c
 * @author William Stackenäs
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "solar_panel_handler.h"

// See https://www.analog.com/media/en/technical-documentation/data-sheets/2983fc.pdf for details
#define SOLAR_PANEL_V2_WRITE (0x02)
#define SOLAR_PANEL_V2_READ  (0x03)

// For v1, 2 0x00 bytes are sent, and the temperature is returned as 16-bit unsigned BE.
static int solar_panel_send_temp(const uint8_t *write_data, uint8_t *read_data, size_t len, uint16_t temp)
{
	uint8_t expected[2];
	memset(expected, 0, sizeof(expected));

	if (write_data == NULL || read_data == NULL || len != sizeof(expected))
		return 1;

	if (memcmp(expected, write_data, sizeof(expected)) != 0)
		return 1;

	read_data[0] = (temp >> 8) & 0xff;
	read_data[1] = temp & 0xff;

	return 0;
}

int solar_panel_handler_0(const uint8_t *write_data, uint8_t *read_data, size_t len)
{
	return solar_panel_send_temp(write_data, read_data, len, 15422);
}

int solar_panel_handler_1(const uint8_t *write_data, uint8_t *read_data, size_t len)
{
	return solar_panel_send_temp(write_data, read_data, len, 7236);
}

int solar_panel_handler_2(const uint8_t *write_data, uint8_t *read_data, size_t len)
{
	return solar_panel_send_temp(write_data, read_data, len, 837);
}

int solar_panel_handler_3(const uint8_t *write_data, uint8_t *read_data, size_t len)
{
	return solar_panel_send_temp(write_data, read_data, len, 31287);
}

int solar_panel_handler_4(const uint8_t *write_data, uint8_t *read_data, size_t len)
{
	return solar_panel_send_temp(write_data, read_data, len, 9174);
}

int solar_panel_v2_handler(const uint8_t *write_data, uint8_t *read_data, size_t len)
{
	int i;

	if (write_data == NULL || read_data == NULL || len != 4)
		return 1;

	// Set default reply to all zeroes
	memset(read_data, 0, 4);

	switch (write_data[0]) {
	case SOLAR_PANEL_V2_WRITE:
		// No reply seems to be needed here, do nothing
		// Sent when the solar panels are initialized, should go to sleep, or awaken.
		// When temperatures should be read, 0x02 0x00 0x00 <0x84 + panel * 2> is sent in the first transaction.
		// A particular GPIO must be set during the first transaction, or a timeout error is returned.
		break;

	case SOLAR_PANEL_V2_READ:
		// In the second transaction, 0x03 0x00 0x00 0x00 is sent.
		if (write_data[2] == 0) {
			// The sixth bit of least significant byte (done bit in the command status register)
                        // must be set in the reply, otherwise a timeout is returned.
			read_data[3] = 0x40;
		} else {
			// Lastly, 0x03 0x00 <0x1c + panel * 8 + i> 0x00 is sent 4 times. (from 0 <= i <= 3).
			i = write_data[2] % 4;
			if (i == 0) {
				// When i == 0, the status is requested and LSB of the reply must be
				// 1, otherwise an ADC error is returned.
				read_data[3] = 1;
			} else {
				// For i > 0, the LSB of the reply is byte i of the 24-bit unsigned BE temperature.
				// Returned temperature is (panel * 4 + 1) * 2^16 + (panel * 4 + 2) * 2^8 + (panel * 4 + 3)
				read_data[3] = ((write_data[2] - 0x1c - i) >> 1) + i;
			}
		}
		break;

	default:
		return 1;
	}

	return 0;
}

unsigned char solar_panel_v2_ready(void)
{
	return 1;
}

