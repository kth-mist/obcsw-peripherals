/**
 * @file   solar_panel_handler.h
 * @author William Stackenäs
 */

#ifndef SOLAR_PANEL_CMD_HANDLER_H
#define SOLAR_PANEL_CMD_HANDLER_H

#include <inttypes.h>

int solar_panel_handler_0(const uint8_t *write_data, uint8_t *read_data, size_t len);
int solar_panel_handler_1(const uint8_t *write_data, uint8_t *read_data, size_t len);
int solar_panel_handler_2(const uint8_t *write_data, uint8_t *read_data, size_t len);
int solar_panel_handler_3(const uint8_t *write_data, uint8_t *read_data, size_t len);
int solar_panel_handler_4(const uint8_t *write_data, uint8_t *read_data, size_t len);
int solar_panel_v2_handler(const uint8_t *write_data, uint8_t *read_data, size_t len);
unsigned char solar_panel_v2_ready(void);

#endif /* SOLAR_PANEL_CMD_HANDLER_H */
